<?php
/*
Plugin Name: Violin Registration
Plugin URI:
Description:
Version: 0.1
Author: ishishmarev
Author URI:
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define( 'VR_PLUGIN_DIR', dirname( __FILE__ ) );
define( 'VR_PLUGIN_URL', plugins_url( 'violin-registration' ) );
define( 'VR_PLUGIN_SETTINGS_PAGE', 'vr-plugin-settings' );
define( 'VR_PLUGIN_PAYMENT_SETTINGS_PAGE', 'vr-plugin-payment-settings' );
define( 'VR_REG_URL', 'register' );
define( 'VR_USER_URL', 'user' );
define( 'VR_PAYMENT_URL', 'payment' );
define( 'VR_PAYMENT_CHECK_URL', 'payment_check' );

include_once VR_PLUGIN_DIR . '/includes/payment.php';
include_once VR_PLUGIN_DIR . '/includes/forms.php';
include_once VR_PLUGIN_DIR . '/includes/functions.php';

// Create plugin registration page when activate plugin.
register_activation_hook( __FILE__, 'vr_plugin_activation' );

// Remove plugin registration page when deactivate plugin.
register_deactivation_hook( __FILE__, 'vr_plugin_deactivation' );

// AJAX auth.
add_action( 'wp_ajax_auth', 'vr_ajax_login' );
add_action( 'wp_ajax_nopriv_auth', 'vr_ajax_login' );

// Plugin settings page.
add_action( 'admin_menu', 'admin_add_menu');

// Plugin wysiwyg editor
add_filter( 'mce_external_plugins', 'vr_mce_external_plugins', 20 );

// Custom fields for user.
add_action('user_register',           'vr_profile_custom_fields' );
add_action('personal_options_update', 'vr_profile_custom_fields' );
add_action('edit_user_profile_update','vr_profile_custom_fields' );

// Hide plugin register page from pages list.
add_action( 'pre_get_posts' ,'vr_hide_register_page_from_default' );

// Redirect from default WP register page to plugin register page.
add_action('init', 'vr_init_redirect');

// Change default link to register page.
add_filter ('register_url', 'vr_register_page_url');

// Register shortcode, contains register form. [vr_register_form]
add_shortcode( 'vr_register_form', 'vr_register_form_shortcode' );

// User page shortcode, contains user form. [vr_user_form]
add_shortcode( 'vr_user_form', 'vr_user_form_shortcode' );

// Templates for Violin Registration Pages.
add_filter( 'template_include', 'vr_page_templates' );
