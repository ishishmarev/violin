<?php
wp_register_script( 'form-valid', VR_PLUGIN_URL . '/assets/js/valid/valid.js' );
wp_enqueue_script( 'form-valid' );
?>
<form action="<?php print $_SERVER['REQUEST_URI']; ?>" method="post" class="register__form">
  <div class="register__form--head force__gutter">
    <h2>Заполните важную информацию</h2>
  </div>
	<?php
	global $reg_errors;
	if ( is_wp_error( $reg_errors ) ) {
		foreach ( $reg_errors->get_error_messages() as $error ) {
			echo '<div>';
			echo '<strong>ERROR</strong>:';
			echo $error . '<br/>';
			echo '</div>';
		}
	}
	?>

  <div class="register__form--body section__border">
    <div class="section__border--system">
      <div class="section__border--bar">
        <div class="section__border--title">Регистрационные данные</div>
        <div class="register__form--set">
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="firstname" data-require="true" value="<?php print ( isset( $_POST['firstname'] ) ? esc_attr($firstname) : null ); ?>">
              <span class="form__field--placeholder">Имя</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="lastname" data-require="true" value="<?php print ( isset( $_POST['lastname'] ) ? esc_attr($lastname) : null ); ?>">
              <span class="form__field--placeholder">Фамилия</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="country" value="<?php print ( isset( $_POST['country'] ) ? esc_attr($country) : null ); ?>">
              <span class="form__field--placeholder">Страна</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="city" value="<?php print ( isset( $_POST['city'] ) ? esc_attr($city) : null ); ?>">
              <span class="form__field--placeholder">Город</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="login" data-require="true" value="<?php print ( isset( $_POST['login'] ) ? esc_attr($login) : null ); ?>">
              <span class="form__field--placeholder">Логин</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="email" name="email" data-require="true" value="<?php print ( isset( $_POST['email'] ) ? esc_attr($email) : null ); ?>">
              <span class="form__field--placeholder">E-mail</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="password" data-require="true" name="rmpassword">
              <span class="form__field--placeholder">Пароль</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="password" data-require="true" name="re_password">
              <span class="form__field--placeholder">Повторить пароль</span>
            </label>
          </div>
        </div>
      </div>
      <div class="section__border--content">
        <div class="section__border--title">Выберите услугу</div>
        <div class="register__price--list">

          <?php
            $option_groups = get_option( VR_PLUGIN_SETTINGS_PAGE );

            foreach ($option_groups as $option_group_id => $option_group) :
          ?>
              <div class="register__price--item">
                <label class="form__radio--label">
                  <input class="form__radio--input" type="radio" name="register_service" value="<?php print $option_group_id; ?>" <?php print $option_group_id == 0 ? 'checked' : ''; ?>>
                  <span class="form__radio--text"><?php print $option_group['services_title']; ?></span>
                </label>
                <div class="register__price--description">
                  <?php print wpautop( $option_group['services_description'], true );?>
                </div>
              </div>
          <?php endforeach; ?>

        </div>
      </div>
    </div>
  </div>
  <div class="register__form--footer col-md-12">
    <div class="register__form--submit">
      <input type="hidden" name="order_cust_id" value="<?php print date('YmdHis', strtotime( current_time( 'mysql' ) ) );?>">
      <input class="btn btn--over-size" type="submit" name="submit" value="Зарегистрироваться"/>
    </div>
    <div class="register__form--text">Нажимая кнопку «Зарегистрироваться»:</div>
    <div class="register__form--line">
      <label class="form__checkbox--label">
        <input class="form__checkbox--input" type="checkbox" name="agree_mailing[]" value="1" checked <?php //print ( isset( $_POST['agree_mailing'] ) ? 'checked' : null ); ?>>
        <span class="form__checkbox--text">
          Я соглашаюсь на получение новостных рассылок
        </span>
      </label>
    </div>
    <div class="register__form--line">
      <label class="form__checkbox--label">
        <input class="form__checkbox--input" type="checkbox" name="agree_policy[]" value="1" checked <?php //print ( isset( $_POST['agree_policy'] ) ? 'checked' : null ); ?>>
        <span class="form__checkbox--text">
          Я принимаю условия <a href="#">Пользовательского соглашения</a>
          и даю своё согласие Яндексу на обработку моей персональной информации на условиях, определенных
          <a href="#">Политикой конфиденциальности</a>
        </span>
      </label>
    </div>
  </div>
</form>
