<?php

global $monetaSDK;

?>

<?php get_header(); ?>

  <div class="page__content">

    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <h1 class="page-title">Покупка прошла успешно</h1>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">
        <?php
        $user_id = get_current_user_id();
        $operation = $monetaSDK->sdkMonetaFindOperationsListByCTID($monetaSDK->getSettingValue('monetasdk_account_id'), $_GET['MNT_TRANSACTION_ID']);
        if (!empty($operation->data->operation) || get_user_meta( $user_id, 'moneta_order_id', true) === $_GET['MNT_TRANSACTION_ID'] ) {
          $__details = $operation->data->operation[0]->attribute;

          foreach ($__details as $detail_col => $detail_col_val) {
            if ($detail_col_val->key === 'statusid' && $detail_col_val->value === 'SUCCEED') { // Заказ успешно оплачен.

              $user_selected_service_id = get_user_meta( $user_id, 'register_service_id', true);
              $services = get_option( VR_PLUGIN_SETTINGS_PAGE );
              $selected_service = $services[$user_selected_service_id];
              $register_service_expire = get_user_meta( $user_id, 'register_service_expire', true);

              if (!empty($register_service_expire)) {

                $current_time = current_time( 'mysql' );

                if (strtotime($register_service_expire) < strtotime($current_time)) {
                  update_user_meta( $user_id, 'register_service_expire', date('Y-m-d H:i:s', strtotime( "+{$selected_service['services_expire']} months", strtotime( current_time( 'mysql' ) ) ) ) );
                }


              } else {
                update_user_meta( $user_id, 'register_service_expire', date('Y-m-d H:i:s', strtotime( "+{$selected_service['services_expire']} months", strtotime( current_time( 'mysql' ) ) ) ) );
              }

              break;
            }
          }
        } else {
          wp_redirect(home_url());
        }


        ?>

        <div class="text-center" style="padding-top: 50px;">
          <a href="<?php print home_url(); ?>" class="btn btn--over-size" style="text-decoration: none;">Перейти к обучению</a>
        </div>
      </div>
    </section>

  </div>

<?php get_footer();
