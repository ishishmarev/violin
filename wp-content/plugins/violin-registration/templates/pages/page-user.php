<?php
/**
 * The template for displaying all pages
 */

if (!is_user_logged_in()) wp_redirect(site_url(VR_REG_URL));

get_header();
the_post();
?>

  <section class="content section section__register--form section__user--form">
    <div class="container">
      <?php
      print do_shortcode(get_post_meta($post->ID, 'form', true));
      ?>
    </div>
  </section>

<?php get_footer();
