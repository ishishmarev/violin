<?php
/**
 * The template for displaying all pages
 */

if (is_user_logged_in()) wp_redirect(site_url(VR_USER_URL));

get_header();
the_post();
?>

  <section class="content section section__head section__register">
    <div class="container">
      <div class="force__gutter">
        <?php the_content(); ?>
      </div>
    </div>
  </section>

  <section class="content section section__register--form">
    <div class="container">
      <?php
      print do_shortcode(get_post_meta($post->ID, 'form', true));
      ?>
    </div>
  </section>

<?php get_footer();
