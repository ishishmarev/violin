<?php
wp_register_script( 'form-valid', VR_PLUGIN_URL . '/assets/js/valid/valid.js' );
wp_enqueue_script( 'form-valid' );

$user_id = get_current_user_id();
$user_data = get_userdata($user_id);
$user_meta = get_user_meta($user_id);

?>
<form action="<?php print $_SERVER['REQUEST_URI']; ?>" method="post" class="register__form">
	<?php wp_nonce_field('user_profile_check', 'user_profile_check_nonce'); ?>
  <div class="register__form--head force__gutter">
    <h2>Привет, <?php print $user_meta['first_name'][0]; ?></h2>
    <div class="register__head--exit">
      <a href="<?php echo wp_logout_url(home_url()); ?>">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="87.3px" height="21.4px" viewBox="0 0 87.3 21.4" xml:space="preserve">
          <g>
            <text transform="matrix(1 0 0 1 29.4097 15.4881)" class="st0 st1">ВЫХОД</text>
            <g class="st2">
              <g>
                <path d="M9.5,19H1.3V1.3h8.2c0.3,0,0.6-0.3,0.6-0.6C10.1,0.3,9.8,0,9.5,0H0.6C0.3,0,0,0.3,0,0.6v19c0,0.3,0.3,0.6,0.6,0.6h8.9c0.3,0,0.6-0.3,0.6-0.6C10.1,19.3,9.8,19,9.5,19L9.5,19z M9.5,19"></path>
                <path d="M20.1,9.7l-4.4-4.4c-0.2-0.2-0.6-0.2-0.9,0c-0.2,0.2-0.2,0.7,0,0.9l3.3,3.3h-13c-0.3,0-0.6,0.3-0.6,0.6c0,0.4,0.3,0.6,0.6,0.6h13l-3.3,3.3c-0.2,0.3-0.2,0.7,0,0.9c0.2,0.2,0.6,0.2,0.9,0l4.4-4.4C20.3,10.3,20.3,9.9,20.1,9.7L20.1,9.7z M20.1,9.7"></path>
              </g>
            </g>
          </g>
        </svg>
      </a>
    </div>
  </div>
	<?php
	global $reg_errors;
	if ( is_wp_error( $reg_errors ) ) {
		foreach ( $reg_errors->get_error_messages() as $error ) {
			echo '<div>';
			echo '<strong>ERROR</strong>:';
			echo $error . '<br/>';
			echo '</div>';
		}
	}
	?>

	<div class="register__form--body section__border">
		<div class="section__border--system">
			<div class="section__border--bar">
				<div class="section__border--title">Регистрационные данные</div>
				<div class="register__form--set">
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="text" name="firstname" data-require="true" value="<?php print ( !empty( $user_meta['first_name'] ) ? esc_attr($user_meta['first_name'][0]) : null ); ?>">
							<span class="form__field--placeholder">Имя</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="text" name="lastname" data-require="true" value="<?php print ( !empty( $user_meta['last_name'] ) ? esc_attr($user_meta['last_name'][0]) : null ); ?>">
							<span class="form__field--placeholder">Фамилия</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="text" name="country" value="<?php print ( !empty( $user_meta['country'] ) ? esc_attr($user_meta['country'][0]) : null ); ?>">
							<span class="form__field--placeholder">Страна</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="text" name="city" value="<?php print ( !empty( $user_meta['city'] ) ? esc_attr($user_meta['city'][0]) : null ); ?>">
							<span class="form__field--placeholder">Город</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="text" name="login" data-require="true" value="<?php print ( !empty( $user_data->data->user_login ) ? esc_attr($user_data->data->user_login) : null ); ?>">
							<span class="form__field--placeholder">Логин</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="email" name="email" data-require="true" value="<?php print ( !empty( $user_data->data->user_email ) ? esc_attr($user_data->data->user_email) : null ); ?>">
							<span class="form__field--placeholder">E-mail</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="password" data-require="true" name="old_password">
							<span class="form__field--placeholder">Старый пароль</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="password" data-require="true" name="rmpassword">
							<span class="form__field--placeholder">Новый пароль</span>
						</label>
					</div>
					<div class="form__field">
						<label class="form__field--label">
							<input class="form__field--input" type="password" data-require="true" name="re_password">
							<span class="form__field--placeholder">Повторить пароль</span>
						</label>
					</div>
				</div>
			</div>
			<div class="section__border--content">
        <?php
          global $monetaSDK;

          $current_time = current_time( 'mysql' );
          $expire_date = get_user_meta( $user_id, 'register_service_expire', true);
          $expire_date_user_format = mysql2date( get_option( 'date_format' ), $expire_date );
          $moneta_order_id = get_user_meta( $user_id, 'moneta_order_id', true);
          $message = '';

          if ( !empty($expire_date) ) {
            $message = "Ваша услуга действует до {$expire_date_user_format}";
          } else {

            $message = "У вас нет оплаты услуг, пожалуйста выберете услугу";

            if ( !empty($moneta_order_id) ) {
              $operation = $monetaSDK->sdkMonetaFindOperationsListByCTID($monetaSDK->getSettingValue('monetasdk_account_id'), $moneta_order_id);

              if (!empty($operation->data->operation)) {
                $__details = $operation->data->operation[0]->attribute;

                foreach ($__details as $detail_col => $detail_col_val) {
                  if ($detail_col_val->key === 'statusid' && $detail_col_val->value === 'SUCCEED') { // Заказ успешно оплачен.

                    $user_selected_service_id = get_user_meta( $user_id, 'register_service_id', true);
                    $services = get_option( VR_PLUGIN_SETTINGS_PAGE );
                    $selected_service = $services[$user_selected_service_id];
                    $expire_date = date('Y-m-d H:i:s', strtotime( "+{$selected_service['services_expire']} months", strtotime( current_time( 'mysql' ) ) ) );
                    $expire_date_user_format = mysql2date( get_option( 'date_format' ), $expire_date );

                    update_user_meta( $user_id, 'register_service_expire', $expire_date);

                    $message = "Ваша услуга действует до {$expire_date_user_format}";

                    break;
                  }
                }

              }
            }

          }

          if (strtotime($expire_date) < strtotime($current_time)) {
            $message = "Срок действия вашей услиги завершен, пожалуйста выберете другую услугу";
          }

        ?>
				<div class="section__border--title"><?php print $message; ?></div>
				<div class="register__price--list">

          <?php
          $option_groups = get_option( VR_PLUGIN_SETTINGS_PAGE );

          foreach ($option_groups as $option_group_id => $option_group) :
            ?>
            <div class="register__price--item">
              <label class="form__radio--label">
                <input class="form__radio--input" type="radio" name="register_service" value="<?php print $option_group_id; ?>" <?php print ($option_group_id == 0 && empty($expire_date_user_format)) ? 'checked' : ''; ?>>
                <span class="form__radio--text"><?php print $option_group['services_title']; ?></span>
              </label>
              <div class="register__price--description">
                <?php print wpautop( $option_group['services_description'], true );?>
              </div>
            </div>
          <?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
	<div class="register__form--footer col-md-12">
		<div class="register__form--submit">
      <input type="hidden" name="order_cust_id" value="<?php print date('YmdHis', strtotime( current_time( 'mysql' ) ) );?>">
			<input class="btn btn--over-size" type="submit" name="submit" value="Сохранить"/>
		</div>
	</div>
</form>
