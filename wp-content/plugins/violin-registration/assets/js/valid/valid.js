(function($) {

  function make_tooltip(message, description) {
    var _mes = '' +
    '<div class="reg-field__popup">' +
      '<div class="form__popup-error form__popup-text" data-t="login-error" role="alert">' +
        '<div class="form__login-suggest">' +
          '<strong class="suggest__status-text error-message">' +
            message +
          '</strong>' +
    '';

    if(description) {
    _mes+=
      '<div>' +
        '<span class="suggest__description-text">' +
          description +
        '</span>' +
      '</div>';
    }

    _mes+= '' +
        '</div>' +
      '</div>' +
    '</div>';

    return $(_mes);
  }

  function not_empty($form__field, $el, callback) {
    $('.register__form--set .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip

    if (!$.trim($el.val()).length) {
      $form__field.addClass('error');
      callback();
    } else {
      $form__field.removeClass('error');
    }
  }

  function validate_data($form__field, $el, _name) {
    switch (_name) {
      case 'firstname':
        not_empty($form__field, $el, function(){make_tooltip('Пожалуйста, укажите имя').appendTo($form__field)});
        break;
      case 'lastname':
        not_empty($form__field, $el, function(){make_tooltip('Пожалуйста, укажите фамилию').appendTo($form__field)});
        break;
      case 'login':
        $('.register__form--set .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip

        if (!$.trim($el.val()).length) {
          // если значение пустое
          $form__field.addClass('error');
          make_tooltip('Поле обязательно для заполнения').appendTo($form__field);
        } else if (!/^[a-zA-Z0-9\-.]+$/.test($el.val())) {
          // Разрешена только латиница, цифры, точки и дефис
          $form__field.addClass('error');
          make_tooltip('Такой логин не подойдет', 'Пожалуйста, используйте латиницу, цифры, точку и дефис').appendTo($form__field);
        } else {
          $form__field.removeClass('error');
          $form__field.find('.reg-field__popup').remove();
        }

        break;
      case 'email':
        $('.register__form--set .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip

        if (!$.trim($el.val()).length) {
          // если значение пустое
          $form__field.addClass('error');
          make_tooltip('Поле обязательно для заполнения').appendTo($form__field);
        } else if(!/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( $el.val() )) {
          make_tooltip('Такой E-mail не подойдет', 'Пожалуйста, проверьте правильность ввода').appendTo($form__field);
        } else {
          $form__field.removeClass('error');
          $form__field.find('.reg-field__popup').remove();
        }

        break;
      case 're_password':
        $('.register__form--set .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip

        if (!$.trim($el.val()).length) {
          // если значение пустое
          $form__field.addClass('error');
          make_tooltip('Необходимо ввести пароль').appendTo($form__field);
        } else if ( $el.val().length < 6 ) {
          $form__field.addClass('error');
          make_tooltip('Пароль должен быть не менее 6 символов').appendTo($form__field);
        } else if ( $el.val() !==  $('.register__form--set input[name="rmpassword"]').val()) {
          $form__field.addClass('error');
          make_tooltip('Подтверждение не совпадает с паролем').appendTo($form__field);
        } else {
          $form__field.removeClass('error');
          $form__field.find('.reg-field__popup').remove();
        }

        if ( $('.register__form [name="old_password"]').length ) { // Если есть поле старого пароля и в старом пароле есть данные

          if ( $.trim($('.register__form [name="old_password"]').val()).length ) {
            if ($el.val() !==  $('.register__form--set input[name="rmpassword"]').val()) {
              $form__field.addClass('error');
              make_tooltip('Подтверждение не совпадает с паролем').appendTo($form__field);
            } else {
              $form__field.removeClass('error');
              $form__field.find('.reg-field__popup').remove();
            }
          } else {
            // Если стерли данные из поле "Старый пароль"
            $form__field.removeClass('error');
            $form__field.find('.reg-field__popup').remove();
          }

        }

        break;
    }
  }

  $('.register__form--select select').styler();

  $('.register__form--set input').each(function(id, el){

    var $el = $(el);
    var _name = $el.attr('name');

    if ($el.data('require')) { // Обязательное ли поле
      $el.focusout(function(e){
        var $form__field = $el.parents('.form__field');
        validate_data($form__field, $el, _name);
      });
    }

  });

  $('.register__form').on('submit', function(e) {
    $('.register__form--set input').focusout();

    if( $('.register__form--set .error').length ) {
      return false;
    }

    return true;

  });

  $('.register__form [name="agree_policy\[\]"]').on('click', function(e) {

    if( !$(this).prop('checked') ) {
      $('.register__form input[type="submit"]').prop('disabled', true)
    } else {
      $('.register__form input[type="submit"]').prop('disabled', false);
    }

  });


})(jQuery);

