<?php
$path = VR_PLUGIN_DIR . '/index.php';
$data = get_plugin_data($path);
$message = ( get_option('no_login_message') ) ? get_option('no_login_message') : '';

$register_post_id = vr_get_page_by_url(VR_REG_URL);
$register_data = get_post($register_post_id);

if (!empty($_POST)) {
	wp_update_post(array(
		'ID'           => $register_post_id,
		'post_title'   => sanitize_text_field($_POST['post_title']),
		'post_content' => $_POST['post_content'],
  ));
	$register_data = get_post($register_post_id);
}
?>

<style type="text/css">
  .nav-tab { cursor: pointer; }
  .nav-tab:first-child { margin-left: 15px; }
  .tab-content { display: none; }
  .tab-content.active { display: block; }
  #button-export, #button-sync { margin-top: 4px; }
</style>

<script>
  (function($) {
    $(function() {
      $('.nav-tab').click(function() {
        $('.tab-content').removeClass('active');
        $('.nav-tab').removeClass('nav-tab-active');
        $('.tab-content.' + $(this).attr('rel')).addClass('active');
        $(this).addClass('nav-tab-active');
      });

      $('#button-export').click(function() {
        var groups = $('#export-field-groups').val();
        if (null != groups) {
          $.post(ajaxurl, {
              action: 'cfs_ajax_handler',
              action_type: 'export',
              field_groups: $('#export-field-groups').val()
            },
            function(response) {
              $('#export-output').text(response);
              $('#export-area').show();
            });
        }
      });

      $('#button-import').click(function() {
        $.post(ajaxurl, {
            action: 'cfs_ajax_handler',
            action_type: 'import',
            import_code: $('#import-code').val()
          },
          function(response) {
            $('#import-message').html(response);
          });
      });

      $('#button-reset').click(function() {
        if (confirm('This will delete all CFS data. Are you sure?')) {
          $.post(ajaxurl, {
              action: 'cfs_ajax_handler',
              action_type: 'reset'
            },
            function(response) {
              window.location.replace(response);
            });
        }
      });



    });
  })(jQuery);
</script>

<div class="wrap">
	<h1>Страница регистрации</h1>

  <h3 class="nav-tab-wrapper">
    <a class="nav-tab nav-tab-active" rel="edit_intro">Редактирование вводного текста</a>
    <a class="nav-tab" rel="pay">Оплата</a>
    <a class="nav-tab" rel="pay-settings">Настройка оплаты</a>
  </h3>

  <div class="content-container">
    <div class="tab-content edit_intro active">
      <form action="<?php print $_SERVER['REQUEST_URI']; ?>" method="post">
	      <?php wp_nonce_field('update-post'); ?>
        <div class="post-title" style="padding-top: 25px;">
          <div id="titlediv">
            <div id="titlewrap">
              <label class="screen-reader-text" id="title-prompt-text" for="title">Введите заголовок</label>
              <input type="text" name="post_title" size="30" value="<?php print $register_data->post_title; ?>" id="title" spellcheck="true" autocomplete="off">
            </div>
          </div>
        </div>
        <div class="post-body" style="padding-top: 25px;">
          <div id="post-body">
				    <?php wp_editor( $register_data->post_content, 'post_content' ); ?>
          </div>
        </div>
        <div id="post-action">
			    <?php submit_button(); ?>
        </div>
      </form>
    </div>
    <div class="tab-content pay">
      <form method="post" enctype="multipart/form-data" action="options.php">
        <?php
        global $wp_settings_fields;

        settings_fields( VR_PLUGIN_SETTINGS_PAGE ); // меняем под себя только здесь (название настроек)
        do_settings_sections( VR_PLUGIN_SETTINGS_PAGE );

        if ( isset( $wp_settings_fields[VR_PLUGIN_SETTINGS_PAGE] ) ) {
          $settings_groups = $wp_settings_fields[VR_PLUGIN_SETTINGS_PAGE];

          wp_enqueue_script( 'accordion' );
          $options = get_option( VR_PLUGIN_SETTINGS_PAGE ) ? get_option( VR_PLUGIN_SETTINGS_PAGE ) : array();

          ?>

        <div id="side-sortables" class="accordion-container" style="padding-top: 25px;">
          <ul class="outer-border">
            <li class="control-section accordion-section open">
              <h3 class="accordion-section-title hndle" tabindex="0">
                Варианты услуг
                <span class="screen-reader-text"><?php _e( 'Press return or enter to open this section' ); ?></span>
              </h3>
              <div class="accordion-section-content">
                <div class="inside">
                  <ul class="services-list">

                    <?php foreach ( $options as $field_group_id => $field_group ) : ?>

                    <li class="services-item">

                      <?php foreach ($field_group as $field_id => $field_value) : ?>

                        <?php call_user_func($settings_groups[''][$field_id]['callback'], array_merge($settings_groups[''][$field_id]['args'], array(
                          'option_group_value_id' => $field_group_id,
                          'option_group_id' => $field_group_id
                        ))); ?>

                      <?php endforeach; ?>


                      <span class="services-remove">удалить</span>
                    </li>

                    <?php endforeach; ?>

                  </ul>
                  <div class="table_footer" style="padding: 10px 12px; text-align: right;">
                    <input type="button" class="button-primary services-add" value="Добавить услугу">
                  </div>
                </div><!-- .inside -->
              </div><!-- .accordion-section-content -->
            </li><!-- .accordion-section -->
          </ul>
        </div>

          <style>
            .services-remove {
              text-decoration: underline;
              cursor: pointer;
              color: red;
            }
            .services-item {
              padding: 8px 0;
              border-bottom: 1px solid #dfdfdf;
            }
            .services-field {
              margin-bottom: 20px;
            }
            .services-field label {
              display: table;
              width: 100%;
            }
            .services-field-description,
            .services-field-value {
              display: table-cell;
            }
            .services-field-description {
              vertical-align: top;
              width: 30%;
            }
          </style>


          <script>

            (function($) {

              $('.services-add').on('click', function(e) {

                var new_id = $('.services-list li').length;
                var new_item = "<li class='services-item'>";
                <?php foreach ($settings_groups[''] as $settings_field_id => $field) {
                  print 'new_item += "';
                  call_user_func($field['callback'], array_merge($field['args'], array(
                    'option_group_id' => '" +  new_id + "'
                  )));
                  print '";';
                  print "\n";
                }
                ?>
                new_item += "<span class='services-remove'>удалить</span>";
                new_item += "</li>";

                $(new_item).appendTo('.services-list');

              });

              $(document).on('click', '.services-remove', function(e) {
                var $that = $(e.currentTarget);
                var parent = $that.parent();
                var index = parent.index();
                var items_count = $('.services-item').length;

                if (items_count > 1) {

                  parent.remove();

                  $('.services-item [name]').each(function(id, el) {
                    var parent_index = $(el).parents('.services-item').index();
                    var attr = $(el).attr('name');
                    var new_name = attr.replace(/vr-plugin-settings\[[0-9]*\]/, 'vr-plugin-settings[' + parent_index + ']');
                    $(el).attr('name', new_name);
                  })

                } else {
                  parent.remove();
                }

              })

            })(jQuery);


          </script>

          <?php
        }
        ?>

        <?php submit_button(); ?>
      </form>
    </div>
    <div class="tab-content pay-settings">
      <form method="post" enctype="multipart/form-data" action="options.php">
        <?php
        global $wp_settings_fields;

        settings_fields( VR_PLUGIN_PAYMENT_SETTINGS_PAGE ); // меняем под себя только здесь (название настроек)
        do_settings_sections( VR_PLUGIN_PAYMENT_SETTINGS_PAGE );

        if ( isset( $wp_settings_fields[VR_PLUGIN_PAYMENT_SETTINGS_PAGE] ) ) {
          $settings_groups = $wp_settings_fields[VR_PLUGIN_PAYMENT_SETTINGS_PAGE];

          $options = get_option( VR_PLUGIN_PAYMENT_SETTINGS_PAGE ) ? get_option( VR_PLUGIN_PAYMENT_SETTINGS_PAGE ) : array();

          ?>

          <?php foreach ($settings_groups[''] as $setting_id => $setting_value) : ?>

            <?php call_user_func($setting_value['callback'], array_merge($setting_value['args'], array(
              'PLUGIN_SETTINGS_TYPE' => VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
            ))); ?>

          <?php endforeach; ?>


          <?php
        }
        ?>

        <?php submit_button(); ?>
      </form>
    </div>
  </div>


</div>
