<?php

/**
 * User profile.
 * Custom fields.
 */
function vr_profile_custom_fields( $user_id ) {

  $register_service = is_string($_POST['register_service']) ? (int) $_POST['register_service'] : 0;
  $services = get_option( VR_PLUGIN_SETTINGS_PAGE );
  $selected_service = $services[$register_service];

  $current_time = current_time( 'mysql' );
  $expire_date = get_user_meta( $user_id, 'register_service_expire', true);

  update_user_meta( $user_id, 'city', sanitize_text_field($_POST['city']) );
  update_user_meta( $user_id, 'country', sanitize_text_field($_POST['country']) );
  update_user_meta( $user_id, 'agree_mailing', is_array($_POST['agree_mailing']) ? (bool) filter_var_array(filter_var_array($_POST['agree_mailing'], FILTER_VALIDATE_INT)) : false );
  update_user_meta( $user_id, 'agree_policy', is_array($_POST['agree_policy']) ? (bool) filter_var_array(filter_var_array($_POST['agree_policy'], FILTER_VALIDATE_INT)) : false );


  if (strtotime($expire_date) < strtotime($current_time)) {

    update_user_meta( $user_id, 'register_service_id', $register_service );
    update_user_meta( $user_id, 'register_service_data', json_encode($selected_service, JSON_UNESCAPED_UNICODE) );
    update_user_meta( $user_id, 'moneta_order_id', "{$_POST['login']}{$_POST['order_cust_id']}" );

  }
}

/**
 * Register form.
 * Validate.
 */
function vr_registration_validation( $firstname, $lastname, $email, $login, $rmpassword, $re_password, $city, $country, $agree_mailing, $agree_policy = null, $old_password = null )  {
  global $reg_errors;
  $reg_errors = new WP_Error;
  $agree_policy  =  is_array($_POST['agree_policy']) ? (bool) filter_var_array($_POST['agree_policy'], FILTER_VALIDATE_INT) : false;

  if ( empty( $firstname ) || empty( $lastname ) ) {
    $reg_errors->add('field', 'Не заполнены обязательные поля');
  }

  if( ! $agree_policy ) {
    $reg_errors->add( 'policy', 'Без согласия с политикой конфиденциальности не зарегистрироваться' );
  }

  if ( username_exists( $login ) ) {
    // Не проверяется в js
    $reg_errors->add('user_name', 'Пользователь с таким логином уже занят');
  }

  if ( ! validate_username( $login ) ) {
    $reg_errors->add( 'username_invalid', 'Такой логин не подойдет. Пожалуйста, используйте латиницу, цифры, точку и дефис' );
  }

  if ($rmpassword !== $re_password) {
    $reg_errors->add( 'password', 'Подтверждение не совпадает с паролем' );
  }

  if ( 5 > strlen( $rmpassword ) ) {
    $reg_errors->add( 'password', 'Пароль должен содержать не менее 6 символов' );
  }

  if ( email_exists( $email ) ) {
    // Не проверяется в js
    $reg_errors->add( 'email', 'Введенный Email уже используется' );
  }

  if ( !is_email( $email ) ) {
    $reg_errors->add( 'email_invalid', 'Некорректный Email' );
  }
}

/**
 * Register form.
 * Complete register.
 */
function vr_complete_registration() {
  global $firstname, $lastname, $email, $login, $rmpassword, $city, $country, $register_service, $reg_errors, $monetaSDK;
  if ( 1 > count( $reg_errors->get_error_messages() ) ) {

    $register_service = is_string($_POST['register_service']) ? (int) $_POST['register_service'] : 0;
    $services = get_option( VR_PLUGIN_SETTINGS_PAGE );
    $selected_service = $services[$register_service];
    $moneta_order_id = $login . $_POST['order_cust_id'];

    $monetaSDK->processCleanChoosenPaymentSystem();

    $result = $monetaSDK->showPaymentFrom($moneta_order_id, $selected_service['services_price'] . '.00', 'RUB', $selected_service['services_description']);

    $payment_args = array(
      'MNT_ID' => $result->data['accountId'],
      'MNT_TRANSACTION_ID' => $result->data['orderId'],
      'MNT_CURRENCY_CODE' => $result->data['currency'],
      'MNT_AMOUNT' => $result->data['amount'],
      'MNT_DESCRIPTION' => $result->data['description'],
      'MNT_TEST_MODE' => $result->data['testMode'],
      'MNT_SIGNATURE' => $result->data['signature'],
      'MNT_SUCCESS_URL' => $result->data['successUrl'],
      'MNT_FAIL_URL' => $result->data['failUrl'],
      'MNT_PAY_SYSTEM' => $result->data['paySystem']
    );

    $redirect_url = $result->data['action'] . '?' . http_build_query($payment_args, '', '&');

    $userdata = array(
      'first_name'    =>   $firstname,
      'last_name'     =>   $lastname,

      'user_email'    =>   $email,
      'user_login'    =>   $login,
      'user_pass'     =>   $rmpassword,

      'register_service_id' => $register_service,
      'moneta_order_id' => $moneta_order_id,

      'city'   =>   $city,
      'country'   =>   $country,
    );

    $user = wp_insert_user( $userdata );
    wp_signon( array(
      'user_login' => $login,
      'user_password' => $rmpassword,
      'remember' => true
    ) );

    if ( wp_redirect( $redirect_url ) ) {
      exit;
    }

  }
}

/**
 * Register form.
 * Main.
 */
function vr_registration_function() {
  if ( isset($_POST['submit'] ) ) {
    vr_registration_validation(
      $_POST['firstname'],
      $_POST['lastname'],

      $_POST['email'],
      $_POST['login'],
      $_POST['rmpassword'],
      $_POST['re_password'],

      $_POST['city'],
      $_POST['country'],

      $_POST['agree_mailing'],
      $_POST['agree_policy']
    );

    // sanitize user form input
    global $firstname, $lastname, $login, $rmpassword, $re_password, $email, $city, $country, $agree_mailing, $agree_policy;
    $login        =   sanitize_user( $_POST['login'] );
    $rmpassword   =   esc_attr( $_POST['rmpassword'] );
    $re_password  =   esc_attr( $_POST['re_password'] );
    $email        =   sanitize_email( $_POST['email'] );
    $firstname    =   sanitize_text_field( $_POST['firstname'] );
    $lastname     =   sanitize_text_field( $_POST['lastname'] );

    $city         =   sanitize_text_field( $_POST['city'] );
    $country      =   sanitize_text_field( $_POST['country'] );

    $register_service = esc_attr($_POST['register_service']);

    $agree_mailing =  is_array($_POST['agree_mailing']) ? (bool) filter_var_array(filter_var_array($_POST['agree_mailing'], FILTER_VALIDATE_INT)) : false;
    $agree_policy  =  is_array($_POST['agree_policy']) ? (bool) filter_var_array($_POST['agree_policy'], FILTER_VALIDATE_INT) : false;


    // call @function complete_registration to create the user
    // only when no WP_error is found
    vr_complete_registration(
      $firstname,
      $lastname,

      $email,
      $login,
      $rmpassword,

      $city,
      $country,

      $register_service,

      $agree_mailing,
      $agree_policy
    );
  }

  include( VR_PLUGIN_DIR . '/templates/register_form.php' );
}

/**
 * Shortcode, contains register form.
 */
function vr_register_form_shortcode() {
  ob_start();
  vr_registration_function();
  return ob_get_clean();
}

/**
 * User profile form.
 * Validate.
 */
function vr_user_form_validation( $firstname, $lastname, $email, $login, $rmpassword, $re_password, $city, $country, $agree_mailing, $agree_policy = null, $old_password = null )  {
  global $reg_errors;
  $reg_errors = new WP_Error;

  // user auth. change pass
  $user_id = get_current_user_id();
  $user_data = get_userdata($user_id);
  $user_meta = get_user_meta($user_id);

  if (!empty( $old_password )) {

    if (! wp_check_password( $old_password, $user_data->data->user_pass, $user_id )) {
      $reg_errors->add( 'old_pass', 'Неверный старый пароль' );
    }

    if ($rmpassword !== $re_password) {
      $reg_errors->add( 'password', 'Подтверждение не совпадает с паролем' );
    }

    if ( 5 > strlen( $rmpassword ) ) {
      $reg_errors->add( 'password', 'Пароль должен содержать не менее 6 символов' );
    }
  }

  if ( $user_data->data->user_login !== $login && username_exists( $login )) {
    $reg_errors->add('user_name', 'Пользователь с таким логином уже занят');
  }

  if ( $user_data->data->user_login !== $login && !validate_username( $login ) ) {
    $reg_errors->add( 'username_invalid', 'Такой логин не подойдет. Пожалуйста, используйте латиницу, цифры, точку и дефис' );
  }

  if ( $user_data->data->user_email !== $email && !is_email( $email ) ) {
    $reg_errors->add( 'email_invalid', 'Некорректный Email' );
  }

  if ( $user_data->data->user_email !== $email && email_exists( $email ) ) {
    // Не проверяется в js
    $reg_errors->add( 'email', 'Введенный Email уже используется' );
  }

}

/**
 * User profile form.
 * Complete User Edit.
 */
function vr_complete_user_edit() {

  if ( !wp_verify_nonce($_REQUEST['user_profile_check_nonce'], 'user_profile_check') ) {
    return false;
  }

  global $firstname, $lastname, $email, $login, $rmpassword, $city, $country, $reg_errors, $monetaSDK;
  if ( 1 > count( $reg_errors->get_error_messages() ) ) {

    $user_id = get_current_user_id();
    $register_service = is_string($_POST['register_service']) ? (int) $_POST['register_service'] : 0;
    $services = get_option( VR_PLUGIN_SETTINGS_PAGE );
    $selected_service = $services[$register_service];
    $moneta_order_id = $login . $_POST['order_cust_id'];
    $current_time = current_time( 'mysql' );
    $expire_date = get_user_meta( $user_id, 'register_service_expire', true);

    $monetaSDK->processCleanChoosenPaymentSystem();

    $result = $monetaSDK->showPaymentFrom($moneta_order_id, $selected_service['services_price'] . '.00', 'RUB', $selected_service['services_description']);

    $payment_args = array(
      'MNT_ID' => $result->data['accountId'],
      'MNT_TRANSACTION_ID' => $result->data['orderId'],
      'MNT_CURRENCY_CODE' => $result->data['currency'],
      'MNT_AMOUNT' => $result->data['amount'],
      'MNT_DESCRIPTION' => $result->data['description'],
      'MNT_TEST_MODE' => $result->data['testMode'],
      'MNT_SIGNATURE' => $result->data['signature'],
      'MNT_SUCCESS_URL' => $result->data['successUrl'],
      'MNT_FAIL_URL' => $result->data['failUrl'],
      'MNT_PAY_SYSTEM' => $result->data['paySystem']
    );

    $redirect_url = $result->data['action'] . '?' . http_build_query($payment_args, '', '&');

    $userdata = array(
      'ID'            =>   $user_id,
      'first_name'    =>   $firstname,
      'last_name'     =>   $lastname,

      'user_email'    =>   $email,
      'user_login'    =>   $login,
      'user_pass'     =>   $rmpassword,

      'city'   =>   $city,
      'country'   =>   $country,
    );
    $user = wp_update_user( $userdata  );

    if ( (strtotime($expire_date) < strtotime($current_time)) && isset($_POST['register_service']) ) {
      if ( wp_redirect( $redirect_url ) ) {
        exit;
      }
    }

    //print $redirect_url;


  }
}

/**
 * User profile form.
 * Main.
 */
function vr_user_form_function() {
  if ( isset($_POST['submit'] ) ) {
    vr_user_form_validation(
      $_POST['firstname'],
      $_POST['lastname'],

      $_POST['email'],
      $_POST['login'],
      $_POST['rmpassword'],
      $_POST['re_password'],

      $_POST['city'],
      $_POST['country'],

      $_POST['agree_mailing'],
      $_POST['agree_policy'],
      $_POST['old_password']
    );

    // sanitize user form input
    global $firstname, $lastname, $login, $rmpassword, $re_password, $email, $city, $country, $agree_mailing, $agree_policy;
    $login        =   sanitize_user( $_POST['login'] );
    $rmpassword   =   esc_attr( $_POST['rmpassword'] );
    $re_password  =   esc_attr( $_POST['re_password'] );
    $email        =   sanitize_email( $_POST['email'] );
    $firstname    =   sanitize_text_field( $_POST['firstname'] );
    $lastname     =   sanitize_text_field( $_POST['lastname'] );

    $city         =   sanitize_text_field( $_POST['city'] );
    $country      =   sanitize_text_field( $_POST['country'] );

    $agree_mailing =  is_array($_POST['agree_mailing']) ? (bool) filter_var_array(filter_var_array($_POST['agree_mailing'], FILTER_VALIDATE_INT)) : false;
    $agree_policy  =  is_array($_POST['agree_policy']) ? (bool) filter_var_array($_POST['agree_policy'], FILTER_VALIDATE_INT) : false;


    // call @function complete_registration to create the user
    // only when no WP_error is found
    vr_complete_user_edit(
      $firstname,
      $lastname,

      $email,
      $login,
      $rmpassword,

      $city,
      $country,

      $agree_mailing,
      $agree_policy
    );
  }

  include( VR_PLUGIN_DIR . '/templates/user_form.php' );
}

/**
 * Shortcode, contains user profile form.
 */
function vr_user_form_shortcode() {
  ob_start();
  vr_user_form_function();
  return ob_get_clean();
}
