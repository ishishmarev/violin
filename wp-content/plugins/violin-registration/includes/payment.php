<?php

include_once(VR_PLUGIN_DIR . "/moneta-sdk-lib/autoload.php");
global $monetaSDK;

$moneta_settings = get_option( VR_PLUGIN_PAYMENT_SETTINGS_PAGE );
$monetaSDK = new \Moneta\MonetaSdk();

// Test mode
$monetaSDK->setSettingValue('monetasdk_demo_mode', isset($moneta_settings['monetasdk_demo_mode']) ? $moneta_settings['monetasdk_demo_mode'] : '0');
$monetaSDK->setSettingValue('monetasdk_test_mode', isset($moneta_settings['monetasdk_test_mode']) ? $moneta_settings['monetasdk_test_mode'] : '0');
$monetaSDK->setSettingValue('monetasdk_debug_mode', isset($moneta_settings['monetasdk_debug_mode']) ? $moneta_settings['monetasdk_debug_mode'] : '0');

// Connect settings.
$monetaSDK->setSettingValue('monetasdk_account_id', isset($moneta_settings['monetasdk_account_id']) ? $moneta_settings['monetasdk_account_id'] : ''); // номер расширенного счета для приема платежей интернет-магазина
$monetaSDK->setSettingValue('monetasdk_account_code', isset($moneta_settings['monetasdk_account_code']) ? $moneta_settings['monetasdk_account_code'] : ''); // код проверки целостности данных. Должен быть идентичным тому, что установлен в настройках используемого расширенного счета
$monetaSDK->setSettingValue('monetasdk_account_pay_password_enrypted', isset($moneta_settings['monetasdk_account_pay_password_enrypted']) ? $moneta_settings['monetasdk_account_pay_password_enrypted'] : ''); // закриптованный платежный пароль.
$monetaSDK->setSettingValue('monetasdk_account_username', isset($moneta_settings['monetasdk_account_username']) ? $moneta_settings['monetasdk_account_username'] : ''); // логин аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина.
$monetaSDK->setSettingValue('monetasdk_account_password', isset($moneta_settings['monetasdk_account_password']) ? $moneta_settings['monetasdk_account_password'] : ''); // пароль аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина
$monetaSDK->setSettingValue('monetasdk_prototype_user_unit_id', isset($moneta_settings['monetasdk_prototype_user_unit_id']) ? $moneta_settings['monetasdk_prototype_user_unit_id'] : ''); // ID юнита в системе монета.ру для использования в качестве прототипа при создании нового профайла пользователя

// Storage.
$monetaSDK->setSettingValue('monetasdk_storage_type', "mysql");
$monetaSDK->setSettingValue('monetasdk_storage_mysql_host', DB_HOST);
$monetaSDK->setSettingValue('monetasdk_storage_mysql_username', DB_USER);
$monetaSDK->setSettingValue('monetasdk_storage_mysql_password', DB_PASSWORD);
$monetaSDK->setSettingValue('monetasdk_storage_mysql_port', "3306"); // @TODO.
$monetaSDK->setSettingValue('monetasdk_storage_mysql_database', DB_NAME);

// URLS
$monetaSDK->setSettingValue('monetasdk_success_url', home_url(VR_PAYMENT_URL));
$monetaSDK->setSettingValue('monetasdk_fail_url', home_url(VR_PAYMENT_URL));
$monetaSDK->setSettingValue('monetasdk_inprogress_url', home_url(VR_PAYMENT_URL));
$monetaSDK->setSettingValue('monetasdk_return_url', home_url(VR_PAYMENT_URL));

$_qwe = 0;

// example dynamic settings.
//$monetaSDK->setSettingValue('monetasdk_account_id', $mnt_id);

