<?php

/**
 * Hook register_activation_hook.
 */
function vr_plugin_activation() {
	if (!current_user_can( 'activate_plugins') ) {
		return;
	}

	if ( empty(vr_get_page_by_url(VR_REG_URL)) ) {
		$current_user = wp_get_current_user();

		$register_page_content = "
			<h2>Как стать членом клуба?</h2>
			<p>Мы хотим, чтобы вы думали, что ваше членство в Скрипичной лаборатории было лучшими деньгами, которые вы когда-либо тратили!</p>
			<p>Скрипка Лаборатория не абонентская услуга! Вы выбираете срок, который соответствует вашему бюджету,и в конце этого срока ваше членство истечет.</p>
		";

		// create post object
		$register_page = array(
			'post_title'    => 'Регистрация',
			'post_content'  => $register_page_content,
			'post_name'     => VR_REG_URL,
			'post_status'   => 'publish',
			'post_author'   => $current_user->ID,
			'post_type'     => 'page',
			'meta_input'    => array( 'form' => '[vr_register_form]' )
		);

		// insert the post into the database
		wp_insert_post( $register_page );

		// create post object
		$user_page = array(
			'post_title'    => 'Профиль пользователя',
			'post_name'     => VR_USER_URL,
			'post_status'   => 'publish',
			'post_author'   => $current_user->ID,
			'post_type'     => 'page',
			'meta_input'    => array( 'form' => '[vr_user_form]' )
		);

		// insert the post into the database
		wp_insert_post( $user_page );

    $payment_page = array(
      'post_title'    => 'Оплата',
      'post_name'     => VR_PAYMENT_URL,
      'post_status'   => 'publish',
      'post_author'   => $current_user->ID,
      'post_type'     => 'page',
      'meta_input'    => array( 'form' => '[vr_register_form]' )
    );

    // insert the post into the database
    wp_insert_post( $payment_page );

    $payment_page = array(
      'post_title'    => 'Проверка оплаты',
      'post_name'     => VR_PAYMENT_CHECK_URL,
      'post_status'   => 'publish',
      'post_author'   => $current_user->ID,
      'post_type'     => 'page',
    );

    // insert the post into the database
    wp_insert_post( $payment_page );
	}
}

/**
 * Hook register_deactivation_hook.
 */
function vr_plugin_deactivation() {
	if (!current_user_can( 'activate_plugins') ) {
		return;
	}

	$register_page = vr_get_page_by_url(VR_REG_URL);
	if ( !empty($register_page) ) {
		wp_delete_post( $register_page, true );
	}

	$user_page = vr_get_page_by_url(VR_USER_URL);
	if ( !empty($user_page) ) {
		wp_delete_post( $user_page, true );
	}

  $payment_page = vr_get_page_by_url(VR_PAYMENT_URL);
  if ( !empty($payment_page) ) {
    wp_delete_post( $payment_page, true );
  }

  $payment_check_page = vr_get_page_by_url(VR_PAYMENT_CHECK_URL);
  if ( !empty($payment_page) ) {
    wp_delete_post( $payment_page, true );
  }
}

/**
 * Include templates.
 */
function vr_page_templates( $template ) {
  $pagename = get_query_var('pagename');

  if ( in_array(get_query_var('pagename'), array(VR_REG_URL, VR_USER_URL, VR_PAYMENT_URL, VR_PAYMENT_CHECK_URL)) ) {

    $current_term = get_query_var('term');

    if ( $theme_file = locate_template( array ( "custom_pages/page-$pagename.php" ) ) ) {
      $template = $theme_file;
    } else {
      $template = VR_PLUGIN_DIR . "/templates/pages/page-$pagename.php";
    }

    return $template;
  }

  return $template;
}

/**
 * AJAX auth.
 */
function vr_ajax_login(){
  check_ajax_referer( 'violinlab-security', 'sec' );

  // Получаем данные POST и выводим результат
  $info = array();
  $info['user_login'] = $_POST['log'];
  $info['user_password'] = $_POST['pwd'];
  $info['remember'] = true;

  $user_signon = wp_signon( $info, false );
  if ( is_wp_error($user_signon) ){
    echo json_encode(
      array(
        'log' => false,
        'mes' => 'Неверное имя или пароль.',
      )
    );
  } else {
    echo json_encode(
      array(
        'log' =>true,
        'mes' => 'Вход выполнен, ожидайте...'
      )
    );
  }

  die();
}

/**
 * Plugin admin menu.
 */
function admin_add_menu() {

	//create new top-level menu
	add_menu_page(
		'Страница регистрации',
		'Страница регистрации',
		'manage_options',
		VR_PLUGIN_DIR,
		'vr_settings_page',
		null
	);

}

function vr_settings_page() {
  include_once VR_PLUGIN_DIR . '/includes/admin_page.php';
}

function vr_register_settings() {

  /**
   * Main plugin settings
   */
  add_settings_field(
    'services_expire',
    'Значение через сколько истекает период услуги в месяцах',
    'vr_display_settings',
    VR_PLUGIN_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'number',
      'step'      => '1',
      'id'        => 'services_expire',
      'desc'      => 'Значение через сколько истекает период услуги в месяцах'
    )
  );
  add_settings_field(
    'services_price',
    'Цена услуги',
    'vr_display_settings',
    VR_PLUGIN_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'number',
      'step'      => '1',
      'id'        => 'services_price',
      'desc'      => 'Цена услуги'
    )
  );
  add_settings_field(
    'services_title',
    'Заголовок',
    'vr_display_settings',
    VR_PLUGIN_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'services_title',
      'desc'      => 'Заголовок для отображения пользователю'
    )
  );
  add_settings_field(
    'services_description',
    'Описание услуги',
    'vr_display_settings',
    VR_PLUGIN_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'textarea',
      'id'        => 'services_description',
      'desc'      => 'Описание услуги'
    )
  );

  /**
   * Payment plugin settings.
   * Debug settings.
   */
  add_settings_field(
    'monetasdk_demo_mode',
    'Демо режим',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'number',
      'step'      => '1',
      'id'        => 'monetasdk_demo_mode',
      'desc'      => 'Принимает одно из значений “1” или “0”. При включении SDK будет работать с demo сервером moneta.ru, если опция выключена - с продакшен сервером.'
    )
  );
  add_settings_field(
    'monetasdk_test_mode',
    'Тестовый режим',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'number',
      'step'      => '1',
      'id'        => 'monetasdk_test_mode',
      'desc'      => '“1” или “0”. Включение тестового режима взаимодействия с moneta.ru'
    )
  );
  add_settings_field(
    'monetasdk_debug_mode',
    'Отладочное логгирование',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'number',
      'step'      => '1',
      'id'        => 'monetasdk_debug_mode',
      'desc'      => '“1” или “0”. Включает отладочное логгирование.'
    )
  );

  /**
   * Payment plugin settings.
   * Account settings.
   */
  add_settings_field(
    'monetasdk_account_id',
    'Номер расширенного счета',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_account_id',
      'desc'      => 'Номер расширенного счета для приема платежей интернет-магазина'
    )
  );
  add_settings_field(
    'monetasdk_account_code',
    'Код проверки целостности данных',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_account_code',
      'desc'      => 'Код проверки целостности данных. Должен быть идентичным тому, что установлен в настройках используемого расширенного счета'
    )
  );
  add_settings_field(
    'monetasdk_account_pay_password_enrypted',
    'Закриптованный платежный пароль',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_account_pay_password_enrypted',
      'desc'      => 'Закриптованный платежный пароль'
    )
  );
  add_settings_field(
    'monetasdk_account_username',
    'Логин аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_account_username',
      'desc'      => 'Логин аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина'
    )
  );
  add_settings_field(
    'monetasdk_account_password',
    'Пароль аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_account_password',
      'desc'      => 'Пароль аккаунта moneta.ru, имеющего доступ к расширенному счету интернет-магазина'
    )
  );
  add_settings_field(
    'monetasdk_prototype_user_unit_id',
    'ID юнита в системе монета.ру для использования в качестве прототипа при создании нового профайла пользователя',
    'vr_display_settings',
    VR_PLUGIN_PAYMENT_SETTINGS_PAGE,
    null,
    array(
      'type'      => 'text',
      'id'        => 'monetasdk_prototype_user_unit_id',
      'desc'      => 'ID юнита в системе монета.ру для использования в качестве прототипа при создании нового профайла пользователя'
    )
  );

  register_setting( VR_PLUGIN_SETTINGS_PAGE, VR_PLUGIN_SETTINGS_PAGE ); // Main plugin settings
  register_setting( VR_PLUGIN_PAYMENT_SETTINGS_PAGE, VR_PLUGIN_PAYMENT_SETTINGS_PAGE ); // Payment plugin settings



}
add_action( 'admin_init', 'vr_register_settings' );

function vr_display_settings($args) {

  $plugin_settings_type = ( ! empty( $args['PLUGIN_SETTINGS_TYPE'] ) ) ? $args['PLUGIN_SETTINGS_TYPE'] : VR_PLUGIN_SETTINGS_PAGE;
  $option_group = ( isset( $args['option_group_id'] ) ) ? "[{$args['option_group_id']}]" : "";
  $__option = get_option( $plugin_settings_type );

  $value = '';
  if (isset($args['option_group_id'])) {
    $value = (!empty($__option[$args['option_group_value_id']][$args['id']])) ? $__option[$args['option_group_value_id']][$args['id']] : '';
  } else {
    $value = (!empty($__option[$args['id']])) ? $__option[$args['id']] : '';
  }

  print "<div class='services-field'>";

  switch ( $args['type'] ) {
    case 'text':
      print "<label>";
      print "<div class='services-field-description'>{$args['desc']}</div>";
      print "<div class='services-field-value'>";
      print "<input class='regular-text' type='text' id='{$args['id']}' name='" . $plugin_settings_type . $option_group . "[{$args['id']}]' value='{$value}' />";
      print "</div>";
      print "</label>";
      break;
    case 'number':
      print "<label>";
      print "<div class='services-field-description'>{$args['desc']}</div>";
      print "<div class='services-field-value'>";
      print "<input class='regular-text' type='number' step='{$args['step']}' id='{$args['id']}' name='" . $plugin_settings_type . $option_group . "[{$args['id']}]' value='{$value}' />";
      print "</div>";
      print "</label>";
      break;
    case 'textarea':
      print "<label>";
      print "<div class='services-field-description'>{$args['desc']}</div>";
      print "<div class='services-field-value'>";
      print "<textarea class='regular-text' type='number' step='100' id='{$args['id']}' name='" . $plugin_settings_type . $option_group . "[{$args['id']}]' >";
      print $value;
      print "</textarea>";
      print "</div>";
      print "</label>";
      break;
  }

  print "</div>";

}

/**
 * Hide plugin register page and user page from pages list.
 */
function vr_hide_register_page_from_default( $query ) {
	if( !is_admin() )
		return $query;
	global $pagenow;
	if( 'edit.php' == $pagenow && ( get_query_var('post_type') && 'page' == get_query_var('post_type') ) )
		$query->set( 'post__not_in', array(
			vr_get_page_by_url(VR_REG_URL),
			vr_get_page_by_url(VR_USER_URL),
			vr_get_page_by_url(VR_PAYMENT_URL)
		));
	return $query;
}

/**
 * Plugin mce include.
 */
function vr_mce_external_plugins($plugins) {
	if ( version_compare( get_bloginfo( 'version' ), '3.9', '>=' ) ) {
		$plugins['code'] = VR_PLUGIN_DIR . '/assets/js/tinymce/code.min.js';
	}
	return $plugins;
}

/**
 * Change default register page url to plugin register page.
 */
function vr_register_page_url( $register_url ) {
	return get_permalink( vr_get_page_by_url(VR_REG_URL) );
}

/**
 * Close default registration page.
 * Redirect to plugin registration page.
 */
function vr_init_redirect() {

	if(!isset($_SERVER["REQUEST_URI"])) {
		return;
	}

	$file = basename($_SERVER["REQUEST_URI"]);

	if(substr($file, 0, 12) != "wp-login.php") {
		return;
	}

	if(isset($_GET["action"])) {
		$action = $_GET["action"];
	} else {
		$action = "login";
	}

	if(isset($_GET["redirect_to"])) {
		$redirect = $_GET["redirect_to"];
	} else {
		$redirect = "";
	}

	switch ($action) {
		case "register":
			$url = site_url(VR_REG_URL);
			break;
		case "login":
		case "lostpassword":
		case "logout":
		default:
			$url = null;
	}

	if($url) {
		wp_redirect($url);
		exit;
	}
}

/**
 * Plugin core function.
 * Get registration page.
 * @return string.
 */
function vr_get_page_by_url($page_url) {
	global $wpdb;

	$query = "SELECT ID, post_name FROM {$wpdb->prefix}posts WHERE post_name = '" . $page_url . "'";
	$register_page = $wpdb->get_row($query, 'ARRAY_A');

	if ( null === $register_page ) {
		return '';
	}
	return $register_page['ID'];
}
