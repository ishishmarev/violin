<?php
/*
Plugin Name: Violin Forum
Plugin URI:
Description:
Version: 0.1
Author: ishishmarev
Author URI:
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define( 'VF_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'VF_PLUGIN_URL', plugins_url( 'violin-forum' ) );
define( 'VF_PAGE_URL', 'forum' );
define( 'VF_QUESTION_POST_TYPE', 'vf-forum-question' );
define( 'VF_ANSWER_POST_TYPE', 'vf-forum-answer' );
define( 'VF_FORUM_PAGINATION', 10 );
define( 'VF_ANSWERS_PAGINATION', 10 );

include_once VF_PLUGIN_DIR . '/includes/functions.php';

// Register content type for Violin Materials.
add_action( 'init', 'vf_plugin_init' );

// Add metaboxes to content type Violin Forum Answer.
add_action( 'add_meta_boxes', 'vf_answer_metabox' );

// Save metaboxes content type Violin Forum Answer.
add_action( 'save_post', 'vf_answer_metabox_save', 1, 2 );

// Header menu set active links.
add_action( 'nav_menu_css_class', 'vf_forum_set_active_link', 10, 2 );

// Register shortcode, contains register form. [vr_register_form]
add_shortcode( 'vf_forum_form', 'vf_forum_form_shortcode' );

// Templates for Violin Materials.
add_filter( 'template_include', 'vf_forum_template' );

// Forums per page.
add_action( 'pre_get_posts', 'vf_forums_per_page' );

// Filter Answers by Forum theme in Admin.
add_action( 'restrict_manage_posts', 'vf_filter_answers_by_forum_theme' , 10, 2);

// Query rewrite for Filter Answers by Forum theme in Admin.
add_filter( 'parse_query', 'vf_query_rewrite_for_filter_answers' );
