<?php

/**
 * Init plugin hook.
 */
function vf_plugin_init() {
  // Answers pagination
  add_rewrite_tag( '%pager%', '([^&]+)' );
  add_rewrite_rule( 'forum/([^/]+)/pager/([0-9]{1,})/?', 'index.php??vf-forum-question=$matches[1]&pager=$matches[2]','top' );

  // Register post types
  vf_register_forum_theme_type();
  vf_register_forum_answer_type();
  flush_rewrite_rules();
}

/**
 * Function for create `vf-forum-question` post type.
 */
function vf_register_forum_theme_type() {

  $labels = array(
    'name' => 'Форум',
    'singular_name' => 'Форум',
    'add_new' => 'Создать новую тему',
    'add_new_item' => 'Создать новую тему',
    'edit_item' => 'Редактировать тему',
    'new_item' => 'Создать новую тему',
    'view_item' => 'Посмотреть тему',
    'search_items' => 'Поиск сообщений форума',
    'not_found' => 'Сообщений форума не найдено',
    'not_found_in_trash' => 'Сообщений форума в корзине не найдено',
    'parent_item_colon' => 'Предок: Форум',
    'menu_name' => 'Форум',
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'description',
    'taxonomies' => array (),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => NULL,
    'menu_icon' => NULL,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => array (
      'slug' => VF_PAGE_URL,
      'hierarchical' => true,
      'with_front' => false,
    ),
    'capability_type' => 'post',
    'supports' => array (
      'title',
      'editor',
      'author',
      'thumbnail',
      'revisions',
      'page-attributes',
    ),
  );

  register_post_type( VF_QUESTION_POST_TYPE, $args );
}

/**
 * Function for create `vf-forum-answer` post type.
 */
function vf_register_forum_answer_type() {

	$labels = array(
    'name' => 'Ответы',
    'singular_name' => 'Ответ',
    'add_new' => 'Добавить новый ответ',
    'add_new_item' => 'Добавить новый ответ',
    'edit_item' => 'Редактировать ответ',
    'new_item' => 'Добавить новый ответ',
    'view_item' => 'Посмотреть ответ',
    'search_items' => 'Поиск ответов',
    'not_found' => 'Ответов не найдено',
    'not_found_in_trash' => 'Ответов в корзине не найдено',
    'parent_item_colon' => 'Предок: Ответ',
    'menu_name' => 'Ответы'
	);

	$args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'description',
    'taxonomies' => array (),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => 'edit.php?post_type=' . VF_QUESTION_POST_TYPE,
    'show_in_admin_bar' => true,
    'menu_position' => NULL,
    'menu_icon' => NULL,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'supports' => array (
      'title',
      'editor',
      'author',
      'page-attributes',
    ),
  );

	register_post_type( VF_ANSWER_POST_TYPE, $args );
}

/**
 * Adds a metabox to the right side of the screen under “Publish” the box
 */
function vf_answer_metabox() {
  add_meta_box(
    'vf-forum-answer-metabox',
    'ID Темы форума',
    'vf_answer_metabox_output',
    VF_ANSWER_POST_TYPE,
    'side'
  );
}

/**
 * Output the HTML for the metabox.
 */
function vf_answer_metabox_output() {
  global $post;

  // Nonce field to validate form request came from current site
  wp_nonce_field( basename( __FILE__ ), 'verify_forum_theme_id' );

  $forum_theme_id = get_post_meta( $post->ID, 'forum_theme_id', true ) ? get_post_meta( $post->ID, 'forum_theme_id', true ) : 0;
  ?>
  <p>
    <strong>ID</strong>
  </p>
  <p>
    <label class="screen-reader-text">ID</label>
    <input name="forum_theme_id" type="text" size="4" id="forum_theme_id" value="<?php echo (int) $forum_theme_id ?>">
  </p>
  <?php
}

/**
 * Save the metabox data
 */
function vf_answer_metabox_save( $post_id, $post ) {
  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return $post_id;
  }

  if ( ! isset( $_POST['forum_theme_id'] ) || ! wp_verify_nonce( $_POST['verify_forum_theme_id'], basename(__FILE__) ) ) {
    return $post_id;
  }

  $forum_theme_id = esc_textarea( $_POST['forum_theme_id'] );

  if ( 'revision' === $post->post_type ) {
    return '';
  }
  if ( get_post_meta( $post_id, 'forum_theme_id', false ) ) {
    update_post_meta( $post_id, 'forum_theme_id', $forum_theme_id );
  } else {
    add_post_meta( $post_id, 'forum_theme_id', $forum_theme_id);
  }
  if ( !$forum_theme_id ) {
    delete_post_meta( $post_id, 'forum_theme_id' );
  }

  return $post_id;
}

/**
 * Header menu set active links.
 */
function vf_forum_set_active_link($classes, $item) {
  global $post;

  if ( is_object( $post ) && $post->post_type == VF_QUESTION_POST_TYPE && $item->object === VF_QUESTION_POST_TYPE ){
    $classes[] = 'current-page-ancestor';
  }

  return $classes;

}

/**
 * Forum forms.
 */
function vf_forum_forms() {
  if ( isset($_POST['submit'] ) ) {
    $current_user = get_current_user_id();
    $current_post = get_post();

    $args = array();
    if ( is_archive() ) {

      $args = array(
        'post_title'    => wp_strip_all_tags($_POST['theme']),
        'post_content'  => $_POST['message'],
        'post_status'   => 'publish',
        'post_author'   => $current_user,
        'post_type'     => VF_QUESTION_POST_TYPE
      );

    } else {

      $args = array(
        'post_title'    => $current_post->post_title,
        'post_content'  => $_POST['message'],
        'post_status'   => 'publish',
        'post_author'   => $current_user,
        'post_type'     => VF_ANSWER_POST_TYPE,
        'meta_input'    => array(
          'forum_theme_id' => $current_post->ID
        ),
      );

    }

    wp_insert_post($args);
    wp_redirect( is_archive() ? get_post_type_archive_link( VF_QUESTION_POST_TYPE ) : get_post_permalink( $current_post ) ) ;

  }

  include( VF_PLUGIN_DIR . '/templates/forum-form.php' );
}

/**
 * Shortcode, contains register form.
 */
function vf_forum_form_shortcode() {
  ob_start();
  vf_forum_forms();
  return ob_get_clean();
}

/**
 * Include templates.
 */
function vf_forum_template( $template ) {
  if ( is_singular( VF_QUESTION_POST_TYPE ) ) {

    if ( $theme_file = locate_template( array ( 'forum-single.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VF_PLUGIN_DIR . 'templates/forum-single.php';
    }

    return $template;
  }

  if ( is_post_type_archive(VF_QUESTION_POST_TYPE) ) {

    if ( $theme_file = locate_template( array ( 'forum.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VF_PLUGIN_DIR . 'templates/forum.php';
    }

    return $template;
  }

  return $template;
}

/**
 * Filter Answers by Forum Theme
 */
function vf_filter_answers_by_forum_theme( $post_type, $which ) {
  if ( VF_ANSWER_POST_TYPE !== $post_type )
    return;

  $forums = get_posts(array(
    'post_type'=> VF_QUESTION_POST_TYPE,
    'status' => 'publish'
  ));

  print "<select name='answers' id='answers' class='postform'>";
  print '<option value="">Показать все</option>';

  foreach ( $forums as $topic ) {

    $answers = get_posts(array(
      'post_type'       => VF_ANSWER_POST_TYPE,
      'meta_query' => array(
        array(
          'key' => 'forum_theme_id',
          'value' => $topic->ID
        )
      )
    ));

    printf(
      '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
      $topic->ID,
      ( ( isset( $_GET['answers'] ) && ( $_GET['answers'] == $topic->ID ) ) ? ' selected="selected"' : '' ),
      $topic->post_title,
      count($answers)
    );
  }

  print '</select>';
}

/**
 * Query rewrite for Filter Answers by Forum theme in Admin.
 */
function vf_query_rewrite_for_filter_answers( $query ){
  global $pagenow;

  if (isset($_GET['post_type'])) {
    $post_type = $_GET['post_type'];
  }

  if ( VF_ANSWER_POST_TYPE == $post_type && is_admin() && $pagenow=='edit.php' && isset($_GET['answers']) && $_GET['answers'] != '') {
    $query->query_vars['meta_key'] = 'forum_theme_id';
    $query->query_vars['meta_value'] = $_GET['answers'];
  }

}

/**
 * Hook pre_get_posts, posts per page.
 */
function vf_forums_per_page( $query ) {
  if ( !is_admin() && $query->is_main_query() && $query->is_post_type_archive(VF_QUESTION_POST_TYPE) ) {
    $query->set( 'posts_per_page', VF_FORUM_PAGINATION );
  }
}

/**
 * Склонение существительных после числительных.
 * $num чисо, от которого будет зависеть форма слова
 * $form_for_1 первая форма слова, например Товар
 * $form_for_2 вторая форма слова - Товара
 * $form_for_5 третья форма множественного числа слова - Товаров
 */
function true_wordform($num, $form_for_1, $form_for_2, $form_for_5){
  $num = abs($num) % 100;
  $num_x = $num % 10;
  if ($num > 10 && $num < 20)
    return $form_for_5;
  if ($num_x > 1 && $num_x < 5)
    return $form_for_2;
  if ($num_x == 1)
    return $form_for_1;
  return $form_for_5;
}
