<?php
/**
 * The template for forum post type
 */
get_header();

/**
 * Pagination
 */
$cur_page = get_query_var('paged');
$paged = 1;

if ($cur_page && is_int($cur_page)) {
  $paged = $cur_page;
}
$total_pages = $wp_query->max_num_pages;

?>

  <div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <div class="head__helper">
            <h1 class="head__title">Общение</h1>
            <div class="helper">
              <div class="helper__main">
                <a href="#">Как задать вопрос?</a>
              </div>
              <?php if ( have_posts() ) : ?>
              <div class="helper__description">
                <span><?php print $wp_query->found_posts . " " . true_wordform( $wp_query->found_posts, 'тема', 'темы', 'тем' ); ?></span>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">

        <?php if ( have_posts() ) : ?>
          <div class="section__nav section__nav--top forum__nav">
          <?php if ($total_pages > 1): ?>
            <div class="pager__nav">
              <ul class="pager__nav--list">

                <?php if ($paged > 1): ?>
                  <li class="pager__nav--item pager__nav--prev">
                    <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . ($paged - 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                    <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                  </svg>
                    </a>
                  </li>

                  <?php if (($paged - 1) !== 1): ?>
                    <li class="pager__nav--item pager__nav--first">
                      <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/1/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                        <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                          <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                      </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                <?php endif; ?>

                <?php for ($i=1; $i<=$total_pages; $i++): ?>
                  <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                    <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . $i . '/'; ?>"><?php print $i; ?></a>
                  </li>
                <?php endfor; ?>

                <?php if ($paged < $total_pages): ?>

                  <?php if (($paged + 1) != $total_pages): ?>
                    <li class="pager__nav--item pager__nav--last">
                      <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . $total_pages . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                        <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                          <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                      </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                  <li class="pager__nav--item pager__nav--next">
                    <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . ($paged + 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                    <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                  </svg>
                    </a>
                  </li>
                <?php endif; ?>

              </ul>
            </div>
          <?php endif; ?>
            <div class="section__nav--help">
              <a class="btn btn--big" href="#">Задать вопрос</a>
            </div>
          </div>
        <?php endif; ?>

        <div class="forum">

          <div class="forum__reply <?php print have_posts() ? 'forum__reply--hide' : ''; ?>">
          <?php print do_shortcode('[vf_forum_form]'); ?>
          </div>

          <?php if (have_posts()) : ?>
          <div class="forum__list">
          <?php while ( have_posts() ) : the_post(); ?>

            <?php
              global $wpdb;

              $forum_user_data = get_userdata($post->post_author);

              $forum_answers_sql = "
              SELECT count({$wpdb->prefix}posts.ID)
              FROM {$wpdb->prefix}posts
              INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
              AND (( {$wpdb->prefix}postmeta.meta_key = 'forum_theme_id' AND {$wpdb->prefix}postmeta.meta_value = {$post->ID} )) 
              AND {$wpdb->prefix}posts.post_type = 'vf-forum-answer'
              AND ({$wpdb->prefix}posts.post_status = 'publish')
              ";

              $forum_answers_count = (int) $wpdb->get_var($forum_answers_sql);
            ?>

            <div class="forum__item">
              <div class="forum__body">

                <div class="forum__user user">
                  <div class="user__photo">
                    <?php print get_avatar($forum_user_data->ID, 150, null, get_user_meta($forum_user_data->ID, 'first_name', true)); ?>
                  </div>
                  <div class="user__name"><?php print get_user_meta($forum_user_data->ID, 'first_name', true); ?></div>
                </div>

                <div class="forum__content">

                  <div class="forum__message">
                    <div class="forum__title"><?php the_title(); ?></div>
                    <div class="forum__text"><?php the_content(); ?></div>
                  </div>

                  <div class="forum__footer">
                    <div class="forum__action">
                      <div class="forum__btn">
                        <a class="btn btn--forum" href="<?php the_permalink(); ?>">
                          <?php if ($forum_answers_count) : ?>
                          Показать дискуссию
                          <?php else: ?>
                          Ответить
                          <?php endif; ?>
                        </a>
                      </div>
                      <div class="forum__answers">
                        <?php
                        if ($forum_answers_count) {
                          print $forum_answers_count . " " . true_wordform( $forum_answers_count, 'ответ', 'ответа', 'ответов' );
                        } else {
                          print "Нет ответов";
                        }
                        ?>
                      </div>
                    </div>
                    <div class="forum__signature">
                      <div class="forum__publish"><?php print get_the_date( get_option( 'date_format' ), $post->ID ); ?></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          <?php endwhile; ?>
          </div>
          <?php endif; ?>

        </div>

        <?php if ( have_posts() ) : ?>
          <?php if ($total_pages > 1): ?>
            <div class="forum__pager">
              <div class="pager__nav">
                <ul class="pager__nav--list">

                  <?php if ($paged > 1): ?>
                    <li class="pager__nav--item pager__nav--prev">
                      <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . ($paged - 1) . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                    </svg>
                      </a>
                    </li>

                    <?php if (($paged - 1) !== 1): ?>
                      <li class="pager__nav--item pager__nav--first">
                        <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/1/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                            <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                        </svg>
                        </a>
                      </li>
                    <?php endif; ?>

                  <?php endif; ?>

                  <?php for ($i=1; $i<=$total_pages; $i++): ?>
                    <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                      <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . $i . '/'; ?>"><?php print $i; ?></a>
                    </li>
                  <?php endfor; ?>

                  <?php if ($paged < $total_pages): ?>

                    <?php if (($paged + 1) != $total_pages): ?>
                      <li class="pager__nav--item pager__nav--last">
                        <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . $total_pages . '/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                            <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                        </svg>
                        </a>
                      </li>
                    <?php endif; ?>

                    <li class="pager__nav--item pager__nav--next">
                      <a class="pager__nav--link" href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE) . 'page/' . ($paged + 1) . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                    </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                </ul>
              </div>
            </div>
          <?php endif; ?>
        <?php endif; ?>

      </div>
    </section>

  </div>

  <script>
    (function($) {
      var navHelp = $('.forum__nav .section__nav--help');
      var forumReply = $('.forum__reply');

      navHelp.find('a').on('click', function(e) {

        e.preventDefault();
        navHelp.fadeOut();
        forumReply.slideDown(function(){
          $(this).removeClass('forum__reply--hide');
        });

      });

      forumReply.find('.about__reviews--cancel').on('click', function(e) {

        e.preventDefault();
        navHelp.fadeIn();
        forumReply.slideUp(function(){
          $(this).addClass('forum__reply--hide');
        });

      })

    })(jQuery);
  </script>

<?php get_footer();
