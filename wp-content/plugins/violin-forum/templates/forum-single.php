<?php
/**
 * The template for forum post type
 */
get_header();

$forum_post_id = $post->ID;

?>

  <div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <div class="head__helper">
            <h1 class="head__title">Вопрос</h1>
            <div class="helper">
              <div class="helper__main--back">
                <a href="<?php print get_post_type_archive_link(VF_QUESTION_POST_TYPE); ?>">
                  <svg width="10.5px" height="19.2px">
                    <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "></polygon>
                  </svg>
                  <span>Вернуться к вопросам</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">

        <div class="forum">
          <div class="forum__list">
            <?php while ( have_posts() ) : the_post(); ?>

              <?php
              global $wpdb;

              $forum_user_data = get_userdata($post->post_author);

              $forum_answers_sql = "
                SELECT count({$wpdb->prefix}posts.ID)
                FROM {$wpdb->prefix}posts
                INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
                AND (( {$wpdb->prefix}postmeta.meta_key = 'forum_theme_id' AND {$wpdb->prefix}postmeta.meta_value = {$post->ID} )) 
                AND {$wpdb->prefix}posts.post_type = 'vf-forum-answer'
                AND ({$wpdb->prefix}posts.post_status = 'publish')
                ";

              $forum_answers_count = (int) $wpdb->get_var($forum_answers_sql);

              if ($forum_answers_count) {
                $answers_last_comment_sql = "
                SELECT {$wpdb->prefix}posts.ID
                FROM {$wpdb->prefix}posts
                INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
                AND (( {$wpdb->prefix}postmeta.meta_key = 'forum_theme_id' AND {$wpdb->prefix}postmeta.meta_value = {$post->ID} )) 
                AND {$wpdb->prefix}posts.post_type = 'vf-forum-answer'
                AND ({$wpdb->prefix}posts.post_status = 'publish')
                ORDER BY {$wpdb->prefix}posts.post_date DESC
                LIMIT 1
                ";

                $forum_answers_last_comment_id = (int) $wpdb->get_var($answers_last_comment_sql);
              }

              ?>

              <div class="forum__item">
                <div class="forum__body">

                  <div class="forum__user user">
                    <div class="user__photo">
                      <?php print get_avatar($forum_user_data->ID, 150, null, $forum_user_data->data->user_login); ?>
                    </div>
                    <div class="user__name"><?php print get_user_meta($forum_user_data->ID, 'first_name', true); ?></div>
                  </div>

                  <div class="forum__content">

                    <div class="forum__message">
                      <div class="forum__title"><?php the_title(); ?></div>
                      <div class="forum__text"><?php the_content(); ?></div>
                    </div>

                    <div class="forum__footer">
                      <div class="forum__signature--single">
                        <div class="forum__user--single"><?php print get_user_meta($forum_user_data->ID, 'first_name', true); ?></div>
                        <div class="forum__answers--single">
                          <?php
                          if ($forum_answers_count) {
                            print $forum_answers_count . " " . true_wordform( $forum_answers_count, 'ответ', 'ответа', 'ответов' );
                          } else {
                            print "Нет ответов";
                          }
                          ?>
                        </div>
                        <div class="forum__publish--single">Разместил(а): <?php print get_the_date( get_option( 'date_format' ), $post->ID ); ?></div>
                        <?php if ($forum_answers_count) : ?>
                        <div class="forum__last-comment--single">Последний комментарий: <?php print get_the_date( get_option( 'date_format' ), $forum_answers_last_comment_id ); ?></div>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            <?php endwhile; ?>
          </div>
        </div>

        <?php

        /**
         * Pagination
         */
        $cur_page = get_query_var('pager');
        $paged = 1;

        if ($cur_page) {
          $paged = (int) $cur_page;
        }

        $forum_answers = new WP_Query(array(
          'status'          => 'publish',
          'post_type'       => VF_ANSWER_POST_TYPE,
          'posts_per_page'  => VF_ANSWERS_PAGINATION,
          'paged'           => $paged,
          'meta_query'      => array(
            array(
              'key'   => 'forum_theme_id',
              'value' => $forum_post_id
            )
          )
        ));

        $total_pages = $forum_answers->max_num_pages;

        ?>

        <?php if ( $forum_answers->have_posts() ) : ?>
        <div class="answers">

          <div class="section__head">
            <div class="head__content--padding">
              <div class="head__additional">
                <h2 class="head__title">Ответы</h2>
                <?php if ( (int) $forum_answers->found_posts ) : ?>
                  <span><?php print $forum_answers->found_posts; ?></span>
                <?php endif; ?>
              </div>
            </div>
          </div>

          <div class="section__body">

            <div class="section__nav section__nav--top forum__nav">
              <?php if ($total_pages > 1): ?>
                <div class="pager__nav">
                  <ul class="pager__nav--list">

                    <?php if ($paged > 1): ?>
                      <li class="pager__nav--item pager__nav--prev">
                        <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . ($paged - 1) . '/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                            <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                          </svg>
                        </a>
                      </li>

                      <?php if (($paged - 1) !== 1): ?>
                        <li class="pager__nav--item pager__nav--first">
                          <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/1/'; ?>">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                              <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                              <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                            </svg>
                          </a>
                        </li>
                      <?php endif; ?>

                    <?php endif; ?>

                    <?php for ($i=1; $i<=$total_pages; $i++): ?>
                      <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                        <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . $i . '/'; ?>"><?php print $i; ?></a>
                      </li>
                    <?php endfor; ?>

                    <?php if ($paged < $total_pages): ?>

                      <?php if (($paged + 1) != $total_pages): ?>
                        <li class="pager__nav--item pager__nav--last">
                          <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . $total_pages . '/'; ?>">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                              <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                              <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                            </svg>
                          </a>
                        </li>
                      <?php endif; ?>

                      <li class="pager__nav--item pager__nav--next">
                        <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . ($paged + 1) . '/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                            <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                          </svg>
                        </a>
                      </li>
                    <?php endif; ?>

                  </ul>
                </div>
              <?php endif; ?>
              <div class="section__nav--help">
                <a class="btn btn--big" href="#">Ответить</a>
              </div>
            </div>

            <div class="forum__reply <?php print have_posts() ? 'forum__reply--hide' : ''; ?>">
              <?php print do_shortcode('[vf_forum_form]'); ?>
            </div>

            <div class="forum__list">

              <?php while ( $forum_answers->have_posts() ) : $forum_answers->the_post(); ?>

                <?php

                $forum_answer_user_data = get_userdata($post->post_author);

                ?>

                <div class="forum__item">
                  <div class="forum__body">

                    <div class="forum__user user">
                      <div class="user__photo">
                        <?php print get_avatar($forum_answer_user_data->ID, 150, null, $forum_answer_user_data->data->user_login); ?>
                      </div>
                      <div class="user__name"><?php print get_user_meta($forum_answer_user_data->ID, 'first_name', true); ?></div>
                      <div class="forum__answers--publish"><?php print get_the_date( get_option( 'date_format' ), $post->ID ); ?></div>
                    </div>

                    <div class="forum__content">

                      <div class="forum__message">
                        <div class="forum__text"><?php the_content(); ?></div>
                      </div>

                    </div>

                  </div>
                </div>

              <?php endwhile; ?>

              <?php wp_reset_postdata(); ?>
            </div>

            <?php if ($total_pages > 1): ?>
            <div class="forum__pager">
              <div class="pager__nav">
                <ul class="pager__nav--list">

                  <?php if ($paged > 1): ?>
                    <li class="pager__nav--item pager__nav--prev">
                      <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . ($paged - 1) . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                            <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                          </svg>
                      </a>
                    </li>

                    <?php if (($paged - 1) !== 1): ?>
                      <li class="pager__nav--item pager__nav--first">
                        <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/1/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                              <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                            <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                            </svg>
                        </a>
                      </li>
                    <?php endif; ?>

                  <?php endif; ?>

                  <?php for ($i=1; $i<=$total_pages; $i++): ?>
                    <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                      <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . $i . '/'; ?>"><?php print $i; ?></a>
                    </li>
                  <?php endfor; ?>

                  <?php if ($paged < $total_pages): ?>

                    <?php if (($paged + 1) != $total_pages): ?>
                      <li class="pager__nav--item pager__nav--last">
                        <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . $total_pages . '/'; ?>">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                              <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                            <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                            </svg>
                        </a>
                      </li>
                    <?php endif; ?>

                    <li class="pager__nav--item pager__nav--next">
                      <a class="pager__nav--link" href="<?php print get_permalink( $forum_post_id ) . 'pager/' . ($paged + 1) . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                            <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                          </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                </ul>
              </div>
            </div>
            <?php endif; ?>
          </div>

        </div>
        <?php else: ?>
          <div class="forum__form-body">

            <div class="section__head">
              <div class="head__content--padding">
                <div class="head__additional">
                  <h2 class="head__title">Ответить</h2>
                </div>
              </div>
            </div>

            <div class="section__body">
              <?php print do_shortcode('[vf_forum_form]'); ?>
            </div>

          </div>
        <?php endif; ?>

      </div>
    </section>

  <script>
    (function($) {
      var navHelp = $('.forum__nav .section__nav--help');
      var forumReply = $('.forum__reply');

      navHelp.find('a').on('click', function(e) {

        e.preventDefault();
        navHelp.fadeOut();
        forumReply.slideDown(function(){
          $(this).removeClass('forum__reply--hide');
        });

      });

      forumReply.find('.about__reviews--cancel').on('click', function(e) {

        e.preventDefault();
        navHelp.fadeIn();
        forumReply.slideUp(function(){
          $(this).addClass('forum__reply--hide');
        });

      })

    })(jQuery);
  </script>

<?php get_footer();
