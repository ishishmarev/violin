<?php
//wp_register_script( 'form-valid', VR_PLUGIN_URL . '/assets/js/valid/valid.js' );
//wp_enqueue_script( 'form-valid' );

$forum_user_data = get_userdata(get_current_user_id());
$user_name = get_user_meta($forum_user_data->ID, 'first_name', true);

?>

<?php if (is_user_logged_in()) : ?>
<form action="<?php print $_SERVER['REQUEST_URI']; ?>" method="post" class="forum__form">
  <?php if (is_archive()) : ?>
  <div class="row">
    <div class="col-md-12">
      <div class="form__field form__field--theme">
        <label class="form__field--label">
          <input type="text" class="form__field--input" name="theme" value="" required="">
          <span class="form__field--placeholder">Тема</span>
        </label>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="form__field form__field--editor <?php print (is_archive()) ? '' : 'form__field--single'; ?>">
    <?php wp_editor('', 'message', array(
      'media_buttons' => false,
      'tinymce' => true,
      'quicktags' => false,
    )); ?>
  </div>
  <div class="about__reviews--submit">
    <label>
      <input type="submit" name="submit" value="<?php print (is_archive()) ? 'Задать вопрос' : 'Ответить' ?>" class="btn btn--big">
    </label>
    <a class="about__reviews--cancel" href="#">
      <svg x="0px" y="0px" width="42.9px" height="42.9px">
        <circle class="st0" cx="21.5" cy="21.5" r="21.5" />
        <g>
          <line class="st1" x1="12.7" y1="13.1" x2="30.2" y2="30.6" />
          <line class="st1" x1="12.7" y1="30.6" x2="30.2" y2="13.1" />
        </g>
      </svg>
    </a>
  </div>
</form>
<?php else: ?>
  для отправки сообщений необходимо авторизоваться
<?php endif; ?>
