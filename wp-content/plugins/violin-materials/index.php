<?php
/*
Plugin Name: Violin Materials
Plugin URI:
Description:
Version: 0.1
Author: ishishmarev
Author URI:
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define( 'VM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'VM_PLUGIN_URL', plugins_url( 'violin-materials' ) );
define( 'VM_PAGE_URL', 'materials' );
define( 'VM_MATERIALS_POST_TYPE', 'vm-materials' );
define( 'VM_MATERIALS_TAXONOMY', 'vm-materials-category' );
define( 'VM_MATERIALS_PAGINATION', 16 );
define( 'VM_MATERIALS_AUTHOR_POSTS_PAGINATION', 10 );
define( 'VM_MATERIALS_MAIN_PAGE_PAGINATION', 3 );
define( 'VM_MATERIALS_MAIN_PAGE_AUTHOR_POSTS_PAGINATION', 6 );

include_once VM_PLUGIN_DIR . '/includes/post-type.php';
include_once VM_PLUGIN_DIR . '/includes/post-taxonomy.php';
include_once VM_PLUGIN_DIR . '/includes/functions.php';

// Hook activate plugin.
register_activation_hook( __FILE__, 'vm_plugin_activation' );

// Register content type for Violin Materials.
add_action( 'init', 'vm_plugin_init' );

// Add metaboxes to content type Violin Materials.
add_action( 'add_meta_boxes', 'vm_materials_type' );

// Save metaboxes content type Violin Materials.
add_action( 'save_post', 'vm_materials_type_save', 1, 2 );

// Templates for Violin Materials.
add_filter( 'template_include', 'vm_materials_templates' );

// Header menu set active links.
add_action( 'nav_menu_css_class', 'vm_materials_set_active_link', 10, 2 );

// Rewrite slug rules.
add_filter( 'generate_rewrite_rules', 'vm_materials_category_slug_rewrite' );

// Permalinks for Violin Materials.
add_filter( 'post_type_link', 'vm_materials_permalinks', 1, 2 );

// Posts per page.
add_action( 'pre_get_posts', 'vm_posts_per_page' );

// Ajax show more on materials main page.
add_action( 'wp_ajax_vm_materials_load_more', 'vm_materials_load_more' );
add_action( 'wp_ajax_nopriv_vm_materials_load_more', 'vm_materials_load_more' );

// Filter Materials by Categories in Admin.
add_action( 'restrict_manage_posts', 'vm_filter_materials_by_category' , 10, 2);
