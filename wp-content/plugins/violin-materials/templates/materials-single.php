<?php
/**
 * The template for page reviews
 */
$current_term = get_the_terms($post->ID, VM_MATERIALS_TAXONOMY)[0];
if ($current_term->slug !== 'author-posts') {
  $file_path = CFS()->get( 'upload', $post->ID );
  if ( isset ($file_path) ) {
    wp_redirect( $file_path, 301 );
  }
}

get_header();
$categories = vm_get_sorted_materials_categories();

?>

  <div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content">

          <ul class="page__menu--list">
            <?php foreach($categories as $key => $val): ?>
              <li <?php print ($val->term_id === $current_term->term_id) ? 'class="current_page_item"' : ''; ?>>
                <a href="<?php print get_term_link($val); ?>">
                  <?php print $val->name; ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>

        </div>
      </div>
    </section>

    <section class="section__body section__body--material">
      <div class="container">
        <?php if ( have_posts() ) : ?>
        <div class="material">
          <?php while ( have_posts() ) : the_post(); ?>
          <div class="material__col material__col--left">
            <div class="material__title"><?php the_title(); ?></div>
            <?php if( has_post_thumbnail() ): ?>
              <div class="material__image">
                <?php the_post_thumbnail(); ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="material__col material__col--right">
            <div class="material__body">
              <?php the_content(); ?>
            </div>
            <div class="material__footer">
              <div class="material__date">
                <?php print get_the_date( get_option( 'date_format' ), $post->ID ); ?>
              </div>
              <div class="material__back">
                <a href="<?php print get_term_link($current_term); ?>">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                  <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                </svg>
                  <span>Вернуться к статьям</span>
                </a>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
        </div>
        <?php else: ?>
        <h2>Нет контента</h2>
        <?php endif; ?>
      </div>
    </section>
  </div>

<?php get_footer();
