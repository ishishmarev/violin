<?php
/**
 * The template for page reviews
 */
get_header();

global $wpdb;

$categories = vm_get_sorted_materials_categories();
$current_term = get_term( $wp_query->queried_object_id, VM_MATERIALS_TAXONOMY );

/**
 * Pagination
 */
$cur_page = get_query_var('paged');
$paged = 1;

if ($cur_page && is_int($cur_page)) {
  $paged = $cur_page;
}

$count_results = (int) $wp_query->found_posts;
$posts_per_page = ($current_term->slug === 'author-posts') ? VM_MATERIALS_AUTHOR_POSTS_PAGINATION : VM_MATERIALS_PAGINATION;
$total_pages = $wp_query->max_num_pages;

?>
  <div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content">

          <ul class="page__menu--list">
            <?php foreach($categories as $key => $val): ?>
              <li <?php print ($val->term_id === $current_term->term_id) ? 'class="current_page_item"' : ''; ?>>
                <a href="<?php print get_term_link($val); ?>">
                  <?php print $val->name; ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>

        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">

        <?php if ( have_posts() ) : ?>
          <?php if ( $current_term->slug !== 'author-posts' ) : ?>
          <div class="materials__books">
            <div class="materials__books--row row">
              <?php
              $contentColumnCount = 0;

              /* Start the Loop */
              while ( have_posts() ) : the_post();
              ?>
              <div class="materials__books--col col-md-3">
                <?php if ( has_post_thumbnail() ) : ?>
                  <div class="materials__books--image">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail(); ?>
                    </a>
                  </div>
                <?php endif; ?>
                <div class="materials__books--title"><?php the_title(); ?></div>
              </div>

              <?php
              $contentColumnCount++;
              if ($contentColumnCount%4 === 0 && $contentColumnCount != count($posts)):
              ?>
              </div>
              <div class="materials__books--row row">
              <?php endif; ?>

              <?php endwhile; ?>
            </div>
          </div>
          <?php else: ?>
          <div class="materials__articles--list">
            <?php
            /* Start the Loop */
            while ( have_posts() ) : the_post();
            ?>

              <div class="<?php print (has_post_thumbnail()) ? 'materials__articles--item' : 'materials__articles--full'; ?>">
                <?php if ( has_post_thumbnail() ) : ?>
                <div class="materials__articles--side">
                  <div class="materials__articles--image">
                    <?php the_post_thumbnail(); ?>
                  </div>
                </div>
                <?php endif; ?>
                <div class="materials__articles--content">
                  <div class="materials__articles--description">
                    <div class="materials__articles--title"><?php the_title(); ?></div>
                    <div class="materials__articles--text"><?php print CFS()->get( 'intro_text', $post->ID ); ?></div>
                  </div>
                  <div class="materials__articles--more">
                    <a href="<?php the_permalink(); ?>">подробнее</a>
                  </div>
                </div>
              </div>

            <?php endwhile; ?>
          </div>
          <?php endif; ?>

          <?php if ($total_pages > 1): ?>
          <div class="materials__books--pager">
            <div class="pager__nav">
              <ul class="pager__nav--list">

                <?php if ($paged > 1): ?>
                  <li class="pager__nav--item pager__nav--prev">
                    <a class="pager__nav--link" href="<?php print get_term_link($current_term) . 'page/' . ($paged - 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                    </svg>
                    </a>
                  </li>

                  <?php if (($paged - 1) !== 1): ?>
                    <li class="pager__nav--item pager__nav--first">
                      <a class="pager__nav--link" href="<?php print get_term_link($current_term) . 'page/1/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                          <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                        </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                <?php endif; ?>

                <?php for ($i=1; $i<=$total_pages; $i++): ?>
                  <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                    <a class="pager__nav--link" href="<?php print get_term_link($current_term) . 'page/' . $i . '/'; ?>"><?php print $i; ?></a>
                  </li>
                <?php endfor; ?>

                <?php if ($paged < $total_pages): ?>

                  <?php if (($paged + 1) != $total_pages): ?>
                    <li class="pager__nav--item pager__nav--last">
                      <a class="pager__nav--link" href="<?php print get_term_link($current_term) . 'page/' . $total_pages . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                          <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                        </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                  <li class="pager__nav--item pager__nav--next">
                    <a class="pager__nav--link" href="<?php print get_term_link($current_term) . 'page/' . ($paged + 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                    </svg>
                    </a>
                  </li>
                <?php endif; ?>

              </ul>
            </div>
          </div>
          <?php endif; ?>

        <?php else: ?>
          <h2>Нет содержимого</h2>
        <?php endif; ?>
      </div>
    </section>
  </div>

<?php get_footer();
