<?php
/**
 * The template for page reviews
 */
get_header();

$categories = vm_get_sorted_materials_categories();

?>
  <div class="page__content">
    <section class="section__materials--menu section__page--menu">
      <div class="container">
        <div class="page__menu">
          <ul class="page__menu--list">
            <?php foreach($categories as $key => $val): ?>
            <li>
              <a href="<?php print get_term_link($val); ?>">
                <?php print $val->name; ?>
              </a>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </section>

    <section class="section__body section__body--material">
      <div class="container">
        <div class="material">
          <?php
            $categories = vm_get_sorted_materials_categories();

            $ajax_settings = array(
              'ajax_url' => site_url() . '/wp-admin/admin-ajax.php',
              'data'    => array()
            );

            foreach ($categories as $key => $category) :

              $materials_per_page = ($category->slug !== 'author-posts' ) ? VM_MATERIALS_MAIN_PAGE_PAGINATION : VM_MATERIALS_MAIN_PAGE_AUTHOR_POSTS_PAGINATION;

              $material_posts = new WP_Query(array(
                'post_type'       => VM_MATERIALS_POST_TYPE,
                'tax_query' => array(
                  array(
                    'taxonomy' => VM_MATERIALS_TAXONOMY,
                    'terms' => $category->term_id,
                  ),
                ),
                'posts_per_page'  => $materials_per_page,
              ));

              $ajax_settings['data'][$category->slug] = array(
                'action'        => 'vm_materials_load_more',
                'query'         => $material_posts->query_vars,
                'max_num_pages' => $material_posts->max_num_pages,
                'page'          => 1
              );
          ?>
            <?php if ($material_posts->have_posts()) : ?>
            <div class="material__divider" data-category="<?php print $category->slug; ?>">
              <div class="material__col material__col--left">
                <div class="material__title"><?php print $category->name; ?></div>
              </div>
              <div class="material__col material__col--right">
                <div class="materials__aggregator">
                  <?php if ( $category->slug !== 'author-posts' ): ?>
                    <div class="materials__aggregator--list">
                      <div class="row materials__aggregator--row">
                      <?php while( $material_posts->have_posts() ) : $material_posts->the_post(); ?>
                        <div class="materials__aggregator--col col-md-4">
                          <div class="materials__aggregator--image">
                            <a href="<?php the_permalink(); ?>">
                              <?php the_post_thumbnail(); ?>
                            </a>
                          </div>
                          <div class="materials__aggregator--title"><?php the_title(); ?></div>
                        </div>
                      <?php endwhile; ?>
                      </div>
                    </div>
                    <?php if($material_posts->max_num_pages > 1): ?>
                    <div class="section__border--more text-center">
                      <a href="#">Показать еще</a>
                    </div>
                    <?php endif; ?>
                  <?php else: ?>
                    <div class="section__faq--list">
                      <div class="materials__aggregator--list">
                      <?php while( $material_posts->have_posts() ) : $material_posts->the_post(); ?>

                        <div class="section__faq--item">
                          <a class="section__faq--question" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>

                      <?php endwhile; ?>
                      </div>
                    </div>
                    <?php if($material_posts->max_num_pages > 1): ?>
                      <div class="section__border--more text-center">
                        <a href="#">Показать еще</a>
                      </div>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
            <?php endif; ?>

            <?php wp_reset_postdata(); ?>

          <?php endforeach; ?>

        </div>
      </div>
    </section>
  </div>

  <script>
    (function($) {
      var ajax_settings = <?php print json_encode($ajax_settings); ?>;
      console.log('ajax_settings', ajax_settings);

      $('.material__divider').each(function(id, el) {
        var material_category = $(el).data('category');

        $(el).find('.section__border--more a').on('click', function(e) {
          e.preventDefault();

          var material_category_settings = ajax_settings['data'][material_category];
          material_category_settings.page++;
          material_category_settings['term_slug'] = material_category;

          $.ajax({
            url: ajax_settings['ajax_url'],
            data: material_category_settings,
            type:'POST', // тип запроса
            success:function(data){
              $(data).appendTo($(el).find('.materials__aggregator--list'));

              if (material_category_settings['max_num_pages'] == material_category_settings['page']) {
                $(el).find('.section__border--more').remove();
              }
            }
          });

        });
      });

    })(jQuery);
  </script>



<?php get_footer();
