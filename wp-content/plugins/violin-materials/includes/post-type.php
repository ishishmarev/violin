<?php

/**
 * Function for create `vm-materials` post type.
 */
function vm_register_materials_post_type() {

  $labels = array(
    'name' => 'Материалы',
    'singular_name' => 'Материал',
    'add_new' => 'Добавить новый материал',
    'add_new_item' => 'Добавить новый материал',
    'edit_item' => 'Редактировать материал',
    'new_item' => 'Новый материал',
    'view_item' => 'Посмотреть материал',
    'search_items' => 'Поиск материалов',
    'not_found' => 'Материалов не найдено',
    'not_found_in_trash' => 'Материалов в корзине нет',
    'parent_item_colon' => 'Предок: Материал',
    'menu_name' => 'Материалы',
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'description',
    'taxonomies' => array (),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => NULL,
    'menu_icon' => NULL,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => VM_PAGE_URL,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => array (
      'slug' => VM_PAGE_URL . '/%' . VM_MATERIALS_TAXONOMY . '%',
      'hierarchical' => true,
      'with_front' => false,
    ),
    'capability_type' => 'post',
    'supports' => array (
      'title',
      'editor',
      'author',
      'thumbnail',
      'revisions',
      'page-attributes',
    ),
  );

  register_post_type( VM_MATERIALS_POST_TYPE, $args );
}

/**
 * Adds a metabox to the right side of the screen under “Publish” the box
 */
function vm_materials_type() {
  add_meta_box(
    'vm_materials_type',
    'Категория материала',
    'vm_materials_type_output',
    VM_MATERIALS_POST_TYPE,
    'side'
  );
}

/**
 * Output the HTML for the metabox.
 */
function vm_materials_type_output() {
  global $post;

  wp_nonce_field( basename( __FILE__ ), 'verify_materials_category' );

  $terms = vm_get_sorted_materials_categories();

  $post_terms = get_the_terms( $post->ID, VM_MATERIALS_TAXONOMY );
  $selected_term = $post_terms ? $post_terms[0]->name : $terms[0]->name;
  ?>
  <?php foreach ($terms as $key => $value): ?>
    <p>
      <label>
        <input name="materials_category" id="materials_category" type="radio" value="<?php print $value->name; ?>" <?php print ($selected_term == $value->name) ? 'checked' : ''; ?>>
        <span><?php print $value->name; ?></span>
      </label>
    </p>
  <?php endforeach;
}

/**
 * Save the metabox data
 */
function vm_materials_type_save( $post_id, $post ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return $post_id;

  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return $post_id;
  }

  if ( ! isset( $_POST['materials_category'] ) || ! wp_verify_nonce( $_POST['verify_materials_category'], basename(__FILE__) ) ) {
    return $post_id;
  }

  $materials_category = esc_textarea( $_POST['materials_category'] );

  wp_set_post_terms( $post_id, $materials_category, VM_MATERIALS_TAXONOMY, false );

  return $post_id;
}
