<?php

/**
 * Function for create `vm-materials` taxonomy.
 */
function vm_register_materials_taxonomy() {

  $labels = array(
    'name'                          => 'Категории материала',
    'menu_name'                     => 'Категории материала',
    'singular_name'                 => 'Категория материала',
    'search_items'                  => 'Поиск категорий',
    'popular_items'                 => 'Популярные категории',
    'all_items'                     => 'Все категории',
    'view_item '                    => 'Посмотреть категорию',
    'edit_item'                     => 'Редактировать категорию',
    'update_item'                   => 'Обновить категорию',
    'add_new_item'                  => 'Добавить новую категорию',
    'new_item_name'                 => 'Название новой категории',
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => false,
    'show_in_nav_menus'   => false,
    'rewrite'             => array('slug' => VM_PAGE_URL)
  );

  register_taxonomy( VM_MATERIALS_TAXONOMY, array( VM_MATERIALS_POST_TYPE ), $args );
}

/**
 * Function for create `vm-materials` taxonomy terms.
 */
function vm_register_materials_taxonomy_terms() {
  $terms = array (
    array (
      'name'          => 'Пособия',
      'slug'          => 'tutorial',
      'description'   => '0',
    ),
    array (
      'name'          => 'Книги автора',
      'slug'          => 'author-books',
      'description'   => '1',
    ),
    array (
      'name'          => 'Ноты',
      'slug'          => 'notes',
      'description'   => '2',
    ),
    array (
      'name'          => 'Статьи автора',
      'slug'          => 'author-posts',
      'description'   => '3',
    ),
  );

  foreach ( $terms as $term_key => $term ) {
    wp_insert_term(
      $term['name'],
      VM_MATERIALS_TAXONOMY,
      array(
        'description'   => $term['description'],
        'slug'          => $term['slug'],
      )
    );
    unset( $term );
  }
}
