<?php

/**
 * Activation plugin hook.
 */
function vm_plugin_activation() {
  vm_register_materials_taxonomy();
  vm_register_materials_taxonomy_terms();
}

/**
 * Init plugin hook.
 */
function vm_plugin_init() {
  vm_register_materials_taxonomy();
  vm_register_materials_post_type();
  flush_rewrite_rules();
}

/**
 * Header menu set active links.
 */
function vm_materials_set_active_link($classes, $item) {
  global $post;

  if ( is_object( $post ) && $post->post_type == VM_MATERIALS_POST_TYPE && $item->object === VM_MATERIALS_POST_TYPE ){
    $classes[] = 'current-page-ancestor';
  }

  return $classes;

}

/**
 * Rewrite permalinks.
 */
function vm_materials_permalinks( $post_link, $post ){
  if ( is_object( $post ) && $post->post_type == VM_MATERIALS_POST_TYPE ){
    $terms = vm_get_sorted_materials_categories();
    $post_terms = get_the_terms( $post->ID, VM_MATERIALS_TAXONOMY );
    $selected_term = $post_terms ? $post_terms[0]->slug : $terms[0]->slug;

    if( $terms ){
      $link = str_replace( '%' . VM_MATERIALS_TAXONOMY . '%', $selected_term, $post_link );

      if ($selected_term !== 'author-posts') {
        $file_path = CFS()->get( 'upload', $post->ID );
        if ( isset ($file_path) ) {
          $link = $file_path;
        }
      }

      return $link;
    }
  }

  return $post_link;
}

/**
 * Hook generate_rewrite_rules, material categories pagination.
 */
function vm_materials_category_slug_rewrite($wp_rewrite) {
  $rules = array();

  $terms = vm_get_sorted_materials_categories();

  foreach ($terms as $term) {
    $rules[VM_MATERIALS_POST_TYPE . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
    $rules[VM_MATERIALS_POST_TYPE . '/' . $term->slug . '/page/([0-9]{1,})/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug . '&paged=' . $wp_rewrite->preg_index( 1 );
  }

  $wp_rewrite->rules = $rules + $wp_rewrite->rules;
  return $wp_rewrite->rules;
}

/**
 * Hook pre_get_posts, posts per page.
 */
function vm_posts_per_page( $query ) {
  if ( !is_admin() && $query->is_tax( VM_MATERIALS_TAXONOMY ) && $query->is_main_query() ) {
    $current_term = get_term( $query->queried_object_id, VM_MATERIALS_TAXONOMY );

    if ($current_term->slug === 'author-posts') {
      $query->set( 'posts_per_page', VM_MATERIALS_AUTHOR_POSTS_PAGINATION );
    } else {
      $query->set( 'posts_per_page', VM_MATERIALS_PAGINATION );
    }
  }
}

/**
 * Include templates.
 */
function vm_materials_templates( $template ) {
  if ( is_singular( VM_MATERIALS_POST_TYPE ) ) {

    if ( $theme_file = locate_template( array ( 'materials-single.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VM_PLUGIN_DIR . 'templates/materials-single.php';
    }

    return $template;
  }

  if ( is_tax(VM_MATERIALS_TAXONOMY) ) {

    if ( $theme_file = locate_template( array ( 'materials-taxonomy.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VM_PLUGIN_DIR . 'templates/materials-taxonomy.php';
    }

    return $template;

  }

  if ( is_post_type_archive(VM_MATERIALS_POST_TYPE) ) {

    if ( $theme_file = locate_template( array ( 'materials.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VM_PLUGIN_DIR . 'templates/materials.php';
    }

    return $template;
  }

  return $template;
}

/**
 * Filter Materials by Categories in Admin
 */
function vm_filter_materials_by_category( $post_type, $which ) {
  if ( VM_MATERIALS_POST_TYPE !== $post_type )
    return;

  $terms = get_terms( VM_MATERIALS_TAXONOMY );

  print "<select name='{" . VM_MATERIALS_TAXONOMY . "}' id='{" . VM_MATERIALS_TAXONOMY . "}' class='postform'>";
  print '<option value="">Показать все</option>';
  foreach ( $terms as $term ) {
    printf(
      '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
      $term->slug,
      ( ( isset( $_GET[VM_MATERIALS_TAXONOMY] ) && ( $_GET[VM_MATERIALS_TAXONOMY] == $term->slug ) ) ? ' selected="selected"' : '' ),
      $term->name,
      $term->count
    );
  }
  print '</select>';
}

/**
 * Get taxonomy terms sorted by description.
 */
function vm_get_sorted_materials_categories() {
  $terms = get_terms( VM_MATERIALS_TAXONOMY, array( 'hide_empty' => false ) );
  usort($terms, function($a, $b) {
    return $a->description - $b->description;
  });
  return $terms;
}

/**
 * Ajax show more on materials main page.
 */
function vm_materials_load_more(){

  $materials_per_page = ($_POST['term_slug'] !== 'author-posts' ) ? VM_MATERIALS_MAIN_PAGE_PAGINATION : VM_MATERIALS_MAIN_PAGE_AUTHOR_POSTS_PAGINATION;
  $materials_category_id = (int) $_POST['query']['tax_query'][0]['terms'];

  if (is_int($materials_category_id)) {

    $material_posts = new WP_Query(array(
      'post_type'       => VM_MATERIALS_POST_TYPE,
      'tax_query' => array(
        array(
          'taxonomy'  => VM_MATERIALS_TAXONOMY,
          'terms'     => $materials_category_id,
        ),
      ),
      'paged' => (int) $_POST['page'],
      'posts_per_page'  => $materials_per_page,
    ));

    if ($material_posts->have_posts()) {
    ?>

      <?php if ( $_POST['term_slug'] !== 'author-posts' ): ?>

        <div class="row materials__aggregator--row">
          <?php while( $material_posts->have_posts() ) : $material_posts->the_post(); ?>
            <div class="materials__aggregator--col col-md-4">
              <div class="materials__aggregator--image">
                <a href="<?php the_permalink(); ?>">
                  <?php the_post_thumbnail(); ?>
                </a>
              </div>
              <div class="materials__aggregator--title"><?php the_title(); ?></div>
            </div>
          <?php endwhile; ?>
        </div>

      <?php else: ?>

        <?php while( $material_posts->have_posts() ) : $material_posts->the_post(); ?>

          <div class="section__faq--item">
            <a class="section__faq--question" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </div>

        <?php endwhile; ?>

      <?php endif; ?>

    <?php

    } else {
      print '<span>error: no data</span>';
    }

    wp_reset_postdata();

  } else {
    print '<span>error: $materials_category_id is not integer</span>';
  }

  die();
}
