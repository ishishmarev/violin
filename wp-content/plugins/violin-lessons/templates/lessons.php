<?php
/**
 * The main template file
 */


$lesson_themes = vl_get_sorted_lesson_themes();
$lesson_tags = get_terms( VL_LESSONS_TAGS_TAXONOMY, array( 'hide_empty' => false ) );

$lessons_themes_and_tags = array_merge($lesson_themes, $lesson_tags);

usort($lessons_themes_and_tags, function($a, $b) {
  return $a->name > $b->name;
});

?>

<?php get_header(); ?>

<div class="page__content">

  <section class="section__body section__no-pad">
    <div class="container">

      <?php if ( have_posts() ) : ?>

        <div class="lessons">

          <div class="lessons__nav">
            <div class="row">
              <div class="lessons__levels">
                <select>
                  <option value="themes" selected>Темы</option>
                  <option value="lessons">Уроки</option>
                </select>
              </div>
              <div class="lessons__videos">
                <input type="text" placeholder="Поиск...">
              </div>
            </div>
          </div>
          <div class="lessons__content">
            <div class="lessons__levels">
              <ul class="lessons__levels--list">

                <?php

                $all_posts = array();

                foreach ($lesson_themes as $key => $theme) :

                  $lessons_posts = new WP_Query(array(
                    'post_type'       => VL_LESSONS_POST_TYPE,
                    'tax_query' => array(
                      array(
                        'taxonomy' => VL_LESSONS_THEMES_TAXONOMY,
                        'terms' => $theme->term_id,
                      ),
                    ),
                  ));

                ?>

                  <?php if ($lessons_posts->post_count): ?>

                  <li class="lessons__levels--item <?php print $theme->slug; ?>">
                    <div class="lessons__levels--name">
                      <a href="#"><?php print $theme->name; ?></a>
                    </div>
                    <ul class="lessons__levels--dropdown">

                    <?php while( $lessons_posts->have_posts() ) : $lessons_posts->the_post(); ?>
                      <?php $all_posts[] = array(
                        'ID' => $post->ID,
                        'post_date' => $post->post_date,
                        'post_title' => $post->post_title,
                        'post_content' => $post->post_content,
                      ); ?>

                      <li id="lesson-<?php print $post->ID; ?>" class="lesson__video">
                        <a href="<?php the_permalink(); ?>" class="play-video"><?php the_title(); ?></a>
                      </li>

                    <?php endwhile; ?>

                    </ul>
                  </li>

                  <?php endif; ?>

                <?php endforeach;?>

                <script>
                  window.all_lessons_data_for_search = <?php print json_encode($all_posts, JSON_UNESCAPED_UNICODE); ?>;
                </script>

              </ul>
            </div>
            <div class="lessons__videos">
              <ol class="lessons__videos--list">
              </ol>
            </div>
          </div>

          <?php



          ?>

          <?php if ( count( $lessons_themes_and_tags ) ) : ?>

          <div class="lessons__tags tags">
            <div class="tags__left">
              <div class="tags__title">
                Темы И Слова Тегов
              </div>
            </div>
            <div class="tags__right">

              <?php

              $floor_cols = floor( count( $lessons_themes_and_tags ) / 3);

              $letters = array();

              foreach ( $lessons_themes_and_tags as $arr_key => $term ) {
                $first_letter = mb_strtolower(mb_substr( $term->name, 0, 1 ));
                $letters[ strtolower($first_letter) ][] = $term;
              }

              $contentItemCount = 0;
              $contentAllItemsCount = 0;

              ?>


              <div class="tags__row row">

                <div class="tags__col col-md-4">

                  <?php foreach ( $letters as $arr_key => $letter ) : ?>

                    <div class="tags__group">
                      <div class="tags__letter"><?php print $arr_key; ?></div>
                      <div class="tags__items">
                        <?php foreach ( $letter as $term_key => $term ) : ?>

                          <div class="tags__item">
                            <?php
                              $terms_posts = get_posts(array(
                                'post_type' => VL_LESSONS_POST_TYPE,
                                'post_status' => 'publish',
                                'tax_query' => array(
                                  array(
                                    'taxonomy' => $term->taxonomy,
                                    'field' => 'term_id',
                                    'terms' => $term->term_id
                                  )
                                )
                              ));

                              if (!empty($terms_posts)):
                                $data_posts = array();
                                foreach ($terms_posts as $t) {
                                  $data_posts[] = array(
                                    'ID' => $t->ID,
                                    'post_title' => $t->post_title,
                                  );
                                }
                            ?>

                            <a href="#" class="tags__item load_video" data-posts='<?php print json_encode($data_posts, JSON_UNESCAPED_UNICODE); ?>'>
                              <?php print $term->name; ?>
                            </a>

                            <?php else: ?>
                              <?php print $term->name; ?>
                            <?php endif; ?>

                          </div>


                        <?php
                          $contentItemCount++;
                          $contentAllItemsCount++;
                          if ($contentItemCount == $floor_cols && ($contentAllItemsCount <= ($floor_cols * 2))) :
                            $contentItemCount = 0;
                        ?>

                      </div>
                    </div>
                </div>
                <div class="tags__col col-md-4">
                  <div class="tags__group" <?php if ((count($letter)-1) == $term_key) print 'style="display:none;"'; ?>>

                    <div class="tags__letter"><?php print $arr_key; ?></div>
                    <div class="tags__items">

                      <?php endif; ?>

                        <?php endforeach; ?>
                      </div>
                    </div>


                  <?php endforeach; ?>

                </div>

              </div>


            </div>
          </div>

          <?php endif; ?>


        </div>

      <?php else: ?>
        <h2>Нет контента</h2>
      <?php endif; ?>

    </div>
  </section>

</div>

<?php get_footer();
