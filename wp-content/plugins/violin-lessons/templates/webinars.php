<?php
/**
 * The main template file
 */

$webinars_years = get_webinars_post_date_years();
$current_year = get_query_var('year') ? get_query_var('year') : $webinars_years[0]['date_year'];

/**
 * Pagination
 */
$cur_page = get_query_var('paged');
$paged = 1;

if ($cur_page && is_int($cur_page)) {
  $paged = $cur_page;
}
$total_pages = $wp_query->max_num_pages;

?>

<?php get_header(); ?>
<div class="page__content">
  <section class="section__head">
    <div class="container">
      <div class="head__content">

        <ul class="page__menu--list">
          <?php foreach($webinars_years as $key => $val): ?>
            <li <?php print ($val['date_year'] == $current_year) ? 'class="current_page_item"' : ''; ?>>
              <a href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $val['date_year'] . '/'; ?>">
                <?php print $val['date_year']; ?>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>

      </div>
    </div>
  </section>

  <section class="section__body">
    <div class="container">

      <?php if ( have_posts() ) : ?>
        <div class="webinars">
          <div class="webinars__row row">

            <?php do_shortcode( '' ); ?>


            <?php
              $contentColumnCount = 0;

              /* Start the Loop */
              while ( have_posts() ) : the_post();

              $post_video_id = cfs()->get('lesson_video', $post->ID);

              $post_video = get_post($post_video_id);

              $current_post = $wp_query->current_post;

              $video_data = array();

              if ( !empty($posts[$current_post - 1]) ) {
                $video_data['prev'] = 'lesson-' . $posts[$current_post - 1]->ID;
              }
              if ( !empty($posts[$current_post + 1]) ) {
                $video_data['next'] = 'lesson-' . $posts[$current_post + 1]->ID;
              }

            ?>

              <div id="lesson-<?php print $post->ID; ?>" class="webinars__col lesson__video col-md-6" data-nav='<?php print json_encode($video_data); ?>'>

                <?php if ( ! empty($post_video) ) : ?>
                  <?php

                  $post_video_poster = get_post_meta($post_video->ID, '_kgflashmediaplayer-poster', true);

                  ?>

                  <?php //print KGVID_shortcode('', $video); ?>

                  <div class="webinars__image">
                    <a href="<?php the_permalink(); ?>" class="play-video">
                      <img src="<?php print $post_video_poster; ?>" alt="<?php print $post_video->post_title; ?>">
                      <span class="play-video__button">
                        <span class="play-video__text">Play Video</span>
                      </span>
                    </a>
                  </div>
                <?php endif; ?>

                <div class="webinars__body">
                  <div class="webinars__date video__title"><?php print get_the_date( get_option( 'date_format' ), $post->ID ); ?></div>
                  <div class="webinars__text video__message">
                    <?php the_content(); ?>
                  </div>
                </div>

              </div>

              <?php
              $contentColumnCount++;
              if ($contentColumnCount%2 === 0 && $contentColumnCount != count($posts)):
                ?>
                </div>
                <div class="webinars__row row">
              <?php endif; ?>

            <?php endwhile; ?>
          </div>
        </div>

        <?php if ($total_pages > 1): ?>
          <div class="webinars__pager">
            <div class="pager__nav">
              <ul class="pager__nav--list">

                <?php if ($paged > 1): ?>
                  <li class="pager__nav--item pager__nav--prev">
                    <a class="pager__nav--link" href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $current_year . '/page/' . ($paged - 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                    </svg>
                    </a>
                  </li>

                  <?php if (($paged - 1) !== 1): ?>
                    <li class="pager__nav--item pager__nav--first">
                      <a class="pager__nav--link" href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $current_year . '/page/1/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                          <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                        </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                <?php endif; ?>

                <?php for ($i=1; $i<=$total_pages; $i++): ?>
                  <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                    <a class="pager__nav--link" href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $current_year . '/page/' . $i . '/'; ?>"><?php print $i; ?></a>
                  </li>
                <?php endfor; ?>

                <?php if ($paged < $total_pages): ?>

                  <?php if (($paged + 1) != $total_pages): ?>
                    <li class="pager__nav--item pager__nav--last">
                      <a class="pager__nav--link" href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $current_year . '/page/' . $total_pages . '/'; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                          <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                        </svg>
                      </a>
                    </li>
                  <?php endif; ?>

                  <li class="pager__nav--item pager__nav--next">
                    <a class="pager__nav--link" href="<?php print get_term_link($wp_query->queried_object_id) . 'year/' . $current_year . '/page/' . ($paged + 1) . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                    </svg>
                    </a>
                  </li>
                <?php endif; ?>

              </ul>
            </div>
          </div>
        <?php endif; ?>

      <?php else: ?>
        <h2>Нет контента</h2>
      <?php endif; ?>
    </div>
  </section>

</div>

<?php get_footer();
