<?php
/*
Plugin Name: Violin Lessons
Plugin URI:
Description:
Version: 0.1
Author: ishishmarev
Author URI:
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define( 'VL_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'VL_PLUGIN_URL', plugins_url( 'violin-lessons' ) );
define( 'VL_PAGE_LESSONS_URL',  'lessons' );
define( 'VL_LESSONS_POST_TYPE', 'vl-lessons' );
define( 'VL_LESSONS_TAXONOMY',  'vl-lessons-type' );
define( 'VL_LESSONS_THEMES_TAXONOMY',  'vl-lessons-themes' );
define( 'VL_LESSONS_TAGS_TAXONOMY',  'vl-lessons-tags' );
define( 'VL_LESSONS_TAXONOMY_URL_REPLACE',  'vl-lesson-type-url-replace' );
define( 'VL_LESSONS_WEBINARS_PAGINATION',  4 );

define('FFMPEG_PATH', '/usr/local/Cellar/ffmpeg/4.0/bin');
define('FREE_VIDEOS_DIR_NAME', 'free_videos');
define('FREE_VIDEO_LENGTH', '120');

include_once VL_PLUGIN_DIR . '/includes/post-lessons-type.php';
include_once VL_PLUGIN_DIR . '/includes/post-lessons.php';
include_once VL_PLUGIN_DIR . '/includes/widget.php';
include_once VL_PLUGIN_DIR . '/includes/functions.php';

// Hook activate plugin.
register_activation_hook( __FILE__, 'vl_plugin_activation' );

// Register content types for Violin Lessons.
add_action( 'init', 'vl_plugin_init' );

// Add metaboxes to content type Violin Lessons.
add_action( 'add_meta_boxes', 'vl_lessons_type' );

// Save metaboxes content type Violin Lessons.
add_action( 'save_post', 'vl_lessons_type_save', 1, 2 );

// Plugin scripts and styles.
add_action( 'wp_enqueue_scripts', 'vl_plugin_theme' );

// Ajax show more on materials main page.
add_action( 'wp_ajax_load_video', 'vl_lessons_load_video' );
add_action( 'wp_ajax_nopriv_load_video', 'vl_lessons_load_video' );

// Ajax for widget.
add_action( 'wp_ajax_vl_lessons_select_post_type', 'vl_lessons_select_post_type_callback' );

// Register widget
add_action( 'widgets_init', 'vl_lessons_widget' );

// Change video url if video not free.
add_filter("kgvid_encodevideo_info", "vl_video_order", null, 3);

// Add option on media type video: "it's not free video?".
add_filter( 'attachment_fields_to_edit', 'vl_option_video_not_free', null, 2 );

// Save option value on media type video: "it's not free video?".
add_filter("attachment_fields_to_save", "vl_option_video_not_free_save", null, 2);

// Rewrite slug rules.
add_filter( 'generate_rewrite_rules', 'vl_lesson_types_slug_rewrite' );

// Permalinks for Violin Lessons.
add_filter( 'post_type_link', 'vl_lesson_types_permalinks', 1, 2 );

// Permalinks for Violin Lessons Type.
add_filter('term_link', 'vl_lesson_tax_types_permalinks', 10, 3);

// Templates for Violin Lessons.
add_filter( 'template_include', 'vl_lesson_types_template' );

// Posts per page.
add_action( 'pre_get_posts', 'vl_posts_per_page' );
