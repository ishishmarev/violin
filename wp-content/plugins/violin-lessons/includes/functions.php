<?php

/**
 * Activation plugin hook.
 */
function vl_plugin_activation() {
  vl_register_lesson_types_taxonomy();
  vl_register_lesson_types_taxonomy_terms();
}

/**
 * Init plugin hook.
 */
function vl_plugin_init() {
  // Register post types
  vl_register_lesson_types_taxonomy();
  vl_register_lesson_themes_taxonomy();
  vl_register_lesson_tags_taxonomy();
  vl_register_lessons_post_type();
  flush_rewrite_rules();
}

/**
 * Plugin scripts and styles.
 */
function vl_plugin_theme() {
  $options = kgvid_get_options();

  // Video.js styles
  if ( $options['embed_method'] == "Video.js" || $options['embed_method'] == "Strobe Media Playback" ) {
    wp_enqueue_style( 'video-js', plugins_url( 'video-embed-thumbnail-generator' ) . '/video-js/video-js.css', '', '5.20.2' );
    if ( $options['js_skin'] == 'kg-video-js-skin' ){ wp_enqueue_style( 'video-js-kg-skin', plugins_url( 'video-embed-thumbnail-generator' ) . '/video-js/kg-video-js-skin.css', '', $options['version'] ); }
  }

  // Video.js scripts
  wp_enqueue_script( 'video-quality-selector', plugins_url( 'video-embed-thumbnail-generator' ) . '/video-js/video-quality-selector.js', array('video-js'), $options['version'], true );
  wp_enqueue_script( 'video-js', plugins_url( 'video-embed-thumbnail-generator' ) . '/video-js/video.js', '', '5.20.2', true );
  wp_localize_script( 'video-quality-selector', 'kgvidL10n_frontend', array(
    'ajaxurl' => admin_url( 'admin-ajax.php', is_ssl() ? 'admin' : 'http' ),
    'ajax_nonce' => wp_create_nonce('kgvid_frontend_nonce'),
    'playstart' => _x("Play Start", 'noun for Google Analytics event', 'video-embed-thumbnail-generator'),
    'completeview' => _x("Complete View", 'noun for Google Analytics event', 'video-embed-thumbnail-generator'),
    'next' => _x("Next", 'button text to play next video', 'video-embed-thumbnail-generator'),
    'previous' => _x("Previous", 'button text to play previous video', 'video-embed-thumbnail-generator'),
    'quality' => _x("Quality", 'text above list of video resolutions', 'video-embed-thumbnail-generator')
  ) );

  wp_enqueue_script( 'violinModal', VL_PLUGIN_URL . '/assets/js/violinModal.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script( 'play-video', VL_PLUGIN_URL . '/assets/js/play-video.js', array( 'violinModal' ), '1.0', true );
  wp_enqueue_style( 'violinModal', VL_PLUGIN_URL . '/assets/css/violinModal.css' );

  // AJAX Auth.
  wp_localize_script( 'play-video', 'ajaxPlayVideo', array(
    'act' => 'load_video',
    'url' => admin_url( 'admin-ajax.php' ),
  ));
}

/**
 * Ajax load lesson video into modal.
 */
function vl_lessons_load_video(){
  global $kgvid_video_id;

  $kgvid_video_id = 99;

  $post = get_post($_POST['lesson_id']);
  $video_id = CFS()->get('lesson_video', $_POST['lesson_id']);
  $video_url = wp_get_attachment_url($video_id);
  print json_encode( array(
    'player' => KGVID_shortcode('', $video_url),
    'title' => get_the_date( get_option( 'date_format' ), $post->ID ),
    'body' => wpautop( $post->post_content, true ),
  ), JSON_UNESCAPED_UNICODE );

}

/**
 * Rewrite permalinks.
 */
function vl_lesson_types_permalinks( $post_link, $post ){
  if ( is_object( $post ) && $post->post_type == VL_LESSONS_POST_TYPE ){
    $terms = vl_get_sorted_lesson_types();
    $post_terms = get_the_terms( $post->ID, VL_LESSONS_TAXONOMY );
    $selected_term = $post_terms ? $post_terms[0] : $terms[0];

    if ( $terms ){

      if ( $selected_term->slug === 'webinars' ) {
        global $wpdb;

        $post_year_date = get_the_date( 'Y', $post->ID );

        $sql_get_current_position = "
        SELECT count(*) as num
        FROM {$wpdb->prefix}posts p
        LEFT JOIN {$wpdb->prefix}term_relationships tax ON p.ID = tax.object_id
        WHERE tax.term_taxonomy_id = {$selected_term->term_id}
        AND p.post_status = 'publish'
        AND p.post_type = '" . VL_LESSONS_POST_TYPE . "'
        AND year(p.post_date) = '{$post_year_date}'
        AND p.post_date >= '{$post->post_date}'
        ";

        $get_current_position = $wpdb->get_var($sql_get_current_position);
        $top_position = ceil($get_current_position / VL_LESSONS_WEBINARS_PAGINATION);

        $search_link = '(%' . VL_PAGE_LESSONS_URL . '%/([^/]+)/)';
        $result_link = $selected_term->slug;

        if ($post_year_date != current_time('Y')) {
          $result_link = $selected_term->slug . '/year/' . $post_year_date;
        }

        if ( $top_position > 1 ) {
          $result_link .= '/page/' . $top_position;
        }

        $result_link .= '/#lesson-' . $post->ID;

        return preg_replace( $search_link, $result_link, $post_link );
      }

      if ( $selected_term->slug === 'lessons' ) {
        $search_link = '(%' . VL_PAGE_LESSONS_URL . '%/([^/]+)/)';
        $result_link = VL_PAGE_LESSONS_URL . '/#lesson' . '-' . $post->ID;
        $qwe = 0;

        return preg_replace( $search_link, $result_link, $post_link );
      }

    }
  }

  return $post_link;
}

/**
 * Rewrite taxonomy permalinks.
 */
function vl_lesson_tax_types_permalinks( $url, $term, $taxonomy ) {
  if ( $taxonomy === VL_LESSONS_TAXONOMY ) {
    return str_replace(  '%' . VL_LESSONS_TAXONOMY_URL_REPLACE . '%/', '', $url );
  }

  return $url;
}

/**
 * Hook generate_rewrite_rules, lesson types categories pagination.
 */
function vl_lesson_types_slug_rewrite($wp_rewrite) {
  $rules = array();

  $terms = vl_get_sorted_lesson_types();

  foreach ($terms as $term) {
    $rules[$term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
    $rules[$term->slug . '/page/?([0-9]{1,})?/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug . '&paged=' . $wp_rewrite->preg_index( 1 );
    $rules[$term->slug . '/year/?([0-9]{1,})?/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug . '&year=' . $wp_rewrite->preg_index( 1 );
    $rules[$term->slug . '/year/([0-9]{1,})/page/([0-9]{1,})?/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug . '&year=' . $wp_rewrite->preg_index( 1 ) . '&paged=' . $wp_rewrite->preg_index( 2 );
  }

  $wp_rewrite->rules = $rules + $wp_rewrite->rules;
  return $wp_rewrite->rules;
}

/**
 * Include templates.
 */
function vl_lesson_types_template( $template ) {
  if ( get_query_var('taxonomy') === VL_LESSONS_TAXONOMY ) {

    $current_term = get_query_var('term');

    if ( $theme_file = locate_template( array ( $current_term . '.php' ) ) ) {
      $template = $theme_file;
    } else {
      $template = VL_PLUGIN_DIR . 'templates/' . $current_term . '.php';
    }

    return $template;
  }

  return $template;
}

/**
 * Hook pre_get_posts, posts per page.
 */
function vl_posts_per_page( $query ) {
  if ( !is_admin() && $query->is_tax( VL_LESSONS_TAXONOMY ) && $query->is_main_query() ) {
    $current_term = get_term( $query->queried_object_id, VL_LESSONS_TAXONOMY );

    if ($current_term->slug === 'webinars') {

      $webinars_years = get_webinars_post_date_years();
      $current_year = get_query_var('year') ? get_query_var('year') : $webinars_years[0]['date_year'];

      $query->set( 'posts_per_page', VL_LESSONS_WEBINARS_PAGINATION );
      $query->set( 'year', $current_year );

    }
  }
}

/**
 * Hook kgvid_encodevideo_info, change video url if video not free.
 */
function vl_video_order($encodevideo_info, $movieurl, $postID){

  $is_not_free = (bool) get_post_meta( $postID, 'it_not_free', true );
  list($basedir, $baseurl) = array_values( array_intersect_key( wp_upload_dir(), array('basedir' => 1, 'baseurl' => 1) ) );

  if (
    $is_not_free
    && ! (current_user_can('editor') || current_user_can('administrator'))
  ) {

    $current_user_id = get_current_user_id();
    $current_user_expire_service = $current_user_id ? get_user_meta($current_user_id, 'register_service_expire', true) : false;
    $current_time = current_time( 'mysql' );

    if (  // Если аноним или просрочен
      !$current_user_id
      || strtotime($current_user_expire_service) < strtotime($current_time)
    ) {

      $new_file_dir = rtrim( $basedir,'/' ) . '/' . FREE_VIDEOS_DIR_NAME . '/';
      $new_file_url = rtrim( $baseurl,'/' ) . '/' . FREE_VIDEOS_DIR_NAME . '/';

      if( !is_dir( $new_file_dir ) ){
        mkdir( $new_file_dir );
      }

      $old_file_url = array_reverse(explode('/', $movieurl));
      $old_file_name = $old_file_url[0];
      $old_file_subdir = $old_file_url[2] . '/' . $old_file_url[1];



      $encodevideo_info['original'] = array (
        'filepath' => $basedir . '/' . $old_file_subdir . '/' . $old_file_name,
        'url' => $movieurl,
        'exists' => true,
      );

      foreach ($encodevideo_info as $key => $value) {

        if ( is_array($value) ) {

          if (!$value['exists']) {
            continue;
          }

          $old_file_name = end( explode('/', $value['url']) );
          $free_file_path = $new_file_dir . 'FREE__' . $old_file_name;
          $free_file_url = $new_file_url . 'FREE__' . $old_file_name;

          if ( !file_exists($free_file_path) ) {
            $command = escapeshellcmd(FFMPEG_PATH . '/' . 'ffmpeg' . ' -i "' . $value['filepath'] . '" -ss 00:00:00 -codec copy -t ' . FREE_VIDEO_LENGTH . ' "' . $free_file_path . '"');
            $command_result = shell_exec($command);
          }

          $encodevideo_info[$key]['url'] = $free_file_url;
          $encodevideo_info[$key]['filepath'] = $free_file_path;

        } else {

          if ( $key == 'moviefilebasename' ) {
            $encodevideo_info[$key] = 'FREE__' . $value;
          }
          if ( $key == 'encodepath' ) {
            $encodevideo_info[$key] = rtrim( $basedir,'/' ) . '/' . FREE_VIDEOS_DIR_NAME . '/';
          }

        }

      }

    }

  }

  return $encodevideo_info;
}

/**
 * Hook pon_attachment_fields_to_edit, add option on media type video: "it's not free video?".
 */
function vl_option_video_not_free( $form_fields, $post ){

  $is_video = vl_is_video($post);

  if ($is_video) {

    $value = get_post_meta( $post->ID, 'it_not_free', true );

    $_qwe = 0;

    $it_not_free_html = '<input type="checkbox" name="attachments['.$post->ID.'][it_not_free]" id="it_not_free" '. checked( $value, 'on', false ) .'>
    <label for="it_not_free">Если стоит галочка, занчит платное</label>';

    $form_fields['it_not_free'] = array(
      'label' => 'Платное видео',
      'input' => 'html',
      'html' => $it_not_free_html,
    );

  }

  return $form_fields;
}

/**
 * Hook attachment_fields_to_save, Save option value on media type video: "it's not free video?".
 */
function vl_option_video_not_free_save($post, $attachment) {
  $is_video = vl_is_video($post);

  if ($is_video) {
    if ( isset( $attachment['it_not_free'] ) ) {
      update_post_meta( $post['ID'], 'it_not_free', $attachment['it_not_free'] );
    } else {
      delete_post_meta( $post['ID'], 'it_not_free' );
    }
  }

  return $post;
}

/**
 * Check media on mime type 'video'.
 * @return true or false
 */
function vl_is_video($post) {

  if (isset($post->post_mime_type)) {
    $post_mime_type = $post->post_mime_type;
  } else if (isset($post['post_mime_type'])) {
    $post_mime_type = $post['post_mime_type'];
  }

	if ( $post_mime_type == 'image/gif' ) {
    $moviefile = get_attached_file($post->ID);
    $is_animated = vl_is_animated_gif($moviefile);
  }
  else { $is_animated = false; }

	if ( substr($post_mime_type, 0, 5) == 'video'	&&
       ( empty($post->post_parent)
         || (strpos(get_post_mime_type( $post->post_parent ), 'video') === false && get_post_meta($post->ID, '_kgflashmediaplayer-externalurl', true) == '' )
       )
       || $is_animated
  ) { //if the attachment is a video with no parent or if it has a parent the parent is not a video and the video doesn't have the externalurl post meta

    return true;

  }
  else {
    return false;
  }

}

/**
 * Check media image on animated gif.
 * @return true or false
 */
function vl_is_animated_gif($filename) {
    if(!($fh = @fopen($filename, 'rb')))
      return false;
    $count = 0;
    //an animated gif contains multiple "frames", with each frame having a
    //header made up of:
    // * a static 4-byte sequence (\x00\x21\xF9\x04)
    // * 4 variable bytes
    // * a static 2-byte sequence (\x00\x2C) (some variants may use \x00\x21 ?)

    // We read through the file til we reach the end of the file, or we've found
    // at least 2 frame headers
    while(!feof($fh) && $count < 2) {
      $chunk = fread($fh, 1024 * 100); //read 100kb at a time
      $count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
    }

    fclose($fh);
    return $count > 1;
}
