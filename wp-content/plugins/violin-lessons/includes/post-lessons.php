<?php

/**
 * Function for create `vl-lessons` post type.
 */
function vl_register_lessons_post_type() {

  $labels = array(
    'name' => 'Уроки',
    'singular_name' => 'Урок',
    'add_new' => 'Добавить новый урок',
    'add_new_item' => 'Добавить новый урок',
    'edit_item' => 'Редактировать урок',
    'new_item' => 'Новый урок',
    'view_item' => 'Посмотреть урок',
    'search_items' => 'Поиск уроков',
    'not_found' => 'Уроков не найдено',
    'not_found_in_trash' => 'Уроков в корзине нет',
    'parent_item_colon' => 'Предок: Урок',
    'menu_name' => 'Уроки',
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'description',
    'taxonomies' => array (),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => NULL,
    'menu_icon' => NULL,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => array(
      'slug' => '%' . VL_PAGE_LESSONS_URL . '%'
    ),
    'capability_type' => 'post',
    'supports' => array (
      'title',
      'editor',
      'author',
      'thumbnail',
      'revisions',
      'page-attributes',
    ),
  );

  register_post_type( VL_LESSONS_POST_TYPE, $args );
}

/**
 * Adds a metabox to the right side of the screen under “Publish” the box
 */
function vl_lessons_type() {
  add_meta_box(
    'vl_lessons_type',
    'Тип урока',
    'vl_lessons_type_output',
    VL_LESSONS_POST_TYPE,
    'side'
  );
}

/**
 * Output the HTML for the metabox.
 */
function vl_lessons_type_output() {
  global $post;

  wp_nonce_field( basename( __FILE__ ), 'verify_lessons_type' );

  $terms = vl_get_sorted_lesson_types();

  $post_terms = get_the_terms( $post->ID, VL_LESSONS_TAXONOMY );
  $selected_term = $post_terms ? $post_terms[0]->name : $terms[0]->name;
  ?>
  <?php foreach ($terms as $key => $value): ?>
    <p>
      <label>
        <input name="lessons_type" id="lessons_type" type="radio" value="<?php print $value->name; ?>" <?php print ($selected_term == $value->name) ? 'checked' : ''; ?>>
        <span><?php print $value->name; ?></span>
      </label>
    </p>
  <?php endforeach;
}

/**
 * Save the metabox data
 */
function vl_lessons_type_save( $post_id, $post ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return $post_id;

  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return $post_id;
  }

  if ( ! isset( $_POST['lessons_type'] ) || ! wp_verify_nonce( $_POST['verify_lessons_type'], basename(__FILE__) ) ) {
    return $post_id;
  }

  $lessons_type = esc_textarea( $_POST['lessons_type'] );

  wp_set_post_terms( $post_id, $lessons_type, VL_LESSONS_TAXONOMY, false );

  return $post_id;
}


