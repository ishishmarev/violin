<?php

/**
 * Function for create `vl-lessons` taxonomy.
 */
function vl_register_lesson_types_taxonomy() {

  $labels = array(
    'name'                          => 'Тип урока',
    'menu_name'                     => 'Тип урока',
    'singular_name'                 => 'Тип урока',
    'search_items'                  => 'Поиск типов',
    'popular_items'                 => 'Популярные типы',
    'all_items'                     => 'Все типы',
    'view_item '                    => 'Посмотреть тип',
    'edit_item'                     => 'Редактировать тип',
    'update_item'                   => 'Обновить тип',
    'add_new_item'                  => 'Добавить новый тип',
    'new_item_name'                 => 'Название нового типа',
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => false,
    'show_in_nav_menus'   => false,
    'rewrite'             => array(
      'slug' => '%' . VL_LESSONS_TAXONOMY_URL_REPLACE . '%'
    )
  );

  register_taxonomy( VL_LESSONS_TAXONOMY, array( VL_LESSONS_POST_TYPE ), $args );
}

/**
 * Function for create `vl-lessons` taxonomy.
 */
function vl_register_lesson_themes_taxonomy() {

  $labels = array(
    'name'                          => 'Темы уроков',
    'menu_name'                     => 'Темы уроков',
    'singular_name'                 => 'Тема урока',
    'search_items'                  => 'Поиск тем',
    'popular_items'                 => 'Популярные темы',
    'parent_item'                   => 'Родительская тема',
    'all_items'                     => 'Все темы',
    'view_item '                    => 'Посмотреть тему',
    'edit_item'                     => 'Редактировать тему',
    'update_item'                   => 'Обновить тему',
    'add_new_item'                  => 'Добавить новую тему',
    'new_item_name'                 => 'Название новой темы',
  );

  $args = array(
    'labels'              => $labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'query_var'           => true,
  );

  register_taxonomy( VL_LESSONS_THEMES_TAXONOMY, array( VL_LESSONS_POST_TYPE ), $args );
}

/**
 * Function for create `vl-lessons` taxonomy.
 */
function vl_register_lesson_tags_taxonomy() {

  $labels = array(
    'name'                          => 'Теги уроков',
    'menu_name'                     => 'Теги уроков',
    'singular_name'                 => 'Тег урока',
    'search_items'                  => 'Поиск тегов',
    'popular_items'                 => 'Популярные теги',
    'all_items'                     => 'Все теги',
    'view_item '                    => 'Посмотреть тег',
    'edit_item'                     => 'Редактировать тег',
    'update_item'                   => 'Обновить тег',
    'add_new_item'                  => 'Добавить новый тег',
    'new_item_name'                 => 'Название нового тега',
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => true,
    'query_var'           => true,
  );

  register_taxonomy( VL_LESSONS_TAGS_TAXONOMY, array( VL_LESSONS_POST_TYPE ), $args );
}

/**
 * Function for create `vl-lessons` taxonomy terms.
 */
function vl_register_lesson_types_taxonomy_terms() {
  $terms = array (
    array (
      'name'          => 'Уроки',
      'slug'          => 'lessons',
      'description'   => '0',
    ),
    array (
      'name'          => 'Вебинары',
      'slug'          => 'webinars',
      'description'   => '1',
    )
  );

  foreach ( $terms as $term_key => $term ) {
    wp_insert_term(
      $term['name'],
      VL_LESSONS_TAXONOMY,
      array(
        'description'   => $term['description'],
        'slug'          => $term['slug'],
      )
    );
    unset( $term );
  }
}

/**
 * Get taxonomy terms sorted by description.
 */
function vl_get_sorted_lesson_types() {
  $terms = get_terms( VL_LESSONS_TAXONOMY, array( 'hide_empty' => false ) );
  usort($terms, function($a, $b) {
    return $a->description - $b->description;
  });
  return $terms;
}

/**
 * Get taxonomy terms sorted by description.
 */
function vl_get_sorted_lesson_themes() {
  $terms = get_terms( VL_LESSONS_THEMES_TAXONOMY, array( 'hide_empty' => false ) );
  usort($terms, function($a, $b) {
    return $a->description - $b->description;
  });
  return $terms;
}

/**
 * Get webinars post_date years.
 */
function get_webinars_post_date_years() {
  global $wpdb;

  $taxonomy_term_webinars = get_term_by( 'slug', 'webinars', VL_LESSONS_TAXONOMY );

  $sql_get_webinars_years = "
  SELECT year(p.post_date) as date_year
  FROM {$wpdb->prefix}posts p
  LEFT JOIN {$wpdb->prefix}term_relationships t ON p.ID = t.object_id 
  WHERE p.post_status = 'publish'
  AND p.post_type = '" . VL_LESSONS_POST_TYPE . "'
  AND t.term_taxonomy_id = {$taxonomy_term_webinars->term_id}
  GROUP BY year(p.post_date)
  ORDER BY year(p.post_date) DESC";

  return $wpdb->get_results( $sql_get_webinars_years, 'ARRAY_A' );
}
