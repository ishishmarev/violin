<?php

class Display_Selected_Posts_Widget extends WP_Widget {

  // Main constructor
  public function __construct() {
    parent::__construct(
      'display_selected_posts_widget',
      'Вывод определенных записей',
      array(
        'customize_selective_refresh' => true,
      )
    );
  }

  // The widget form (for the backend )
  public function form( $instance ) {

    // Set widget defaults
    $defaults = array(
      'post_type'       => 'vl-lessons',
      'filter_by'       => '',
      'selected_posts'  => '',
      'display_type'    => 'grid',
    );

    // Parse current settings with defaults
    extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

    <div class="widget-data" data-id="<?php print $this->number; ?>" data-id_base="<?php print $this->id_base; ?>">
      <p>
        <label for="<?php echo $this->get_field_id( 'post_type' ); ?>">Выбрать тип записи:</label>
        <select name="<?php echo $this->get_field_name( 'post_type' ); ?>" id="<?php echo $this->get_field_id( 'post_type' ); ?>" class="widefat vl-lessons-select-post-type">
          <?php
          // Your options array
          $post_types = get_post_types( array( '_builtin' => false, 'public' => true ),'objects' );

          // Loop through options and add each one to the select dropdown
          foreach ( $post_types as $key => $value ) {
            echo '<option value="' . esc_attr( $value->name ) . '" id="' . esc_attr( $value->name ) . '" '. selected( $post_type , $value->name, false ) . '>'. $value->label . '</option>';
          }

          ?>
        </select>
      </p>

      <?php if (false) : // !empty(get_object_taxonomies( $post_type, $output = 'object' )) ?>
      <div class="filter_posts">
        <p>Фильтры: <a href="#" onclick="event.preventDefault(); ($(this).text() == 'Показать') ? $(this).text('Скрыть') : $(this).text('Показать'); $('.filter_posts-data').toggle();" class="toggle_filter_posts-data" style="float: right;">Показать</a></p>
        <div class="filter_posts-data" style="display: none; ">
        <?php

          vl_draw_widget_post_filters($post_type);

        ?>
        </div>
      </div>
      <?php endif; ?>

      <div class="vl-lessons-widget-add-post">
        <p>Отобразить записи:</p>
        <div class="vl-lessons-widget-data">
          <?php
            vl_draw_widget_select_posts($post_type, $instance, $this->get_field_name( 'selected_posts' ));
          ?>
          <?php

          ?>
        </div>
      </div>

      <div class="vl-lessons-widget-display-type">
        <p>Формат вывода:</p>

        <p>
          <label>
            <input name="<?php print $this->get_field_name( 'display_type' ); ?>" type="radio" value="grid" <?php print checked($display_type, 'grid'); ?> />
            <span>Плитка</span>
          </label>
        </p>
        <p>
          <label>
            <input name="<?php print $this->get_field_name( 'display_type' ); ?>" type="radio" value="full" <?php print checked($display_type, 'full'); ?> />
            <span>Полная новость</span>
          </label>
        </p>
      </div>


      <script>

        (function($){
          $('.vl-lessons-select-post-type').on('change', function(e) {
            var selected = $(this).find(':selected').val();
            var container = $(this).parents('.widget');
            var parent = $(this).parents('.widget-data');

            $.ajax({
              url: '<?php print admin_url( 'admin-ajax.php' ); ?>',
              data: {
                'action': 'vl_lessons_select_post_type',
                'post_type': selected
              },
              type: 'POST',
              success:function(data){

                console.log(data);

                if (data) {

                  var field_name = 'widget-' + parent.data('id_base') + '[' + parent.data('id') + ']' + '[selected_posts][]';
                  container.find('.vl-lessons-widget-add-post:visible .vl-lessons-widget-data').html(data);
                  container.find('.vl-lessons-widget-add-post:visible .vl-lessons-widget-data input').each(function(){
                    $(this).attr('name', field_name);
                  });
                  //$('.filter_posts').show();
                  //$('.filter_posts-data').html(data);

                } else {
                  //$('.filter_posts').hide();
                  container.find('.vl-lessons-widget-add-post:visible .vl-lessons-widget-data').html('Нет контента');
                }

              },
              error: function(errorThrown){
                console.log(errorThrown);
              }
            });
          });
        })(jQuery);

      </script>
    </div>
    <?php
  }

  // Update widget settings
  public function update( $new_instance, $old_instance ) {

    $instance = $old_instance;
    $instance['post_type']      = isset( $new_instance['post_type'] ) ? wp_strip_all_tags( $new_instance['post_type'] ) : 'vl-lessons';
    $instance['selected_posts'] = isset( $new_instance['selected_posts'] ) ? $new_instance['selected_posts'] : array('selected_posts' => array());
    $instance['display_type'] = isset( $new_instance['display_type'] ) ? $new_instance['display_type'] : 'grid';

    return $instance;

  }

  // Display the widget
  public function widget( $args, $instance ) {
    global $posts, $post;

    extract( $args );

    $post_type        = isset( $instance['post_type'] ) ? $instance['post_type'] : 'vl-lessons';
    $selected_posts   = isset( $instance['selected_posts'] ) ? $instance['selected_posts'] : array('selected_posts' => array());
    $display_type     = isset( $instance['display_type'] ) ? $instance['display_type'] : 'grid';


    $settings_posts = new WP_Query(array(
      'post_type'     => $post_type,
      'post_status'   => 'publish',
      'post__in'      => $selected_posts
    ));


    if (!empty($settings_posts)) {

      // WordPress core before_widget hook (always include )
      print $before_widget;

      if ($display_type == 'grid') {

        // Display the widget
        print '<div class="lessons__widget--list">';

        // Display widget title if defined

        while ( $settings_posts->have_posts() ) : $settings_posts->the_post(); ?>

          <?php

          $post_video_id = cfs()->get('lesson_video', $post->ID);

          $post_video = get_post($post_video_id);

          ?>

          <div id="lesson-<?php print $post->ID; ?>" class="lessons__widget--item lesson__video">

            <?php if ( ! empty($post_video) ) : ?>
              <?php

              $post_video_poster = get_post_meta($post_video->ID, '_kgflashmediaplayer-poster', true);

              ?>

              <?php //print KGVID_shortcode('', $video); ?>

              <div class="webinars__image webinars__image--widget">
                <a href="<?php the_permalink(); ?>" class="play-video">
                  <img src="<?php print $post_video_poster; ?>" alt="<?php print $post_video->post_title; ?>">
                  <span class="play-video__button">
                            <span class="play-video__text">Play Video</span>
                          </span>
                </a>
              </div>
            <?php endif; ?>

            <div class="webinars__body">
              <div class="video__message video__message--widget">
                <?php the_title(); ?>
              </div>
            </div>

          </div>

        <?php endwhile;

        print '</div>';

      } elseif ($display_type == 'full') {

        while ( $settings_posts->have_posts() ) : $settings_posts->the_post(); ?>

        <div class="lessons__widget--full">
          <div class="lessons__widget--full-title">
            <?php the_title(); ?>
          </div>
          <div class="lessons__widget--full-content">
            <?php print CFS()->get( 'full_text', $post->ID ); ?>
          </div>
        </div>

        <?php endwhile;
      }



      wp_reset_postdata();

      // WordPress core after_widget hook (always include )
      print $after_widget;

    }

  }

}

/**
 * Ajax callback for select post type.
 */
function vl_lessons_select_post_type_callback() {
  vl_draw_widget_select_posts($_POST['post_type']);
  die();
}

/**
 * Draw select posts.
 */
function vl_draw_widget_select_posts($post_type, $instance = array(), $field_name = '') {
  $settings_posts = get_posts(array(
    'post_type'     => $post_type,
    'post_status'   => 'publish',
  ));

  if (empty($instance)) $instance = array('selected_posts' => array());

  foreach ($settings_posts as $settings_post_key => $settings_post) :
    ?>

    <p>
      <input id="<?php print $settings_post->post_type . '-' . $settings_post->ID; ?>" name="<?php print $field_name; ?>[]" type="checkbox" value="<?php print $settings_post->ID; ?>" <?php print (in_array($settings_post->ID, $instance['selected_posts'])) ? 'checked' : ''; ?> />
      <label for="<?php print $settings_post->post_type . '-' . $settings_post->ID; ?>"><?php print $settings_post->post_title; ?></label>
    </p>

    <?php
  endforeach;
}

/**
 * Draw filters for easy select posts.
 */
function vl_draw_widget_post_filters($post_type) {
  $post_types_taxonomies = get_object_taxonomies( $post_type, $output = 'object' );
  foreach ( $post_types_taxonomies as $key => $taxonomy_object ) {
    print '<b>' . $taxonomy_object->label . '</b><br>';

    $taxonomy_object_terms = get_terms( $taxonomy_object->name, array( 'hide_empty' => false ) );

    foreach ($taxonomy_object_terms as $term_key => $term) :
      ?>

      <p>
        <input id="<?php print $term->taxonomy . '-' . $term->term_id; ?>" name="widget_post_filters[]" type="checkbox" value="<?php print $term->name; ?>" />
        <label for="<?php print $term->taxonomy . '-' . $term->term_id; ?>"><?php print $term->name; ?></label>
      </p>

      <?php
    endforeach;

    print '<hr>';
  }
}

/**
 * Register widget.
 */
function vl_lessons_widget() {
  register_widget( 'Display_Selected_Posts_Widget' );
}
