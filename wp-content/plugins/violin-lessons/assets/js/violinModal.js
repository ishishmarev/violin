(function ($, window) {
  /**
   * violinModal function
   */
  function violinModal(options) {
    this.options = $.extend({}, this.options);
    $.extend(this.options, options);
    this._init();
  }

  /**
   * violinModal options
   */
  violinModal.prototype.options = {
    // element to which the notification will be appended
    // defaults to the document.body
    wrapper: 'body',

    /**
     *  the message. see function _buildContent().
     *  head: {
     *    title: 'Header title!',
     *    message: 'Header message'
     *  },
     *  body: {
     *    title: 'Body title!',
     *    message: 'body message'
     *  },
     *  footer: {
     *    title: 'Footer title!',
     *    message: 'Footer message'
     *  }
     */
    message: {
      head: false,
      body: {
        title: false,
        message: 'body message'
      },
      footer: false
    },

    // layout type: main|game|other
    layout: 'main',

    // effects for the specified layout:
    // for growl layout: scale|slide|genie|jelly
    // for attached layout: flip|bouncyflip
    // for other layout: boxspinner|cornerexpand|loadingcircle|thumbslider
    // ...
    effect: 'slide',

    // user can not close popup without having performed the primary action
    modal: false,

    // if true, then used method remove. if false, then used method hide.
    removeOnClose: false,

    // if true then after init modal showing else modal not show
    autoShow: false,

    // if the user doesnВґt close the modal then we remove it after the following time
    autoHide: false,

    // following time for close the modal
    ttl: 3000, // @TODO.

    // callbacks
    onClose: function () {
      return false;
    },
    onOpen: function () {
      return false;
    }
  };

  /**
   * init function
   * initialize and cache some vars
   */
  violinModal.prototype._init = function () {

    // dismiss after [options.ttl]ms
    var self = this;

    var content = this._buildContent();
    var close = !(this.options.modal) ? '<a href="#" class="violinModal__close">\n  <svg width="28px" height="28px">\n    <g class="st0">\n      <path class="st1" d="M20.7,7.7c-0.2-0.2-0.6-0.2-0.8,0L7.7,19.8c-0.2,0.2-0.2,0.6,0,0.8c0.1,0.1,0.3,0.2,0.4,0.2c0.1,0,0.3-0.1,0.4-0.2L20.7,8.5C20.9,8.3,20.9,7.9,20.7,7.7L20.7,7.7z M20.7,7.7">\n      </path>\n      <path class="st1" d="M27.7,18.2C27.7,18.2,27.8,18.2,27.7,18.2c0.4-1.3,0.6-2.7,0.6-4.1c0-0.9-0.1-1.8-0.3-2.6c0,0,0-0.1,0-0.1c-0.1-0.3-0.2-0.7-0.2-1c0-0.1-0.1-0.2-0.1-0.4c-0.1-0.3-0.2-0.5-0.3-0.8c-0.1-0.2-0.1-0.4-0.2-0.6c-0.1-0.2-0.2-0.4-0.2-0.5c-0.1-0.3-0.2-0.5-0.4-0.8c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.3-0.4-0.6-0.6-0.9c0,0,0,0,0,0c-0.5-0.7-1.1-1.4-1.7-2.1c-0.6-0.6-1.3-1.2-2-1.7c0,0-0.1,0-0.1-0.1c-0.3-0.2-0.6-0.4-0.9-0.5c-0.1-0.1-0.2-0.1-0.3-0.2c-0.2-0.1-0.5-0.2-0.7-0.3c-0.2-0.1-0.4-0.2-0.6-0.3C19.4,1,19.2,1,19.1,0.9c-0.3-0.1-0.5-0.2-0.8-0.3c-0.1,0-0.2-0.1-0.3-0.1c-0.3-0.1-0.7-0.2-1.1-0.3c0,0,0,0,0,0C16,0.1,15.1,0,14.2,0c-0.9,0-1.8,0.1-2.7,0.3c0,0,0,0,0,0c-0.4,0.1-0.7,0.2-1,0.3c-0.1,0-0.2,0.1-0.3,0.1C9.8,0.7,9.6,0.8,9.3,0.9C9.1,0.9,8.9,1,8.8,1.1C8.6,1.2,8.4,1.2,8.2,1.3C8,1.4,7.7,1.6,7.5,1.7C7.4,1.7,7.3,1.8,7.2,1.9C6.9,2,6.6,2.2,6.3,2.4c0,0,0,0,0,0C5.5,2.9,4.8,3.5,4.2,4.2C3.5,4.8,2.9,5.5,2.4,6.2c0,0,0,0,0,0C2.2,6.6,2,6.9,1.9,7.2C1.8,7.3,1.7,7.4,1.7,7.5C1.6,7.7,1.4,8,1.3,8.2C1.2,8.4,1.2,8.6,1.1,8.7C1,8.9,0.9,9.1,0.9,9.3c-0.1,0.3-0.2,0.5-0.3,0.8c0,0.1-0.1,0.2-0.1,0.4c-0.1,0.3-0.2,0.7-0.2,1c0,0,0,0.1,0,0.1C0.1,12.4,0,13.3,0,14.2c0,0.9,0.1,1.8,0.3,2.7c0,0,0,0,0,0.1c0.1,0.3,0.2,0.7,0.3,1c0,0.1,0.1,0.2,0.1,0.3c0.1,0.3,0.2,0.5,0.3,0.8c0.1,0.2,0.1,0.4,0.2,0.5c0.1,0.2,0.2,0.4,0.3,0.6c0.1,0.2,0.2,0.5,0.4,0.7c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.3,0.3,0.6,0.5,0.9c0,0,0,0,0.1,0.1c0.5,0.7,1,1.4,1.7,2c1.6,1.6,3.6,2.8,5.8,3.5c0,0,0.1,0,0.1,0c0.4,0.1,0.8,0.2,1.2,0.3c0.2,0,0.3,0.1,0.5,0.1c0.3,0.1,0.6,0.1,0.9,0.1c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.7,0c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0c2,0,3.9-0.4,5.7-1.2c0,0,0,0,0.1,0c0.1-0.1,0.3-0.1,0.4-0.2c0.2-0.1,0.4-0.2,0.6-0.3c0.2-0.1,0.4-0.2,0.6-0.4c0.2-0.1,0.3-0.2,0.5-0.3c0.2-0.1,0.4-0.3,0.6-0.4c0.2-0.1,0.3-0.2,0.5-0.3c0.2-0.2,0.4-0.3,0.6-0.5c0.1-0.1,0.2-0.2,0.3-0.3c0,0,0,0,0,0c0.1-0.1,0.1-0.1,0.2-0.2c1.4-1.4,2.5-3.1,3.2-4.9c0,0,0,0,0-0.1C27.5,18.9,27.6,18.6,27.7,18.2z M23.4,23.4c-0.2,0.2-0.3,0.3-0.5,0.5c-0.2,0.2-0.3,0.3-0.5,0.5c-0.1,0.1-0.3,0.2-0.4,0.3c-0.2,0.1-0.4,0.3-0.6,0.4c-0.2,0.1-0.3,0.2-0.5,0.3c-0.2,0.1-0.4,0.2-0.5,0.3c-0.2,0.1-0.4,0.2-0.6,0.3c-1.7,0.8-3.6,1.3-5.6,1.3c-0.3,0-0.6,0-0.9,0c-0.2,0-0.4,0-0.5,0c-0.3,0-0.5-0.1-0.8-0.1c-0.1,0-0.3,0-0.4-0.1c-0.4-0.1-0.7-0.2-1.1-0.3c0,0-0.1,0-0.1,0c-2.8-0.9-5.2-2.7-6.9-5.1c0,0,0-0.1-0.1-0.1c-0.2-0.3-0.3-0.5-0.5-0.8c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.2-0.2-0.4-0.3-0.7c-0.1-0.2-0.2-0.3-0.2-0.5C2.1,19,2,18.8,2,18.6c-0.1-0.2-0.2-0.5-0.3-0.7c0-0.1-0.1-0.2-0.1-0.3c-0.1-0.3-0.2-0.6-0.2-0.9c0,0,0-0.1,0-0.1c-0.3-1.6-0.3-3.2,0-4.8c0,0,0-0.1,0-0.1c0.1-0.3,0.1-0.6,0.2-0.9c0-0.1,0.1-0.2,0.1-0.3C1.8,10.2,1.9,10,2,9.7C2,9.5,2.1,9.4,2.1,9.2C2.2,9,2.3,8.9,2.4,8.7C2.5,8.5,2.6,8.2,2.7,8c0-0.1,0.1-0.2,0.2-0.3C3,7.5,3.2,7.2,3.4,6.9c0,0,0,0,0,0c0.9-1.3,2.1-2.5,3.4-3.4c0,0,0,0,0.1,0C7.2,3.2,7.4,3,7.7,2.9C7.8,2.8,7.9,2.8,8,2.7c0.2-0.1,0.4-0.2,0.7-0.3C8.9,2.3,9,2.2,9.2,2.1C9.4,2.1,9.5,2,9.7,2c0.2-0.1,0.5-0.2,0.7-0.2c0.1,0,0.2-0.1,0.3-0.1c0.3-0.1,0.6-0.2,0.9-0.2c0,0,0,0,0.1,0c1.6-0.3,3.2-0.3,4.8,0c0,0,0,0,0.1,0c0.3,0.1,0.6,0.1,0.9,0.2c0.1,0,0.2,0.1,0.3,0.1c0.2,0.1,0.5,0.2,0.7,0.2C18.8,2,19,2.1,19.2,2.1c0.2,0.1,0.3,0.2,0.5,0.2c0.2,0.1,0.5,0.2,0.7,0.3c0.1,0.1,0.2,0.1,0.3,0.2c0.3,0.2,0.6,0.3,0.8,0.5c0,0,0,0,0.1,0c1.4,0.9,2.5,2.1,3.4,3.4c0,0,0,0,0,0c0.2,0.3,0.3,0.6,0.5,0.8c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.5,0.3,0.7C26,8.9,26.1,9,26.2,9.2c0.1,0.2,0.1,0.3,0.2,0.5c0.1,0.2,0.2,0.5,0.2,0.7c0,0.1,0.1,0.2,0.1,0.3c0.1,0.3,0.2,0.6,0.2,0.9c0,0,0,0,0,0.1c0.4,2,0.3,4.1-0.3,6.1c0,0,0,0,0,0.1C26,19.9,25,21.8,23.4,23.4z"></path><path class="st1" d="M20.7,20.7c0.2-0.2,0.2-0.6,0-0.8L8.5,7.7c-0.2-0.2-0.6-0.2-0.8,0C7.6,7.8,7.5,7.9,7.5,8.1c0,0.1,0.1,0.3,0.2,0.4l12.2,12.2C20.1,20.9,20.4,20.9,20.7,20.7L20.7,20.7z M20.7,20.7">\n    </path>\n    </g>\n  </svg>\n</a>' : '';

    // create HTML structure
    this.makeUp = $('<div class="violinModal violinModal__' + this.options.layout + '">\n  <div class="violinModal__dialog">\n    <div class="violinModal__align">\n      <div class="violinModal__align-row">\n        <div class="violinModal__align-cell">\n          <div class="violinModal__content">\n            ' + close + '\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>');

    $(content).appendTo(this.makeUp.find('.violinModal__content'));

    if (this.options.autoShow) {
      this.show();
    }
  };

  /**
   * Build content.
   */
  violinModal.prototype._buildContent = function () {
    var _message = this.options.message;
    var _tmp = '';

    if (_message !== null && typeof _message === 'object') {

      if (_message instanceof jQuery) {
        return _message;
      } else {

        for (var id in _message) {
          if (_message[id] !== null && typeof _message[id] === 'object') {
            _tmp += '<div class="violinModal__' + id + '">';
            for (var elem in _message[id]) {
              if (_message[id] instanceof jQuery) {
                return _message[id]
              } else {
                if (typeof _message[id][elem] === 'string' && elem === 'title' || elem === 'message') {
                  _tmp += '<div class="modal__' + id + '--' + elem + '">';
                  _tmp += _message[id][elem];
                  _tmp += '</div>';
                }
              }
            }
            _tmp += '</div>';
          }
        }

      }

    } else if (_message !== null && typeof _message === 'string') {
      _tmp += _message
    }

    return _tmp;
  };

  /**
   * init events
   */
  violinModal.prototype._initEvents = function () {
    if (!this.options.modal) {

      if (this.options.removeOnClose) {
        $(document).on('click', '.violinModal', this.remove.bind(this));
        $(document).on('click', '.violinModal__close', this.remove.bind(this));
      } else {
        $(document).on('click', '.violinModal', this.hide.bind(this));
        $(document).on('click', '.violinModal__close', this.hide.bind(this));
      }
      $(document).on('click', '.violinModal__content', this._stopBubble);

    }
  };

  /**
   * remove events
   */
  violinModal.prototype._removeEvents = function () {
    if (!this.options.modal) {

      $(document).off('click', '.violinModal');
      $(document).off('click', '.violinModal__close');
      $(document).off('click', '.violinModal__content');

    }
  };

  /**
   * show the modal
   */
  violinModal.prototype.show = function () {
    var self = this;

    if (!$('html').find(this.makeUp).length) {
      this.makeUp.appendTo(this.options.wrapper);

      // init events
      this._initEvents();
      this.options.onOpen();
    } else {
      this.makeUp.removeClass('hide');
    }

    $('body').addClass('violinModal__open');

    setTimeout(function () {
      self.makeUp.addClass('show');
    }, 100);

    this.active = true;
  };

  /**
   * hide the modal
   */
  violinModal.prototype.hide = function (event) {
    var self;
    if (event) {
      event.preventDefault();
      self = $(event.currentTarget).hasClass('violinModal') ? $(event.currentTarget) : $(event.currentTarget).parents('.violinModal');
    } else {
      self = this.makeUp;
    }

    this.active = false;


    self.removeClass('show');
    $('body').removeClass('violinModal__open');

    setTimeout(function () {
      self.addClass('hide');
    }, 200);

    this.options.onClose();
  };

  /**
   * remove the modal
   */
  violinModal.prototype.remove = function (event) {
    if (event) {
      event.preventDefault();
    }

    var self = this;

    this._removeEvents();

    this.active = false;

    this.makeUp.removeClass('show');
    $('body').removeClass('violinModal__open');

    setTimeout(function () {
      self.makeUp.remove();
    }, 200);

    self.options.onClose();
  };

  /**
   * Stop bubble if onclick content.
   */
  violinModal.prototype._stopBubble = function (event) {
    event.stopPropagation();
  };


  /**
   * open modal from links data.
   */
  $(document).on('click', '.violinModal__link', function (e) {

    e.preventDefault();
    var $link = $(e.currentTarget);
    var _target = $link.data('target');
    var $target = $(_target);

    if (!_target) {
      console.error('violinModal: ', 'В ссылке не указан атрибут `data-target`.');
      return false;
    }
    if (!$target.length) {
      console.error('violinModal: ', 'Указанный в атрибуте селектор не удалось найти.');
      return false;
    }

    new violinModal({
      message: $target.clone(),
      removeOnClose: true,
      autoShow: true
    });

  });

  /**
   * add to global namespace
   */
  window.violinModal = violinModal;
})(jQuery, window);
