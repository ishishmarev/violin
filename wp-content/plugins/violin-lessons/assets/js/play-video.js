(function ($) {

  var ajax_settings = ajaxPlayVideo;
  var validDate = /(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})(?=\W)|\b(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])?|(?:(?:16|[2468][048]|[3579][26])00)?)))(?=\W)|\b(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))(\4)?(?:(?:1[6-9]|[2-9]\d)?\d{2})?(?=\b)/gm;
  var prevVideo = null;
  window.modal = new violinModal({
    removeOnClose: true,
    layout: 'empty'
  });

  $('.play-video').on('click', function(e) {
    e.preventDefault();

    var lesson_parent = $(this).parents('.lesson__video');
    var lesson_data = lesson_parent.attr('id').split('-');

    var video_prev = '';
    var video_next = '';

    var nav = {};

    if ($('.lessons__levels').length) { // Если эта страница "библиотека уроков"

      if ($('.lessons__nav select :selected').val() == 'themes') {

        var prev = lesson_parent.prev();
        var next = lesson_parent.next();

        if (prev.length) {
          nav.prev = prev.attr('id');
        }
        if (next.length) {
          nav.next = next.attr('id');
        }

      } else {

        var all_lessons_videos = $('.lessons__content .lesson__video');
        var current_video_play = all_lessons_videos.index(lesson_parent);

        if ((current_video_play + 1) < all_lessons_videos.length) {
          nav.next = all_lessons_videos.eq(current_video_play + 1).attr('id');
        }
        if ((current_video_play - 1) >= 0) {
          nav.prev = all_lessons_videos.eq(current_video_play - 1).attr('id');
        }

        all_lessons_videos.eq(current_video_play + 1);
        all_lessons_videos.eq(current_video_play - 1);

      }

    } else {

      nav = lesson_parent.data('nav')

    }

    if ((typeof nav).toLowerCase() === 'object') {
      if ( nav.prev ) {
        video_prev = '<div class="video__prev">\n  <a href="#" data-move="#' + nav.prev + '">\n    <svg width="42px" height="78px">\n      <polygon class="st0" points="39.3,78.5 41.6,76.2 4.6,39.3 41.6,2.3 39.3,0 0,39.3 ">\n      </polygon>\n    </svg>\n  </a>\n</div>'
      }
      if ( nav.next ) {
        video_next = '<div class="video__next">\n  <a href="#" data-move="#' + nav.next + '">\n    <svg width="42px" height="78px">\n      <polygon class="st0" points="2.3,0 0,2.3 36.9,39.3 0,76.2 2.3,78.5 41.6,39.3 ">\n      </polygon>\n    </svg>\n  </a>\n</div>'
      }
    }

    $.ajax({
      url: ajax_settings['url'],
      data: {
        'action': ajax_settings['act'],
        'lesson_id': lesson_data[1]
      },
      type:'POST', // тип запроса


    success:function(data){
        data = data.slice(-1) === "0" ? data.substring(0, data.length - 1) : data; // Убирает 0, который передает плагин KGVID

        data = JSON.parse(data);

        var $video = $('<div class="lessons__modal"><div class="video"><div class="video__title">' + data.title + '</div><div class="video__content">' + video_prev + data.player + video_next + '</div><div class="video__message">' + data.body + '</div></div></div>');
        var videodiv = $video.find('.kgvid_videodiv');
        var video_vars = videodiv.data('kgvid_video_vars');
        video_vars.autoplay = "true";

        // append video start
        if (prevVideo) {
          prevVideo.dispose();
          //$('.kgvid_wrapper').remove();
          prevVideo = null;
        }

        window.modal.options.message = $video;
        window.modal._init();
        window.modal.options.onOpen = function () {
          prevVideo = kgvid_load_videojs(video_vars);
        };
        window.modal.options.onClose = function () {
          window.location.hash = '';
        };
        window.modal.show();

        if ($('.violinModal').length > 1) {
          $('.violinModal').eq(0).remove();
        }

      }
    });

  });

  $('.load_video').on('click', function(e) {
    e.preventDefault();

    var main_data = $(this).data('posts');

    var video_prev = '';
    var video_next = '';

    if (main_data[1]) {
      video_next = '<div class="video__next">\n  <a href="#" data-index="' + 1 + '" data-data=\'' + JSON.stringify(main_data) + '\' data-move="#lesson-' + main_data[1].ID + '">\n    <svg width="42px" height="78px">\n      <polygon class="st0" points="2.3,0 0,2.3 36.9,39.3 0,76.2 2.3,78.5 41.6,39.3 ">\n      </polygon>\n    </svg>\n  </a>\n</div>'
    }

    $.ajax({
      url: ajax_settings['url'],
      data: {
        'action': ajax_settings['act'],
        'lesson_id': main_data[0].ID
      },
      type:'POST', // тип запроса


      success:function(data){
        data = data.slice(-1) === "0" ? data.substring(0, data.length - 1) : data; // Убирает 0, который передает плагин KGVID

        data = JSON.parse(data);

        var $video = $('<div class="lessons__modal"><div class="video"><div class="video__title">' + data.title + '</div><div class="video__content">' + video_prev + data.player + video_next + '</div><div class="video__message">' + data.body + '</div></div></div>');
        var videodiv = $video.find('.kgvid_videodiv');
        var video_vars = videodiv.data('kgvid_video_vars');
        video_vars.autoplay = "true";

        // append video start
        if (prevVideo) {
          prevVideo.dispose();
          //$('.kgvid_wrapper').remove();
          prevVideo = null;
        }

        window.modal.options.message = $video;
        window.modal._init();
        window.modal.options.onOpen = function () {
          prevVideo = kgvid_load_videojs(video_vars);
        };
        window.modal.options.onClose = function () {
          window.location.hash = '';
        };
        window.modal.show();

        if ($('.violinModal').length > 1) {
          $('.violinModal').eq(0).remove();
        }

      }
    });

  });

  $(document).on('click', '.video__prev a, .video__next a', function(e) {
    e.preventDefault();
    var _this = e.currentTarget;
    var move = $(_this).data('move');
    var index = $(_this).data('index');
    var main_data = $(_this).data('data');

    if (main_data && main_data.length) {

      var video_prev = '';
      var video_next = '';

      if (main_data[index-1]) {
        video_prev = '<div class="video__prev">\n  <a href="#" data-index="' + (index - 1) + '" data-data=\'' + JSON.stringify(main_data) + '\' + data-move="#lesson-' + main_data[index-1].ID + '">\n    <svg width="42px" height="78px">\n      <polygon class="st0" points="39.3,78.5 41.6,76.2 4.6,39.3 41.6,2.3 39.3,0 0,39.3 ">\n      </polygon>\n    </svg>\n  </a>\n</div>'
      }
      if (main_data[index+1]) {
        video_next = '<div class="video__next">\n  <a href="#" data-index="' + (index + 1) + '" data-data=\'' + JSON.stringify(main_data) + '\' data-move="#lesson-' + main_data[index+1].ID + '">\n    <svg width="42px" height="78px">\n      <polygon class="st0" points="2.3,0 0,2.3 36.9,39.3 0,76.2 2.3,78.5 41.6,39.3 ">\n      </polygon>\n    </svg>\n  </a>\n</div>'
      }

      $.ajax({
        url: ajax_settings['url'],
        data: {
          'action': ajax_settings['act'],
          'lesson_id': main_data[index].ID
        },
        type:'POST', // тип запроса


        success:function(data){
          data = data.slice(-1) === "0" ? data.substring(0, data.length - 1) : data; // Убирает 0, который передает плагин KGVID

          data = JSON.parse(data);

          var $video = $('<div class="lessons__modal"><div class="video"><div class="video__title">' + data.title + '</div><div class="video__content">' + video_prev + data.player + video_next + '</div><div class="video__message">' + data.body + '</div></div></div>');
          var videodiv = $video.find('.kgvid_videodiv');
          var video_vars = videodiv.data('kgvid_video_vars');
          video_vars.autoplay = "true";

          // append video start
          if (prevVideo) {
            prevVideo.dispose();
            //$('.kgvid_wrapper').remove();
            prevVideo = null;
          }

          window.modal.options.message = $video;
          window.modal._init();
          window.modal.options.onOpen = function () {
            prevVideo = kgvid_load_videojs(video_vars);
          };
          window.modal.options.onClose = function () {
            window.location.hash = '';
          };
          window.modal.show();

          if ($('.violinModal').length > 1) {
            $('.violinModal').eq(0).remove();
          }

        }
      });
    } else if ($(move).length) {
      //window.modal.remove();
      window.location.hash = move;
      $(move).find('.play-video').click();
    }

  });

  $(document).on('click', '.lessons__content .lessons__videos a', function(e) {
    e.preventDefault();
    var _this = e.currentTarget;
    var move = $(_this).attr('href');
    if ($(move).length) {
      //window.modal.remove();
      window.location.hash = move;
      $(move).find('.play-video').click();
    }

  });

  $('.lessons__levels--name a').on('click', function(e) {
    e.preventDefault();
    $('.lessons__levels--item').removeClass('active');
    $(this).parents('.lessons__levels--item').addClass('active');
  });

  $('.lessons__nav .lessons__levels select').on('change', function(e) {

    if ($(this).find(':selected').val() == 'lessons') {
      $('.lessons__content .lessons__levels').addClass('all-lessons');
    } else {
      $('.lessons__content .lessons__levels').removeClass('all-lessons');
    }

  });

  $('.lessons__nav .lessons__videos input').on('keyup keydown', function(e){
    var search_input = $(this).val();
    var search_result_list = $('.lessons__videos--list');

    if (search_input.length > 2) {

      var search_results = $.grep(window.all_lessons_data_for_search, function( a ) {
        if (a.post_title.indexOf(search_input) !== -1 || a.post_content.indexOf(search_input) !== -1) {
          return true;
        }

        var dateMatches;
        while ((dateMatches = validDate.exec(search_input)) !== null) {

          // This is necessary to avoid infinite loops with zero-width matches
          if (dateMatches.index === validDate.lastIndex) {
            validDate.lastIndex++;
          }

          var postDate = new Date(a.post_date);

          var postDateOfMonth = '' + postDate.getDate();
          var postDateMonth = '' + (postDate.getMonth() + 1);
          var postDateYear = '' + (postDate.getFullYear());

          if (
            dateMatches[0].indexOf(postDateOfMonth) !== -1 ||
            dateMatches[0].indexOf(postDateMonth) !== -1 ||
            dateMatches[0].indexOf(postDateYear) !== -1
          ) {
            return true;
          }

        }

        return false;
      });

      var html = '';

      if (search_results.length) {

        search_results.forEach(function(el, id){
          html += '<li>';
          html += '<a href="#lesson-' + el.ID + '" class="lessons__videos--text">';
          html += el.post_title;
          html += '</a>';
          html += '</li>';
        });

      } else {
        html = 'По вашему запросу ничего не найдено...'
      }

      search_result_list.html(html);

    } else if ($(this).val().length == 0) {

      search_result_list.html('')

    } else {

      search_result_list.html('Введите более 2 символов');

    }
  });

  if ($(window.location.hash).length) {
    if ($('.lessons__levels').length) { // Если эта страница "библиотека уроков"

      //$('.play-video').index();$(window.location.hash).find('.play-video')
      $(window.location.hash).parents('.lessons__levels--item').addClass('active');

    }

    $(window.location.hash).find('.play-video').click();
  } else {
    if ($('.lessons__levels').length) { // Если эта страница "библиотека уроков"

      $('.lessons__levels--item').eq(0).addClass('active');

    }
  }


  function kgvid_load_videojs(video_vars) {

    var videojs_options = { "language": video_vars.locale };

    if ( videojs.browser.IS_IPHONE == true ) {
      videojs_options.html5 = { "nativeTextTracks" : true };
    }
    else { videojs_options.html5 = { "nativeTextTracks" : false }; }

    if ( video_vars.resize == "true" || video_vars.fullwidth == "true" ) {
      videojs_options.fluid = true;
    }
    else {
      videojs_options.fluid = false;
    }
    if ( video_vars.width != undefined && video_vars.width.indexOf('%') === -1 && video_vars.height != undefined ) {
      videojs_options.aspectRatio = video_vars.width + ':' + video_vars.height;
    }
    if ( video_vars.nativecontrolsfortouch == "true" ) {
      videojs_options.nativeControlsForTouch = true;
    }
    if ( video_vars.playback_rate == "true" ) {
      videojs_options.playbackRates = [0.5, 1, 1.25, 1.5, 2];
    }
    if ( video_vars.enable_resolutions_plugin == "true" ) {

      if ( videojs.VERSION.split('.')[0] >= 5 ) {

        videojs_options.plugins = { "resolutionSelector" : { "force_types" : ["video/mp4"] } };
        if ( video_vars.default_res ) {
          videojs_options.plugins.resolutionSelector.default_res = video_vars.default_res;
        }

      }

      else {
        console.warn('Video Embed & Thumbnail Generator: Video.js version '+videojs.VERSION+' is loaded by another application. Resolution selection is not compatible with this older version and has been disabled.');
      }
    }

    return videojs('video_' + video_vars.id, videojs_options ).ready(function(){ kgvid_setup_video(video_vars.id); });

  }

  function kgvid_setup_video(id) {

    var video_vars = jQuery('#video_'+id+'_div').data('kgvid_video_vars');

    if ( typeof (jQuery) == 'function' ) { jQuery.fn.fitVids=function(){}; }; //disable fitvids

    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false );
    if (iOS && video_vars.player_type == "StrobeMediaPlayback" ) { video_vars.player_type = "Video.js"; }

    jQuery('#video_'+id+'_div').prepend(jQuery('#video_'+id+'_watermark'));
    jQuery('#video_'+id+'_watermark').attr('style', ''); //shows the hidden watermark div
    jQuery('#video_'+id+'_div').prepend(jQuery('#video_'+id+'_meta'));
    jQuery('#video_'+id+'_embed, #click_trap_'+id).appendTo('#video_'+id+'_div');
    jQuery('#click_trap_'+id).on('click', function(){ kgvid_share_icon_click(id); });
    jQuery('#video_'+id+'_meta').attr('style', ''); //shows the hidden meta div
    if ( video_vars.autoplay == "true" ) {
      kgvid_video_counter(id, 'play');
      jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');
    }

    if ( video_vars.right_click != "on" ) {
      jQuery('#video_'+id+'_div').bind('contextmenu',function() { return false; });
    }

    if ( video_vars.player_type == "Video.js" ) {

      var player = eval('videojs.players.video_'+id);

      if ( jQuery('#video_'+id+'_flash_api').parent().is('.fluid-width-video-wrapper') ) { //disables fitVids.js
        jQuery('#video_'+id+'_flash_api').unwrap();
      }

      jQuery('#video_'+id).append(jQuery('#video_'+id+'_watermark'));

      if ( videojs.VERSION.split('.')[0] >= 5 && videojs.browser.TOUCH_ENABLED == true ) {

        if ( video_vars.nativecontrolsfortouch == "true" && videojs.browser.IS_ANDROID ) {
          jQuery('.vjs-big-play-button').hide();
        }

        if ( player.controls() == false && player.muted() == false ) { //mobile browsers allow autoplay only if the player is muted
          player.controls(true);
        }
      }

      if ( video_vars.autoplay == "true" && !player.paused() && player.hasClass('vjs-paused') ) {
        player.pause();
        player.play();
      }

      player.on('loadedmetadata', function(){

        if ( videojs.VERSION.split('.')[0] >= 5 ) {

          var text_tracks = player.textTracks();
          var track_elements = player.options_.tracks;
          var played = jQuery('#video_'+id+'_div').data("played") || "not played";

          if ( played == "not played" ) { //only turn on the default captions on first load

            if ( track_elements != null ) {
              jQuery(text_tracks).each(function(index, track) {
                if ( track_elements[index].default == true && track.mode != 'showing' ) { player.textTracks()[index].mode = 'showing'; }
              });
            }

            if ( video_vars.start != '' ) {
              player.currentTime(kgvid_convert_from_timecode(video_vars.start));
            }

          }

        }

        if ( video_vars.set_volume != "" ) { player.volume(video_vars.set_volume); }

        if ( video_vars.autoplay == "true" && player.paused() ) { player.play(); }

      });

      player.on('play', function kgvid_play_start(){
        player.off('timeupdate', kgvid_timeupdate_poster);
        if ( video_vars.meta ) {
          kgvid_add_hover(id);
          jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');
        }
        if ( video_vars.autoplay == "true" ) { jQuery('#video_'+id+' > .vjs-control-bar').removeClass('vjs-fade-in'); }
        if ( video_vars.endofvideooverlay != "" ) { jQuery('#video_'+id+' > .vjs-poster').hide(); }

        if ( video_vars.pauseothervideos == "true" && videojs.VERSION.split('.')[0] >= 5 ) {
          jQuery.each(videojs.getPlayers(), function(otherPlayerId, otherPlayer) {
            if ( player.id() != otherPlayerId && !otherPlayer.paused() && !otherPlayer.autoplay() ) {
              otherPlayer.pause();
            }
          });
        }

        kgvid_video_counter(id, 'play');

        player.on('timeupdate', function(){

          var percent_duration = Math.round(player.currentTime() / player.duration() * 100);

          if ( jQuery('#video_'+id+'_div').data("25") == undefined && percent_duration >= 25 && percent_duration < 50 ) {
            jQuery('#video_'+id+'_div').data("25", true);
            kgvid_video_counter(id, '25');
          }
          else if ( jQuery('#video_'+id+'_div').data("50") == undefined && percent_duration >= 50 && percent_duration < 75 ) {
            jQuery('#video_'+id+'_div').data("50", true);
            kgvid_video_counter(id, '50');
          }
          else if ( jQuery('#video_'+id+'_div').data("75") == undefined && percent_duration >= 75 && percent_duration < 100 ) {
            jQuery('#video_'+id+'_div').data("75", true);
            kgvid_video_counter(id, '75');

          }

        });

      });

      player.on('pause', function kgvid_play_pause(){
        jQuery('#video_'+id+'_meta').addClass('kgvid_video_meta_hover');
      });

      player.on('ended', function kgvid_play_end(){
        if ( jQuery('#video_'+id+'_div').data("end") == undefined ) {
          jQuery('#video_'+id+'_div').data("end", true);
          kgvid_video_counter(id, 'end');
        }
        setTimeout(function() { jQuery('#video_'+id+' > .vjs-loading-spinner').hide(); }, 250);
        if ( video_vars.endofvideooverlay != "" ) {
          jQuery('#video_'+id+' > .vjs-poster').css({
            'background-image':'url('+video_vars.endofvideooverlay+')'
          }).fadeIn();

          setTimeout(function() { player.on('timeupdate', kgvid_timeupdate_poster); }, 500);

        }
        if ( jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != "" && jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != null ) {
          kgvid_video_gallery_end_action(id, jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end'));
        }
      });

      player.on('fullscreenchange', function(){

        var
          fullScreenApi = {
            supportsFullScreen: false,
            isFullScreen: function() { return false; },
            requestFullScreen: function() {},
            cancelFullScreen: function() {},
            fullScreenEventName: '',
            prefix: ''
          },
          browserPrefixes = 'webkit moz o ms khtml'.split(' ');

        // check for native support
        if (typeof document.cancelFullScreen != 'undefined') {
          fullScreenApi.supportsFullScreen = true;
        } else {
          // check for fullscreen support by vendor prefix
          for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
            fullScreenApi.prefix = browserPrefixes[i];
            if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
              fullScreenApi.supportsFullScreen = true;
              break;
            }
          }
        }

        if ( player.availableRes != undefined ) {
          kgvid_resize_video(id);
        }

        if ( fullScreenApi.supportsFullScreen == false ) {
          if ( jQuery('#video_'+id).hasClass('vjs-fullscreen') ) {
            jQuery('#video_'+id+'_meta').hide();
            jQuery('#video_'+id+'_watermark img').css('position', 'fixed');
          }
          else {
            jQuery('#video_'+id+'_meta').show();
            jQuery('#video_'+id+'_watermark img').css('position', 'absolute');
          }
        }

      });

    } //end if Video.js

    if ( video_vars.player_type == "WordPressDefault" ) {

      var player = jQuery('#video_'+id+'_div video');
      var mejs_id = jQuery('#video_'+id+'_div .mejs-container').attr('id');

      player.on('loadedmetadata', function() {

        var mejs_player = eval('mejs.players.'+mejs_id);
        var resolutions = player.availableRes;
        var played = jQuery('#video_'+id+'_div').data("played") || "not played";

        if ( video_vars.set_volume != "" ) { player[0].volume = video_vars.set_volume; }
        if ( video_vars.mute == "true" ) { player[0].setMuted(true); }
        if ( video_vars.pauseothervideos == "false" ) { mejs_player.options.pauseOtherPlayers = false; }
        jQuery('#video_'+id+'_div .mejs-container').append(jQuery('#video_'+id+'_watermark'));

        if ( played == "not played" ) { //only turn on the default captions on first load

          jQuery.each(mejs_player.tracks, function(key, item) {
            if ( item.srclang == jQuery('#'+mejs_id+' track[default]').attr('srclang') ) {
              mejs_player.setTrack(item.srclang);
              jQuery('#'+mejs_id+' .mejs-captions-selector input[value="en"]').prop('checked',true);
            }
          });

          if ( video_vars.start != '' ) {
            player[0].setCurrentTime(kgvid_convert_from_timecode(video_vars.start));
          }

        }

      });

      player.on('play', function(){
        kgvid_add_hover(id);
        jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');

        kgvid_video_counter(id, 'play');

        player.on('timeupdate', function(){

          var percent_duration = Math.round(player[0].currentTime / player[0].duration * 100);

          if ( jQuery('#video_'+id+'_div').data("25") == undefined && percent_duration >= 25 && percent_duration < 50 ) {
            jQuery('#video_'+id+'_div').data("25", true);
            kgvid_video_counter(id, '25');
          }
          else if ( jQuery('#video_'+id+'_div').data("50") == undefined && percent_duration >= 50 && percent_duration < 75 ) {
            jQuery('#video_'+id+'_div').data("50", true);
            kgvid_video_counter(id, '50');
          }
          else if ( jQuery('#video_'+id+'_div').data("75") == undefined && percent_duration >= 75 && percent_duration < 100 ) {
            jQuery('#video_'+id+'_div').data("75", true);
            kgvid_video_counter(id, '75');
          }

        });

      });

      player.on('pause', function(){
        jQuery('#video_'+id+'_meta').addClass('kgvid_video_meta_hover');
      });

      jQuery(document).on('mozfullscreenchange webkitfullscreenchange fullscreenchange', function(){

        var mejs_player = eval('mejs.players.'+mejs_id);

        if ( mejs_player.isFullScreen ) {
          //mejs_player.enterFullScreen();
        }
      });

      player.on('ended', function(){
        if ( jQuery('#video_'+id+'_div').data("end") == undefined ) {
          jQuery('#video_'+id+'_div').data("end", true);
          kgvid_video_counter(id, 'end');
        }
        if ( video_vars.endofvideooverlay != "" ) {
          jQuery('#video_'+id+'_div .mejs-poster').css({
            'background-image':'url('+video_vars.endofvideooverlay+')'
          }).fadeIn();

          player.on('seeking.kgvid', function() {
            player = jQuery('#video_'+id+'_div video');
            if ( player[0].currentTime != 0) {
              jQuery('#video_'+id+'_div .mejs-poster').fadeOut();
              player.off('seeking.kgvid');
            }
          } );
        }
        if ( jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != "" && jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != null ) {
          kgvid_video_gallery_end_action(id, jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end'));
        }
      });


    } //end if WordPress Default

    if (  video_vars.player_type == "JWPlayer" ) {
      var player_id = jQuery('#video_'+id+'_div').children('div[id^="jwplayer"]').attr('id');
      player_id = player_id.replace('_wrapper', ''); //Flash JW Players have wrapper in the id
      var player = jwplayer(player_id);

      if ( video_vars.set_volume != "" ) { player.setVolume(Math.round(video_vars.set_volume*100)); }
      if ( video_vars.mute == "true" ) { player.setMute(true); }

      player.onPlay( function() {
        kgvid_video_counter(id, 'play');

        if ( video_vars.meta ) {
          kgvid_add_hover(id);
          jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');
        }

        if ( video_vars.pauseothervideos == "true" ) {
          var i = 0;
          while (true) {
            var testplayer = jwplayer(i);
            if (!testplayer.id) {
              break;
            }
            else if ( testplayer.id == player.id ) {
              i++;
            }
            else {
              testplayer.pause(true);
              i++;
            }
          }//end loop through jwplayers
        }

        player.onTime( function(e){

          if ( jQuery('#video_'+id+'_div').data("25") == undefined && Math.round(e.position / e.duration * 100) == 25 ) {
            jQuery('#video_'+id+'_div').data("25", true);
            kgvid_video_counter(id, '25');
          }
          else if ( jQuery('#video_'+id+'_div').data("50") == undefined && Math.round(e.position / e.duration * 100) == 50 ) {
            jQuery('#video_'+id+'_div').data("50", true);
            kgvid_video_counter(id, '50');
          }
          else if ( jQuery('#video_'+id+'_div').data("75") == undefined && Math.round(e.position / e.duration * 100) == 75 ) {
            jQuery('#video_'+id+'_div').data("75", true);
            kgvid_video_counter(id, '75');

          }

        });

      });

      player.onComplete( function() {

        if ( jQuery('#video_'+id+'_div').data("end") == undefined ) {
          jQuery('#video_'+id+'_div').data("end", true);
          kgvid_video_counter(id, 'end');
        }

        if ( jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != "" && jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end') != null ) {
          kgvid_video_gallery_end_action(id, jQuery('#kgvid_video_gallery_thumb_'+id).data('gallery_end'));
        }

      });

      if ( video_vars.right_click != "on" ) {
        player.onReady( function() {
          jQuery('#video_'+id+'_div .jwclick').remove();
        });
      }

    }

    if ( video_vars.resize == "true" || window.location.search.indexOf("kgvid_video_embed[enable]=true") !== -1 ) {
      kgvid_resize_video(id);
      jQuery(window).resize( function(){ kgvid_resize_video(id) } );
    }

    if ( typeof jQuery.modal !== "undefined" && jQuery('#kgvid-simplemodal-container').length > 0 ) { jQuery.modal.setPosition(); }

  }

  function kgvid_resize_video(id) {

    if ( typeof kgvid_resize_video.counter == 'undefined' ) {
      kgvid_resize_video.counter = 0;
    }

    var video_vars = jQuery('#video_'+id+'_div').data('kgvid_video_vars');

    if ( video_vars !== undefined ) {

      var set_width = video_vars.width;
      var set_height = video_vars.height;
      var aspect_ratio = Math.round(set_height/set_width*1000)/1000
      var reference_div = jQuery('#kgvid_'+id+'_wrapper').parent();
      var window_width = jQuery(window).width();
      var window_height = jQuery(window).height();

      if ( reference_div.is('body') ) { //if the video is embedded
        parent_width = window.innerWidth;
        set_width = window.innerWidth;
      }
      else if ( reference_div.attr('id') == 'kgvid_popup_video_holder_'+id ) { //if it's a pop-up video
        parent_width = window_width-40;
      }
      else {
        parent_width = reference_div.width();
        if ( video_vars['fullwidth'] == 'true' ) { set_width = parent_width; }
      }
      if ( parent_width < set_width ) { set_width = parent_width; }

      if ( set_width != 0 && set_width < 30000 ) {

        jQuery('#kgvid_'+id+'_wrapper').width(set_width);
        var set_height = Math.round(set_width * aspect_ratio);

        if ( reference_div.attr('id') == 'kgvid_popup_video_holder_'+id && set_height > window_height-60 ) { //if it's a popup video
          set_height = window_height-60;
          set_width = Math.round(set_height / aspect_ratio);
        }

        if ( reference_div.is('body') && set_height > window.innerHeight ) { //if it's a tall embedded video
          set_height = window.innerHeight;
          var change_aspect = true;

        } //if the video is embedded

        if ( ( video_vars.player_type == "Video.js" && eval('videojs.players.video_'+id) != null ) ) {

          video_vars.player_type == "Video.js"

          var player = eval('videojs.players.video_'+id);
          if ( change_aspect ) { player.aspectRatio(Math.floor(set_width)+':'+Math.floor(set_height)); }
          player.width(set_width).height(set_height);
          if ( set_width < 500 ) {
            var scale = Math.round(100*set_width/500)/100;
            jQuery('#kgvid_'+id+'_wrapper .vjs-big-play-button').css('-webkit-transform','scale('+scale+')').css('-o-transform','scale('+scale+')').css('-ms-transform','scale('+scale+')').css('transform','scale('+scale+')');
            if ( set_width < 261 ) {
              jQuery('#video_'+id+' > .vjs-control-bar > .vjs-mute-control').css('display', 'none');
              if ( set_width < 221 ) {
                jQuery('#video_'+id+' > .vjs-control-bar > .vjs-volume-control').css('display', 'none');
                if ( set_width < 171 ) {
                  jQuery('#video_'+id+' > .vjs-control-bar > .vjs-duration, #video_'+id+' > .vjs-control-bar > .vjs-time-divider').css('display', 'none');
                }
              }
            }
          }
          else { jQuery('#kgvid_'+id+'_wrapper .vjs-big-play-button').css('transform', ''); }
        }

        if ( video_vars.player_type == "StrobeMediaPlayback" ) {
          jQuery('#video_'+id+'_div').height(set_height);
          jQuery('#video_'+id).attr('width',set_width).attr('height',set_height);
          jQuery('#video_'+id+'_html5_api').attr('width',set_width).attr('height',set_height);
        }

        if ( video_vars.player_type == "WordPressDefault" && typeof mejs !== 'undefined' ) {

          player = eval('mejs.players.'+jQuery('#kgvid_'+id+'_wrapper div.wp-video-shortcode').attr('id'));

          if ( change_aspect ) {
            player.options.setDimensions = false;
            jQuery('#kgvid_'+id+'_wrapper div.wp-video-shortcode').css('height', set_height+'px');
          }

        }

        if ( ( video_vars.player_type == "Video.js" && eval('videojs.players.video_'+id) != null )
          ||  ( video_vars.player_type == "WordPressDefault" && typeof mejs !== 'undefined' )
        ) {
          if ( video_vars.auto_res == 'automatic' && player.availableRes !== undefined ) {

            var resolutions = player.availableRes;
            var resNumbers = new Array();

            jQuery.each(resolutions, function(key, value){
              if ( typeof key !== 'undefined' && !isNaN(parseInt(key)) ) {
                resNumbers.push(parseInt(key));
              }
            });
            var current_resolution = parseInt(player.getCurrentRes());
            if ( !isNaN(current_resolution) ) {
              if ( video_vars.pixel_ratio == "true" && window.devicePixelRatio != undefined ) {
                var pixel_ratio = window.devicePixelRatio;
              } //for retina displays
              else { pixel_ratio = 1; }

              if ( jQuery('#video_'+id).hasClass('vjs-fullscreen') || jQuery('#video_'+id+'_div .mejs-container').hasClass('mejs-container-fullscreen') ) {
                pixel_height = window_width * aspect_ratio * pixel_ratio;
              }
              else { pixel_height = set_width * aspect_ratio * pixel_ratio; }

              var res_options = jQuery.map(resNumbers, function(n) {
                if ( n >= pixel_height ) { return n; }
              });
              var set_res = Math.min.apply(Math,res_options);

              if ( set_res != current_resolution ) {

                if ( video_vars.player_type == "Video.js" ) {

                  if ( player.paused() ) {
                    player.one('play', function() {
                      player.changeRes(set_res+'p');
                      player.play();
                    });
                  }
                  else {
                    player.changeRes(set_res+'p');
                  }

                }

                if ( video_vars.player_type == "WordPressDefault" ) {

                  if ( player.media.paused ) {
                    jQuery(player.media).one('play', function() {
                      player.changeRes(set_res+'p');
                    });
                  }
                  else {
                    player.changeRes(set_res+'p');
                  }

                }

              }

              if (  video_vars.player_type == "Video.js" && jQuery('#video_'+id).hasClass('vjs-has-started') == false ) {
                if ( player.muted() == true ) {
                  player.muted(false);
                  player.muted(true);
                } // reset volume and mute otherwise player doesn't display properly
                if ( player.volume() != 1 ) {
                  var current_volume = player.volume();
                  player.volume(1);
                  player.volume(current_volume);
                }
              }

            } //automatic
          }
        }

        var meta = jQuery('#kgvid_video_gallery_thumb_'+id).data('meta');
        var extra_meta_height = Math.round(20*meta);
        jQuery('#kgvid-simplemodal-container').width(parseInt(set_width)+10);
        jQuery('#kgvid-simplemodal-container').height(parseInt(set_height)+10+extra_meta_height);
        jQuery('.simplemodal-wrap').css('overflow', 'hidden');

      }
      else if ( kgvid_resize_video.counter < 3 ) { setTimeout(function() { kgvid_resize_video(id); }, 250); } //if it's a wacky result, wait 1/4 second
    }

    ++kgvid_resize_video.counter;

  }

  function kgvid_timeupdate_poster() {
    jQuery('#'+this.id()+' > .vjs-poster').fadeOut();
  }

  function kgvid_add_hover(id) {

    jQuery('#video_'+id+'_div').hover(
      function(){
        jQuery('#video_'+id+'_meta').addClass('kgvid_video_meta_hover');
      },
      function(){
        jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');
      }
    );

    jQuery('#video_'+id+'_div').focus(
      function(){
        jQuery('#video_'+id+'_meta').addClass('kgvid_video_meta_hover');
      }
    )
      .focusout(
        function(){
          jQuery('#video_'+id+'_meta').removeClass('kgvid_video_meta_hover');
        }
      );

  }

  function kgvid_video_counter(id, event) {

    var video_vars = jQuery('#video_'+id+'_div').data('kgvid_video_vars');
    var changed = false;
    var title = jQuery('#kgvid_'+id+'_wrapper meta[itemprop=name]').attr('content');

    var played = jQuery('#video_'+id+'_div').data("played") || "not played";
    if ( played == "not played" ) {
      if (video_vars.countable) { //video is in the db
        changed = true;
        jQuery('#video_'+id+'_div').data("played", "played");
      }
      if (typeof ga != "undefined") { ga("send", "event", "Videos", kgvidL10n_frontend.playstart, title); }
      else if (typeof __gaTracker != "undefined") { __gaTracker("send", "event", "Videos", kgvidL10n_frontend.playstart, title); } // Yoast renamed ga function
      else if (typeof _gaq != "undefined") { _gaq.push(["_trackEvent", "Videos", kgvidL10n_frontend.playstart, title]); }

    }

    if ( !isNaN(event) ) {

      if (video_vars.countable) { //video is in the db
        changed = true;
      }

      if (typeof ga != "undefined") { ga("send", "event", "Videos", event+"%", title); }
      else if (typeof __gaTracker != "undefined") { __gaTracker("send", "event", "Videos", event+"%", title); } // Yoast renamed ga function
      else if (typeof _gaq != "undefined") { _gaq.push(["_trackEvent", "Videos", event+"%", title]); }

    }

    if ( event == "end" ) {

      if (video_vars.countable) { //video is in the db
        changed = true;
      }

      if (typeof ga != "undefined") { ga("send", "event", "Videos", kgvidL10n_frontend.completeview, title); }
      if (typeof __gaTracker != "undefined") { __gaTracker("send", "event", "Videos", kgvidL10n_frontend.completeview, title); } // Yoast renamed ga function
      else if (typeof _gaq != 'undefined') { _gaq.push(['_trackEvent', 'Videos', kgvidL10n_frontend.completeview, title]); }

    }

    if ( changed == true
      && video_vars.count_views != 'false'
      && (
        video_vars.count_views == 'quarters'
        || ( video_vars.count_views == 'start_complete' && ( event == 'play' || event == 'end' ) )
        || ( video_vars.count_views == 'start' && event == 'play' )
      )
    ) {
      jQuery.post(kgvidL10n_frontend.ajaxurl, {
        action: 'kgvid_count_play',
        security: kgvidL10n_frontend.ajax_nonce,
        post_id: video_vars.attachment_id,
        video_event: event
      }, function(data) {
        if ( event == "play" ) { jQuery('#video_'+id+'_viewcount').html(data); }
      });
    }
  }

})(jQuery);
