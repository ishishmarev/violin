<?php
/**
 * Template for displaying search forms in Violin lab
 *
 * @package WordPress
 * @subpackage violinlab
 * @since 1.0
 * @version 1.0
 */
?>
<form role="search" method="get" id="info__search" class="info__search" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
  <input type="text" id="info__search--input" class="info__search--input" placeholder="Что найти?" value="<?php echo get_search_query(); ?>" name="s" />
  <span class="info__search--button">
    <input type="submit" id="info__search--link" class="info__search--link" value="Поиск видео" />
  </span>
</form>
