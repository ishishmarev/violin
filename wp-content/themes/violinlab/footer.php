<?php
/**
 * The footer for our theme
 */
?>
    <footer class="footer">
      <div class="footer__content container">
        <div class="footer__main">
	        <?php if ( has_nav_menu( 'footer_menu' ) ) : ?>
          <div class="footer__nav">
	          <?php wp_nav_menu(array(
		          'theme_location'  => 'footer_menu',
		          'container'       => 'nav',
		          'container_class' => 'navigation',
		          'menu_class'      => 'navigation__list',
	          )); ?>
          </div>
	        <?php endif; ?>
          <div class="footer__social">
            <div class="footer__social--list">
              <div class="footer__social--item facebook"><a class="footer__social--facebook" href="#">facebook</a></div>
              <div class="footer__social--item google"><a class="footer__social--google" href="#">google</a></div>
              <div class="footer__social--item vk"><a class="footer__social--vk" href="#">vk</a></div>
              <div class="footer__social--item twitter"><a class="footer__social--twitter" href="#">twitter</a></div>
            </div>
          </div>
        </div>
        <div class="footer__copyright"><span>Copyright © 2017 - 2018 Все права защищены.</span></div>
      </div>
    </footer>
  </div>
  <?php wp_footer(); ?>
<?php get_template_part( 'template-parts/default/footer'); ?>
