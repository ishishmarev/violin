(function($) {
  $(function(){

    //console.log(1, $('.main__auth .auth__action--submit'));
    // Выполнение AJAX-запроса при нажатии на кнопку Входа
    $('.main__auth .auth__action--submit').on('click', function(e){
      //$('form#login p.status').show().text(ajax_login_object.loadingmessage);
      //console.log('Auth message: ', ajaxLogon.mes);
      $('.main__auth .form__message span').text(ajaxLogon.mes);

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajaxLogon.url,
        data: {
          'action': 'auth',
          'log': $('.main__auth input[name="log"]').val(),
          'pwd': $('.main__auth input[name="pwd"]').val(),
          'sec': ajaxLogon.sec
        },
        success: function(data){
          console.log('Auth message: ', data.mes);
          $('.main__auth .form__message span').text(data.mes);
          if (data.log == true){
            document.location.href = ajaxLogon.red;
          }
        }
      });
      e.preventDefault();
    });

    $('.main__auth .auth__visible').on('click', function(e) {
      e.preventDefault();
      $('.main__auth').toggleClass('active');
    });
    $('.main__auth').on('click', function(e) {
      e.stopPropagation();
    });
    $(document).on('click', function(e) {
      $('.main__auth').removeClass('active');
    });

    $('.form__field textarea:not(".wp-editor-area")').each(function(id, el) {
      $(el).resizable({
        handles: 'n, s',
        minHeight: $(el).height()
      });
    });
    // @todo forms.
    $('.form__field input, .form__field textarea:not(".wp-editor-area")')
    .each(function(){
      if($.trim($(this).val()).length) {
        $(this).closest('.form__field').addClass('not-empty');
      }
    }).on('focusin', function() {
      $(this).closest('.form__field').addClass('focus');
    }).on('focusout', function() {
      $(this).closest('.form__field').removeClass('focus');
    }).on('keyup', function() {
      if (!$.trim(this.value).length) {
        $(this).closest('.form__field').removeClass('not-empty');
      } else {
        $(this).closest('.form__field').addClass('not-empty');
      }
    });

  });
})(jQuery);
