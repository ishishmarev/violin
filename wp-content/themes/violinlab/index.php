<?php
/**
 * The main template file
 */
?>

<?php get_header(); ?>
	<div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <?php if ( is_home() && ! is_front_page() ) : ?>
            <h1 class="page-title"><?php single_post_title(); ?></h1>
          <?php else : ?>
            <h1 class="page-title"><?php print $wp_post_types[$post_type]->label; ?></h1>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">
        <?php if ( have_posts() ) : ?>
          <div class="post__list row">
          <?php
            $contentColumnCount = 0;

            /* Start the Loop */
            while ( have_posts() ) : the_post();

          ?>
              <div class="post__item col-md-6">
                <div class="post__body">
                  <?php if ( has_post_thumbnail() ) : ?>
                    <div class="post__image">
                      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail(); ?>
                      </a>
                    </div>
                  <?php endif; ?>
                  <div class="post__title"><?php the_title(); ?></div>
                  <div class="post__text"><?php the_content(); ?></div>
                  <div class="post__more">
                    <a href="<?php the_permalink(); ?>">
                      подробнее
                    </a>
                  </div>
                  <!-- <div class="post__date"><?php the_time('d.m.Y'); ?></div> -->
                </div>
              </div>

            <?php
              $contentColumnCount++;
              if ($contentColumnCount%2 === 0 && $contentColumnCount != count($posts)):
            ?>
              </div>
              <div class="post__list row">
            <?php endif; ?>

          <?php endwhile; ?>
          </div>

        <?php endif; ?>
      </div>
    </section>
  </div>
<?php get_footer(); ?>
