<?php
/**
 * The header for our theme
 */
get_template_part( 'template-parts/default/header');

if (is_page()) {
  $_post_ancestors = get_post_ancestors($_post);
  if (!empty($_post_ancestors)) {
    $_post = get_post( $_post_ancestors[0] );
    $breadcrumb_name = $_post->post_title;
  } else {
    $breadcrumb_name = $post->post_title;
  }
}

if ((is_archive() || is_single()) && !empty($post_type)) {
  $post_type_labels = get_post_type_labels(get_post_type_object($post_type));
  $breadcrumb_name = $post_type_labels->name;
}

if (is_tax()) {

  if ($taxonomy === VL_LESSONS_TAXONOMY) {

    if ( $term === "lessons" ) {
      $breadcrumb_name = 'Библиотека уроков';
    } else if ( $term === 'webinars' ) {
      $breadcrumb_name = 'ВЭБ семинары';
    } else {
      $breadcrumb_name = VL_LESSONS_TAXONOMY . ' error: not found lesson type';
    }

  } else {
    $_post_type_labels = get_post_type_labels(get_post_type_object(get_post_type()));
    $breadcrumb_name = $_post_type_labels->name;
  }

}

?>
<div class="page">
  <header class="header">
    <div class="header_container container">
      <div class="header__main">
        <div class="main__logo">
          <?php the_custom_logo(); ?>
        </div>
        <div class="main__nav">
          <?php
            // Header menu.
            if ( has_nav_menu( 'header_menu' ) ) {
              wp_nav_menu(array(
                'theme_location'  => 'header_menu',
                'container'       => 'nav',
                'container_class' => 'navigation',
                'menu_class'      => 'navigation__list',
              ));
            }
          ?>

          <?php
            // Header auth.
          get_template_part( 'template-parts/default/login' );
          ?>

        </div>
      </div>
      <div class="header__info">
        <div class="info__breadcrumbs">
          <div class="info__breadcrumbs--path"><?php print $breadcrumb_name; ?></div>
          <!--
          <div class="info__breadcrumbs--count">243 урока</div>
          -->
        </div>
        <?php get_search_form(); ?>
      </div>
    </div>
  </header>
