<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

  <div class="page__content">

    <?php if ( is_active_sidebar( 'sub_header_sidebar' ) ) : ?>
    <section class="section__head">
      <div class="container">
        <div class="head__content">
          <?php dynamic_sidebar( 'sub_header_sidebar' ); ?>
        </div>
      </div>
    </section>
    <?php endif; ?>

  <section class="section__body">
    <div class="container">
      <div class="main">
        <?php if ( is_active_sidebar( 'additional_sidebar' ) ) : ?>
        <div class="main__left">
        <?php endif; ?>

        <?php dynamic_sidebar( 'main_sidebar' ); ?>

        <?php if ( is_active_sidebar( 'additional_sidebar' ) ) : ?>
        </div>
        <div class="main__right">
          <?php dynamic_sidebar( 'additional_sidebar' ); ?>
        </div>
        <?php endif; ?>

      </div>

      <div class="faq">

        <div class="faq__head">
          <div class="faq__title">Часто задаваемые вопросы</div>
        </div>

        <div class="faq__content">
          <div class="faq__container">
            <?php
            $faq = get_posts(array(
              'post_type' => 'faq'
            ));
            ?>
            <div class="faq__list">
              <?php foreach($faq as $faq_key => $faq_item) : ?>
                <div class="faq__item">
                  <div class="faq__question"><a href="#"><?php print $faq_item->post_title; ?></a></div>
                  <div class="faq__answer"><?php print wpautop($faq_item->post_content); ?></div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>

      </div>

      <script>
        (function($){
          var faq = $('.faq');

          faq.find('.faq__question a').on('click', function(e){
            e.preventDefault();

            var faq_item = $(this).parents('.faq__item');
            var faq_answer = faq_item.find('.faq__answer');

            faq_answer.toggle();

          })
        })(jQuery);
      </script>

    </div>
  </section>


  </div>

<?php get_footer();
