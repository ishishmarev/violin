<?php
/**
 * Template Name: Violin Reviews Form
 * Description: A completely Violin Reviews form template.
 */

$fields = wpmtst_get_form_fields( WPMST()->atts( 'form_id' ) );

function violin_render_form_field( $field ) {
  $form_values = WPMST()->form->get_form_values();

  echo '<div class="form__field form__field--'.$field['name'].'">';

  if ( 'checkbox' != $field['input_type'] ) {
    if ( ! isset( $field['show_label'] ) || $field['show_label'] ) {
      $label = '<label class="form__field--label">';
      echo $label;
    }

    echo wpmtst_get_form_field_meta( $field, 'before' );
  }

  // Check for callback first.
  if ( isset( $field['action_input'] ) && $field['action_input'] ) {
    $value = ( isset( $form_values[ $field['name'] ] ) && $form_values[ $field['name'] ] ) ? $form_values[ $field['name'] ] : '';
    do_action( $field['action_input'], $field, $value );
  }
  // Check field type.
  else {
    switch ( $field['input_type'] ) {

      case 'category-selector' :
        $value = isset( $form_values[ $field['name'] ] ) ? (array) $form_values[ $field['name'] ] : array();

        printf( '<select id="wpmtst_%s" name="%s" class="%s" %s tabindex="0">',
          $field['name'],
          $field['name'],
          wpmtst_field_classes( $field['input_type'], $field['name'] ),
          wpmtst_field_required_tag( $field ) );

        echo '<option value="">&mdash;</option>';
        wpmtst_nested_cats( $value );
        echo '</select>';

        break;

      case 'category-checklist' :
        $value = isset( $form_values[ $field['name'] ] ) ? (array) $form_values[ $field['name'] ] : array();
        wpmtst_form_category_checklist_frontend( $value );

        break;

      case 'textarea' :
        $value = ( isset( $form_values[ $field['name'] ] ) && $form_values[ $field['name'] ] ) ? $form_values[ $field['name'] ] : '';

        // textarea tags must be on same line for placeholder to work
        printf( '<textarea id="wpmtst_%s" name="%s" class="form__field--textarea %s" %s %s tabindex="0" wrap="off">%s</textarea>',
          $field['name'],
          $field['name'],
          wpmtst_field_classes( $field['input_type'], $field['name'] ),
          wpmtst_field_required_tag( $field ),
          wpmtst_field_placeholder( $field ),
          esc_textarea( $value ) );

        break;

      case 'file' :
        echo '<input id="wpmtst_' . $field['name'] . '" type="file" name="' . $field['name'] . '"' . wpmtst_field_required_tag( $field ) . ' tabindex="0">';
        break;

      case 'shortcode' :
        if ( isset( $field['shortcode_on_form'] ) && $field['shortcode_on_form'] ) {
          echo do_shortcode( $field['shortcode_on_form'], true );
        }
        break;

      case 'rating' :
        wpmtst_star_rating_form( $field, $field['default_form_value'], 'in-form' );
        break;

      case 'checkbox' :
        if ( ! isset( $field['show_label'] ) || $field['show_label'] ) {
          $label = '<label for="wpmtst_' . $field['name'] . '">' . wpmtst_form_field_meta_l10n( $field['label'], $field, 'label' ) . '</label>';
          echo $label;
        }

        wpmtst_field_before( $field );

        echo '<div class="field-wrap">';

        printf( '<input id="wpmtst_%s" type="%s" class="%s" name="%s" %s %s tabindex="0">',
          $field['name'],
          $field['input_type'],
          wpmtst_field_classes( $field['input_type'], $field['name'] ),
          $field['name'],
          wpmtst_field_required_tag( $field ),
          checked( $field['default_form_value'], 1, false ) );

        if ( isset( $field['text'] ) ) {
          echo '<label for="wpmtst_' . $field['name'] . '" class="checkbox-label">';
          echo wpmtst_form_field_meta_l10n( $field['text'], $field, 'text' );
          echo '</label>';
        }

        if ( isset( $field['required'] ) && $field['required'] ) {
          wpmtst_field_required_symbol();
        }

        echo '</div><!-- .field-wrap -->';

        break;

      default: // text, email, url
        printf( '<input id="wpmtst_%s" type="%s" class="form__field--input %s" name="%s" %s %s %s tabindex="0">',
          $field['name'],
          $field['input_type'],
          wpmtst_field_classes( $field['input_type'], $field['name'] ),
          $field['name'],
          wpmtst_field_value( $field, $form_values ),
          wpmtst_field_placeholder( $field ),
          wpmtst_field_required_tag( $field ) );

    }
  }
  echo wpmtst_get_form_field_meta( $field, 'after' );

  if ( 'checkbox' != $field['input_type'] ) {
    if ( ! isset( $field['show_label'] ) || $field['show_label'] ) {
      echo '<span class="form__field--placeholder">';
      echo wpmtst_form_field_meta_l10n( $field['label'], $field, 'label' );
      echo '</span>';
      echo '</label>';
    }
  }

  wpmtst_field_error( $field );
  echo '</div>' . "\n";

}

?>
<form class="about__reviews--form <?php wpmtst_container_class(); ?>" <?php wpmtst_form_info(); ?>>
  <?php wpmtst_form_setup(); ?>
  <div class="row">
    <div class="col-md-6">
      <?php violin_render_form_field($fields[0]); ?>
    </div>
    <div class="col-md-6">
      <?php violin_render_form_field($fields[1]); ?>
    </div>
  </div>
  <div class="form__field reviews__form reviews__form--name">
    <?php violin_render_form_field($fields[2]); ?>
  </div>
  <div class="about__reviews--submit">
    <label>
      <input type="submit" id="wpmtst_submit_testimonial" name="wpmtst_submit_testimonial"
             value="<?php esc_attr_e( wpmtst_get_form_message( 'form-submit-button' ) ); ?>"
             class="btn btn--big" tabindex="0">
    </label>
    <a class="about__reviews--cancel" href="#">
      <svg x="0px" y="0px" width="42.9px" height="42.9px">
        <circle class="st0" cx="21.5" cy="21.5" r="21.5" />
        <g>
          <line class="st1" x1="12.7" y1="13.1" x2="30.2" y2="30.6" />
          <line class="st1" x1="12.7" y1="30.6" x2="30.2" y2="13.1" />
        </g>
      </svg>
    </a>
  </div>
</form>
