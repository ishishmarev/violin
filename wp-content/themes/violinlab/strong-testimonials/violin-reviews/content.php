<?php
/**
 * Template Name: Violin Reviews
 * Description: A violin Reviews template.
 */
?>
<?php do_action( 'wpmtst_before_view' ); ?>

<div class="about__reviews <?php wpmtst_container_class(); ?>"<?php wpmtst_container_data(); ?>>

	<div class="about__reviews--list <?php wpmtst_content_class(); ?>">
		<?php do_action( 'wpmtst_before_content' ); ?>

		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <?php
        $atts = WPMST()->atts();
        if ( isset( $atts['client_section'] ) ) {
          $fields = $atts['client_section'];
          $custom_fields = wpmtst_get_custom_fields();

          $field_user_name = $fields[0];
          $field_review_date = $fields[1];

          $field_name_user_name = get_post_meta( $query->post->ID, $field_user_name['field'], true );
          $field_name_review_date = $field_user_name['field'];

          $format = isset( $field['format'] ) && $field_review_date['format'] ? $field_review_date['format'] : get_option( 'date_format' );

          // Fall back to post_date if submit_date missing.
          $the_date = get_post_meta( $query->post->ID, $field_name_review_date, true );
          $the_date = $the_date ? $the_date : $query->post->ID;

          $field_name_review_date = mysql2date( $format, $the_date );
        }
      ?>
			<div class="about__reviews--item <?php wpmtst_post_class(); ?>">

        <div class="about__reviews--name"><?php print $field_name_user_name; ?></div>
        <div class="about__reviews--title"><?php wpmtst_the_title(); ?></div>
        <div class="about__reviews--message"><?php wpmtst_the_content(); ?></div>
        <div class="about__reviews--publish"><?php print $field_name_review_date; ?></div>

			</div>
		<?php endwhile; ?>

		<?php do_action( 'wpmtst_after_content' ); ?>
	</div>

</div>

<?php do_action( 'wpmtst_after_view' ); ?>
