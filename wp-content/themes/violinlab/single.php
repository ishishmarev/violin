<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

  <div class="page__content">
    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <h1 class="page-title"><?php the_title(); ?></h1>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">
        <div class="post__single">
          <?php

            /* Start the Loop */
            while ( have_posts() ) : the_post();

          ?>

            <?php if ( has_post_thumbnail() ) : ?>
              <div class="post__image">
                <?php the_post_thumbnail(); ?>
              </div>
            <?php endif; ?>

            <div class="post__title"><?php  ?></div>
            <div class="post__text--single"><?php the_content(); ?></div>
            <div class="post__footer">
              <div class="post__date--single"><?php the_time('d.m.Y'); ?></div>
              <div class="post__back">
                <a href="<?php print home_url( $wp_post_types[$post_type]->rewrite['slug'] ); ?>">
                  <svg width="10.5px" height="19.2px">
                    <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "></polygon>
                  </svg>
                  <span>Вернуться</span>
                </a>
              </div>
            </div>

          <?php
            endwhile; // End of the loop.
          ?>
        </div>
      </div>
    </section>
  </div>

<?php
get_footer();
