<?php
/**
 * The template for page about author
 */
get_header();

global $wpdb;

$children = get_children(array(
  'post_parent' => $post->ID,
  'post_type'   => $post->post_type,
  'order'       => 'ASC',
));

?>

  <section class="section__page--menu">
    <div class="container">
      <div class="page__menu">
        <?php print violinlab_list_child_pages(); ?>
      </div>
    </div>
  </section>

  <section class="content section section__head section__about">
    <div class="container">
      <div class="force__gutter">
        <?php print apply_filters('the_content', $post->post_content); ?>
       </div>
    </div>
  </section>

<?php get_footer();
