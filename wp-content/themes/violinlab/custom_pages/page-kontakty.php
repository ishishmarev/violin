<?php
/**
 * The template for page contacts
 */
get_header();

global $wpdb;

$children = get_children(array(
  'post_parent' => $post->ID,
  'post_type'   => $post->post_type,
  'order'       => 'ASC',
));

?>

  <section class="section__page--menu">
    <div class="container">
      <div class="page__menu">
        <?php print violinlab_list_child_pages(); ?>
      </div>
    </div>
  </section>

  <section class="section__contact">
    <div class="container">
      <div class="section__head section__contact--head">
        <div class="force__gutter">
          <?php print apply_filters('the_content', $post->post_content); ?>
        </div>
      </div>
      <div class="section__contact--body">
        <div class="section__border--system row">
          <div class="section__border--bar">
            <div class="section__border--title">
              <?php
                print do_shortcode(get_post_meta($post->ID, 'form_label', true));
              ?>
            </div>
          </div>
          <div class="section__border--content">
            <?php
              print do_shortcode(get_post_meta($post->ID, 'form', true));
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    (function($) {


      function make_tooltip(message, description) {
        var _mes = '' +
          '<div class="reg-field__popup">' +
          '<div class="form__popup-error form__popup-text" data-t="login-error" role="alert">' +
          '<div class="form__login-suggest">' +
          '<strong class="suggest__status-text error-message">' +
          message +
          '</strong>' +
          '';

        if(description) {
          _mes+=
            '<div>' +
            '<span class="suggest__description-text">' +
            description +
            '</span>' +
            '</div>';
        }

        _mes+= '</div></div></div>';

        return $(_mes);
      }

      function not_empty($form__field, $el, callback) {
        $('.wpcf7-form .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip
        if (!$.trim($el.val()).length) {
          $form__field.addClass('error');
          callback();
        } else {
          $form__field.removeClass('error');
        }
      }

      function validate_data($form__field, $el, _name) {
        switch (_name) {
          case 'email':
            $('.register__form--set .form__field').find('.reg-field__popup').remove(); // убрать наслаивание tooltip
            if (!$.trim($el.val()).length) {
              // если значение пустое
              $form__field.addClass('error');
              make_tooltip('Поле обязательно для заполнения').appendTo($form__field);
            } else if(!/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( $el.val() )) {
              make_tooltip('Такой E-mail не подойдет', 'Пожалуйста, проверьте правильность ввода').appendTo($form__field);
            } else {
              $form__field.removeClass('error');
            }
            break;
          default:
            not_empty($form__field, $el, function(){make_tooltip('Поле обязательно для заполнения').appendTo($form__field)});
            break;
        }
      }

      $('.wpcf7-form input, .wpcf7-form textarea').each(function(id, el){

        var $el = $(el);
        var _name = $el.attr('type');

        if (!!$el.attr('aria-required')) { // Обязательное ли поле
          $el.focusout(function(e){

            var $form__field = $el.parents('.form__field');
            validate_data($form__field, $el, _name);
          });
        }

      });

      $('.wpcf7-form').on('submit', function(e) {

        $('.wpcf7-form input, .wpcf7-form textarea').focusout();
        if( $('.wpcf7-form .error').length ) {
          return false;
        }
        return true;
      });


    })(jQuery);

  </script>

<?php get_footer();
