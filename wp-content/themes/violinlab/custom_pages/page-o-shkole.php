<?php
/**
 * The template for page about
 */

$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
);

$children = new WP_Query( $args );

wp_redirect(get_permalink($children->posts[0]->ID));
