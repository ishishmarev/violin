<?php
/**
 * The template for page reviews
 */
get_header();

global $wpdb;

$children = get_children(array(
  'post_parent' => $post->ID,
  'post_type'   => $post->post_type,
  'order'       => 'ASC',
));

$get_testimonials_view = wpmtst_get_view(1);
$get_testimonials_view_data = unserialize( $get_testimonials_view['value'] );

/**
 * Pagination
 */
$total_pages = 0;

if ($get_testimonials_view_data['pagination']) {
  $sql = "
  SELECT count(wp.ID) FROM {$wpdb->prefix}posts wp
  LEFT JOIN {$wpdb->prefix}term_relationships rel ON wp.ID = rel.object_id
  WHERE wp.post_type = 'wpm-testimonial'
  AND wp.post_status = 'publish'
  AND rel.term_taxonomy_id = 44";

  $sql_res = $wpdb->get_var($sql);
  $get_testimonials_data_count = (int) $sql_res;
  $maxNumItems = $get_testimonials_view_data['pagination_settings']['per_page'];
  $cur_page = get_query_var('paged');

  if ($get_testimonials_data_count > 0) {

    $paged = 1;
    $total_pages = ceil($get_testimonials_data_count / $maxNumItems);

    if ($cur_page && is_int($cur_page)) {
      $paged = $cur_page;
    }

  }


}

?>

  <section class="section__page--menu">
    <div class="container">
      <div class="page__menu">
        <?php print violinlab_list_child_pages(); ?>
      </div>
    </div>
  </section>

  <section class="section__about--reviews <?php print ($get_testimonials_data_count > 0) ? 'reviews__form--hide' : ''; ?>">
    <div class="container">

      <?php if ($total_pages > 0): ?>
        <div class="section__nav section__nav--top">
          <?php if ($get_testimonials_view_data['pagination']): ?>
          <div class="pager__nav">
            <ul class="pager__nav--list">

              <?php if ($paged > 1): ?>
                <li class="pager__nav--item pager__nav--prev">
                  <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . ($paged - 1) . '/'; ?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                    </svg>
                  </a>
                </li>

                <?php if (($paged - 1) !== 1): ?>
                  <li class="pager__nav--item pager__nav--first">
                    <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/1/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                        <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                        </svg>
                    </a>
                  </li>
                <?php endif; ?>

              <?php endif; ?>

              <?php for ($i=1; $i<=$total_pages; $i++): ?>
                <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                  <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . $i . '/'; ?>"><?php print $i; ?></a>
                </li>
              <?php endfor; ?>

              <?php if ($paged < $total_pages): ?>

                <?php if (($paged + 1) != $total_pages): ?>
                  <li class="pager__nav--item pager__nav--last">
                    <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . $total_pages . '/'; ?>">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                          <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                        <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                        </svg>
                    </a>
                  </li>
                <?php endif; ?>

                <li class="pager__nav--item pager__nav--next">
                  <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . ($paged + 1) . '/'; ?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                      <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                    </svg>
                  </a>
                </li>
              <?php endif; ?>

            </ul>
          </div>
          <?php endif;?>

          <div class="section__nav--help">
            <a class="btn btn--big" href="#">Оставить отзыв</a>
          </div>
        </div>
      <?php endif;?>

      <?php echo do_shortcode('[testimonial_view id=2]'); ?>

      <?php echo do_shortcode('[testimonial_view id=1]'); ?>

      <?php if ($get_testimonials_view_data['pagination'] && $total_pages > 0): ?>
      <div class="section__nav section__nav--bottom">
        <div class="pager__nav">
          <ul class="pager__nav--list">

            <?php if ($paged > 1): ?>
              <li class="pager__nav--item pager__nav--prev">
                <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . ($paged - 1) . '/'; ?>">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                    <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                  </svg>
                </a>
              </li>

              <?php if (($paged - 1) !== 1): ?>
                <li class="pager__nav--item pager__nav--first">
                  <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/1/'; ?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                        <polygon class="st0" points="6,9.6 14.7,0.9 13.8,0 4.2,9.6 13.8,19.2 14.7,18.3 "/>
                      <polygon class="st0" points="1.9,9.6 10.5,0.9 9.6,0 0,9.6 9.6,19.2 10.5,18.3 "/>
                      </svg>
                  </a>
                </li>
              <?php endif; ?>

            <?php endif; ?>

            <?php for ($i=1; $i<=$total_pages; $i++): ?>
              <li class="pager__nav--item <?php print ($i == $paged) ? 'active' : ''; ?>">
                <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . $i . '/'; ?>"><?php print $i; ?></a>
              </li>
            <?php endfor; ?>

            <?php if ($paged < $total_pages): ?>

              <?php if (($paged + 1) != $total_pages): ?>
                <li class="pager__nav--item pager__nav--last">
                  <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . $total_pages . '/'; ?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14.7px" height="19.2px" viewBox="0 0 14.7 19.2" xml:space="preserve">
                        <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                      <polygon class="st0" points="12.9,9.6 4.2,18.3 5.1,19.2 14.7,9.6 5.1,0 4.2,0.9 "/>
                      </svg>
                  </a>
                </li>
              <?php endif; ?>

              <li class="pager__nav--item pager__nav--next">
                <a class="pager__nav--link" href="<?php print get_permalink($post->ID) . 'page/' . ($paged + 1) . '/'; ?>">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10.5px" height="19.2px" viewBox="0 0 10.5 19.2" xml:space="preserve">
                    <polygon class="st0" points="8.7,9.6 0,18.3 0.9,19.2 10.5,9.6 0.9,0 0,0.9 "/>
                  </svg>
                </a>
              </li>
            <?php endif; ?>

          </ul>
        </div>
      </div>
      <?php endif; ?>

    </div>
  </section>

  <script>
    (function($){
      $('.section__nav--help .btn').on('click', function(e) {
        e.preventDefault();

        $(this).fadeOut();

        $('.about__reviews--form').slideDown();

      });

      $('.about__reviews--form .about__reviews--cancel').on('click', function(e) {
        e.preventDefault();

        $('.section__nav--help .btn').fadeIn();

        $('.about__reviews--form').slideUp();

      })
    })(jQuery);
  </script>

<?php get_footer();
