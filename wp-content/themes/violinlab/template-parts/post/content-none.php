<?php
  global $wp_query;

?>

  <section class="section__head">
    <div class="container">
      <div class="head__content--padding">
        <?php if ( have_posts() ) : ?>
          <h1 class="page-title"><?php printf( 'Поиск по запросу %s', '<span>"' . get_search_query() . '"</span>' ); ?></h1>
        <?php else : ?>
          <h1 class="page-title">
            Ничего не найдено
          </h1>
        <?php endif; ?>
      </div>
    </div>
  </section>
