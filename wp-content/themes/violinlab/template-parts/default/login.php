<?php
/**
 * The auth form for our theme.
 */
?>
<div class="main__auth">
	<?php
		/**
		 * User auth.
		 */
		if (!is_user_logged_in()):
	?>
      <div class="auth__visible"><a class="auth__link" href="#">Вход</a></div>
      <div class="auth__hidden">
        <div class="auth__form">
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="text" name="log" value="">
              <span class="form__field--placeholder">Email/Логин</span>
            </label>
          </div>
          <div class="form__field">
            <label class="form__field--label">
              <input class="form__field--input" type="password" name="pwd" value="">
              <span class="form__field--placeholder">Пароль</span>
            </label>
          </div>
        </div>
        <div class="auth__forgot"><a class="auth__forgot--link" href="<?php echo wp_lostpassword_url(); ?>">Забыли пароль?</a></div>
        <div class="auth__action">
          <div class="auth__action--line">
            <a class="auth__action--submit auth__action--btn btn btn--big" href="#">Войти</a>
          </div>
          <div class="auth__action--line">
            <a class="auth__action--btn btn btn--small" href="<?php print wp_registration_url(); ?>">Регистрация</a>
          </div>
          <div class="form__message">
            <span></span>
          </div>
        </div>
      </div>

	<?php
		/**
		 * User not auth.
		 */
		else:
			$user_name = get_user_meta(get_current_user_id(), 'first_name', true);
	?>

      <div class="auth__visible">
        <div class="auth__name"><span><?php print $user_name; ?></span></div>
        <div class="auth__ico"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.64 27.18"><polygon class="cls-1" points="31.54 13.38 33.59 15.43 35.64 13.38 31.54 13.38"/><path class="cls-2" d="M13.59,26.79A13.1,13.1,0,0,1,6,24.34a.4.4,0,0,1,.09-.7c3.32-1.23,3.85-2.22,3.85-4.13a1.39,1.39,0,0,0-.32-1,5.19,5.19,0,0,1-.9-2.35c0-.2-.07-.22-.25-.3-.48-.23-.93-.57-1-2.11a1.82,1.82,0,0,1,.47-1.36c0-.27-.12-.72-.2-1.22C7.29,9,7.27,6.25,10.39,5c2.72-1.1,4.9-.5,5.58.22a3.59,3.59,0,0,1,2.59,1.15c1.34,1.55.91,5.09.77,6a1.82,1.82,0,0,1,.47,1.34c-.11,1.56-.56,1.9-1,2.13-.18.09-.21.1-.25.3a5.21,5.21,0,0,1-.9,2.35,1.39,1.39,0,0,0-.32,1c0,1.91.54,2.91,3.85,4.13a.4.4,0,0,1,.09.7,13.1,13.1,0,0,1-7.64,2.44ZM7,24.11a12.35,12.35,0,0,0,13.09,0c-2.78-1.14-3.65-2.29-3.65-4.61a2.12,2.12,0,0,1,.46-1.42,4.56,4.56,0,0,0,.77-2,1.1,1.1,0,0,1,.7-.88c.25-.12.5-.24.59-1.44,0-.61-.28-.78-.29-.78a.42.42,0,0,1-.21-.43c.2-1.06.52-4.38-.55-5.62A2.81,2.81,0,0,0,15.86,6h-.11a.64.64,0,0,1-.34-.21c-.21-.37-2.11-1.12-4.73-.06S8.1,9,8.44,11.07c.11.77.23,1.43.23,1.43a.4.4,0,0,1-.22.44s-.27.17-.27.81c.09,1.17.34,1.29.59,1.41a1.08,1.08,0,0,1,.69.88,4.51,4.51,0,0,0,.78,2,2.14,2.14,0,0,1,.46,1.42c0,2.32-.87,3.47-3.65,4.61Zm0,0"/><path class="cls-2" d="M13.59,0A13.59,13.59,0,1,0,23,23.39a.55.55,0,1,0-.77-.8,12.45,12.45,0,1,1,3-4.5.55.55,0,1,0,1,.4A13.6,13.6,0,0,0,13.59,0Zm0,0"/></svg></div>
      </div>
      <div class="auth__hidden">
        <ul class="auth__links">
          <li class="auth__links--item"><a class="auth__links--link btn btn--medium " href="<?php print site_url(VR_USER_URL);?>">ЛИЧНЫЙ КАБИНЕТ</a></li>
          <li class="auth__links--item"><a class="auth__links--link btn btn--medium " href="/#lessons">МОИ УРОКИ</a></li>
          <li class="auth__links--item"><a class="auth__links--link btn btn--medium auth__links--exit" href="<?php echo wp_logout_url(violinlab_current_url()); ?>">ВЫЙТИ</a></li>
        </ul>
      </div>

	<?php
		endif;
	?>
</div>
