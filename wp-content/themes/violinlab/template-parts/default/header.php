<?php
/**
 * Default page head.
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
  <!--[if lt IE 9]>
  <script src="<?php print get_template_directory_uri() . '/assets/js/html5shiv.min.js;'?>"></script>
  <![endif]-->

  <link rel="stylesheet" href="<?php print get_template_directory_uri() . '/assets/css/jquery.formstyler.css'; // @TODO. ?>">
  <link rel="stylesheet" href="<?php print get_template_directory_uri() . '/assets/css/jquery.formstyler.theme.css'; // @TODO. ?>">
	<?php wp_head(); ?>


	<!--  <link rel="stylesheet" href="style.css">-->
	<!---->
	<!--  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
</head>
<body>
