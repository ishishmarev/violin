<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

  <div class="page__content">

    <section class="section__head">
      <div class="container">
        <div class="head__content--padding">
          <?php if ( have_posts() ) : ?>
            <h1 class="page-title"><?php printf( 'Поиск по запросу %s', '<span>"' . get_search_query() . '"</span>' ); ?></h1>
          <?php else : ?>
            <h1 class="page-title">
              Ничего не найдено
            </h1>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <section class="section__body">
      <div class="container">

        <?php if ( have_posts() ) : ?>

          <div class="search">

          <?php while ( have_posts() ) : the_post(); ?>

            <div class="post__item">
              <div class="post__body">
                <div class="post__title"><?php the_title(); ?></div>
                <div class="post__text"><?php the_content(); ?></div>
                <div class="post__more">
                  <a href="<?php the_permalink(); ?>">
                    подробнее
                  </a>
                </div>
              </div>
            </div>

          <?php endwhile; ?>

          </div>

        <?php endif; ?>

      </div>
    </section>

  </div>

<?php get_footer();
