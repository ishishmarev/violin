<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="page__content">

  <section class="section__head">
    <div class="container">
      <div class="head__content--padding">
        <h1 class="page-title text-center">Мы не можем найти эту страницу</h1>
      </div>
    </div>
  </section>

  <section class="section__body section__body--404">
    <div class="container">
      <div class="not_found_404">
        <img src="<?php print get_theme_file_uri( '/assets/images/404.png' ); ?>" alt="404">
      </div>
      <div class="not_found_404-btn">
        <a href="<?php print home_url(); ?>" class="btn btn--big">На главную</a>
      </div>
    </div>
  </section>

</div>

<?php get_footer();
