<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function violinlab_setup() {
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

  // Add theme support for Custom Logo.
  add_theme_support( 'custom-logo', array(
    'width'       => 250,
    'height'      => 250,
    'flex-width'  => true,
  ));

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'header_menu'   => 'Меню в шапке',
		'footer_menu'   => 'Меню в подвале',
	));

	add_theme_support( 'post-thumbnails' );

  add_image_size( 'violinlab-webinars-preview', 465, 295, true );
}
add_action( 'after_setup_theme', 'violinlab_setup' );

/**
 * Enqueue scripts and styles.
 */
function violinlab_scripts() {
  // Theme stylesheet.
  //wp_enqueue_style( 'violinlab', get_stylesheet_uri() ); @TODO. prod mode
  wp_enqueue_style( 'violinlab', get_template_directory_uri() . '/style.css', array(), time() ); // @TODO. develop mode

	// Theme javascript.
	wp_enqueue_script( 'violinlab-formstyler', get_theme_file_uri( '/assets/js/jquery.formstyler.min.js' ), array( 'jquery' ), '1.0', true );
  wp_enqueue_script( 'jQuery-UI', get_theme_file_uri( '/assets/js/jquery-ui.min.js' ), array( 'jquery' ), '1.12.1', true);
	wp_enqueue_script( 'violinlab-main', get_theme_file_uri( '/assets/js/main.js' ), array( 'violinlab-formstyler' ), '1.0', true );

	// AJAX Auth.
	wp_localize_script( 'violinlab-main', 'ajaxLogon', array(
		'url' => admin_url( 'admin-ajax.php' ),
		'mes' => 'Проверка данных...',
		'sec' => wp_create_nonce('violinlab-security'),
		'red' => violinlab_current_url()
	));
}
add_action( 'wp_enqueue_scripts', 'violinlab_scripts' );

/**
 * Register widget area.
 */
function violinlab_widgets_init() {
  register_sidebar(
    array(
      'name'          => 'Полоска под шапкой',
      'id'            => 'sub_header_sidebar',
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );

  register_sidebar(
    array(
      'name'          => 'Основная новость',
      'id'            => 'main_sidebar',
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );

  register_sidebar(
    array(
      'name'          => 'Дополнительный сайдбар',
      'id'            => 'additional_sidebar',
      'description'   => 'Вывод справа от основной новости',
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );
}
add_action( 'widgets_init', 'violinlab_widgets_init' );


/**
 * Remove the span wrapper in Contact Form 7
 */
function violinlab_remove_span_wrappers_cf7($content) {
  $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

  return $content;
}
add_filter( 'wpcf7_form_elements', 'violinlab_remove_span_wrappers_cf7' );

/**
 * Search page templates.
 */
function violinlab_get_page_template() {
  $id = get_queried_object_id();
  $template = get_page_template_slug();
  $pagename = get_query_var('pagename');

  if ( ! $pagename && $id ) {
    // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
    $post = get_queried_object();
    if ( $post )
      $pagename = $post->post_name;
  }

  $templates = array();

  if ( $template && 0 === validate_file( $template ) )
    $templates[] = $template;
  // if there's a custom template then still give that priority

  if ( $pagename )
    $templates[] = "custom_pages/page-$pagename.php";
  // change the default search for the page-$slug template to use our directory
  // you could also look in the theme root directory either before or after this

  if ( $id )
    $templates[] = "page-$id.php";
  $templates[] = 'page.php';

  /* Don't call get_query_template again!!!
		 // return get_query_template( 'page', $templates );
		 We also reproduce the key code of get_query_template() - we don't want to call it or we'll get stuck in a loop .
		 We can remove lines of code that we know won't apply for pages, leaving us with...
	*/

  $template = locate_template( $templates );

  return $template;
}
add_filter( 'page_template', 'violinlab_get_page_template' );

/**
 * Page about.
 * Page navigation.
 */
function violinlab_list_child_pages() {
	global $post;
	if ( is_page() && $post->post_parent ) {
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	} else {
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
	}

	if ( $childpages ) {
		$string = '<ul class="page__menu--list">' . $childpages . '</ul>';
	}

	return $string;
}

/**
 * Current page.
 */
function violinlab_current_url() {
  global $wp;
  return home_url(add_query_arg( array(), $wp->request ));
}

/**
 * Remove dns-prefetch.
 */
remove_action( 'wp_head', 'wp_resource_hints', 2);

/**
 * Remove emoji icons.
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Remove unused tags.
 */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
 * Remove wp version
 * from generator.
 */
function wp_version_js_css($src) {
  if (strpos($src, 'ver=' . get_bloginfo('version')))
    $src = remove_query_arg('ver', $src);
  return $src;
}
add_filter('style_loader_src', 'wp_version_js_css', 9999);
add_filter('script_loader_src', 'wp_version_js_css', 9999);

/**
 * Remove wp version
 * from links to scripts.
 */
function remove_wp_version() {
  return '';
}
add_filter('the_generator', 'remove_wp_version');
