<?php
/*
 * dl-file.php
 *
 * Protect uploaded files with login.
 *
 * @link http://wordpress.stackexchange.com/questions/37144/protect-wordpress-uploads-if-user-is-not-logged-in
 *
 * @author hakre <http://hakre.wordpress.com/>
 * @license GPL-3.0+
 * @registry SPDX
 */

define('FFMPEG_PATH', '/home/user/bin');
define('FREE_VIDEOS_DIR_NAME', 'free_videos');
define('FREE_VIDEO_LENGTH', '120'); // продолжительность видео в секундах

require_once('wp-load.php');

$file_path = $_GET['file'];

$post_select_sql = "
SELECT pm.post_id
FROM {$wpdb->prefix}postmeta pm
WHERE pm.meta_value = '{$file_path}'
";

$post_id = $wpdb->get_var($post_select_sql);
$media_post = get_post($post_id);
$post_parent = get_post($media_post->post_parent);

while ($post_parent->post_type !== 0 && $post_parent->post_type === 'attachment') {
  $media_post = $post_parent;
  $post_parent = get_post($post_parent->post_parent);
}

$is_not_free = (bool) get_post_meta( $media_post->ID, 'it_not_free', true );

list($basedir, $baseurl) = array_values( array_intersect_key( wp_upload_dir(), array('basedir' => 1, 'baseurl' => 1) ) );

if ( // Если видео платное и пользователь не является редактором или админом
  $is_not_free
  && ! (current_user_can('editor') || current_user_can('administrator'))
) {

  $free_videos_path = rtrim( $basedir,'/' ) . '/' . FREE_VIDEOS_DIR_NAME;
  $file_name = end( explode('/', $file_path) );

  $not_free_file_name = rtrim( $basedir,'/' ) . '/' . $file_path;
  $free_file_name = 'FREE__' . $file_name;
  $free_file_path = $free_videos_path . '/' . $free_file_name;
  $free_file_url = rtrim( $baseurl,'/' ) . '/' . FREE_VIDEOS_DIR_NAME . '/' . $free_file_name;

  if( !is_dir( $free_videos_path ) ){
    mkdir( $free_videos_path );
  }

  if ( !file_exists($free_file_path) ) {
    $command = escapeshellcmd(FFMPEG_PATH . '/' . 'ffmpeg' . ' -i "' . $not_free_file_name . '" -ss 00:00:00 -codec copy -t ' . FREE_VIDEO_LENGTH . ' "' . $free_file_path . '"');
    $command_result = shell_exec($command);
  }

  $current_user_id = get_current_user_id();

  if ($current_user_id) { // Если пользователь не аноним

    $current_user_expire_service = get_user_meta($current_user_id, 'register_service_expire', true);
    $current_time = current_time( 'mysql' );

    if (strtotime($current_user_expire_service) < strtotime($current_time)) { // Если у пользователя истек срок действия услуги

      header('Location: ' . $free_file_url, true, 301);
      die();

    } else {

      $file = rtrim( $basedir,'/' ) . '/' . $file_path;

    }

  } else {
    header('Location: ' . $free_file_url, true, 301);
    die();
  }

} else {
  $file = rtrim( $basedir,'/' ) . '/' . $file_path;
}

if (!$basedir || !is_file($file)) {
  status_header(404);
  die('404 &#8212; File not found.');
}

$mime = wp_check_filetype($file);
if( false === $mime[ 'type' ] && function_exists( 'mime_content_type' ) )
  $mime[ 'type' ] = mime_content_type( $file );

if( $mime[ 'type' ] )
  $mimetype = $mime[ 'type' ];
else
  $mimetype = 'image/' . substr( $file, strrpos( $file, '.' ) + 1 );

header( 'Content-Type: ' . $mimetype ); // always send this
if ( false === strpos( $_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS' ) )
  header( 'Content-Length: ' . filesize( $file ) );

$last_modified = gmdate( 'D, d M Y H:i:s', filemtime( $file ) );
$etag = '"' . md5( $last_modified ) . '"';
header( "Last-Modified: $last_modified GMT" );
header( 'ETag: ' . $etag );
header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 100000000 ) . ' GMT' );

// Support for Conditional GET
$client_etag = isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ? stripslashes( $_SERVER['HTTP_IF_NONE_MATCH'] ) : false;

if( ! isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) )
  $_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;

$client_last_modified = trim( $_SERVER['HTTP_IF_MODIFIED_SINCE'] );
// If string is empty, return 0. If not, attempt to parse into a timestamp
$client_modified_timestamp = $client_last_modified ? strtotime( $client_last_modified ) : 0;

// Make a timestamp for our most recent modification...
$modified_timestamp = strtotime($last_modified);

if ( ( $client_last_modified && $client_etag )
  ? ( ( $client_modified_timestamp >= $modified_timestamp) && ( $client_etag == $etag ) )
  : ( ( $client_modified_timestamp >= $modified_timestamp) || ( $client_etag == $etag ) )
) {
  status_header( 304 );
  exit;
}

header("X-Accel-Redirect: /play-videos/" . $file_path);
