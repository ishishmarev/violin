"use strict";

var gulp = require('gulp'),
    prefix = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    stylelint = require('gulp-stylelint'),
    browserSync = require('browser-sync');

const THEME_PATH = './wp-content/themes/violinlab/';

var paths = {
  sass: THEME_PATH + 'assets/sass/',
  css: THEME_PATH
};

/**
 * Compile .scss files into public css directory With autoprefixer no
 * need for vendor prefixes then live reload the browser.
 */
gulp.task('sass', function () {
  return gulp.src(paths.sass + '**/*.scss')
    .pipe(stylelint({
      reporters: [
        {formatter: 'string', console: true}
      ]
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: [paths.sass],
      outputStyle: 'expanded'
    }))
    .on('error', sass.logError)
    .pipe(prefix(['last 15 versions', '> 1%', 'Firefox ESR', 'safari >= 6', 'ie > 8'], {
      cascade: true
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('browser-sync', function() {
  var files = [
    '**/*.php',
    '**/*.{png,jpg,gif}'
  ];

  browserSync.init(files, {
    proxy: 'dev.violin.ru',
    port: 3000,
    injectChanges: true
  })
});

gulp.task('build', ['sass']);

gulp.task('default', ['sass', 'browser-sync'], function (){
  gulp.watch(paths.sass + '**/*.scss', ['sass']);
});
